<?php

namespace App\Services;

use App\Services\Service;

class GetGmgColorService extends Service
{
    public function exec()
    {
        return config('enum.gmgColor');
    }
}
