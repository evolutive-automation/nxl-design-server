<?php

namespace Tests\Feature;

use App\Models\CaseType;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\WithFaker;
use App\Services\GetCaseTypeService;
use Tests\DesignTestCase;

class GetCaseTypeServiceTest extends DesignTestCase
{
    use WithFaker, RefreshDatabase, DatabaseMigrations;

    public function test_it_can_get_case_type_list()
    {
        $this->createMetaData();
        $args = ['active' => true];

        $data = app(GetCaseTypeService::class)
            ->setArgs($args)
            ->exec();

        $caseTypes = CaseType::where('active', true)->get();
        $this->assertCount(count($caseTypes), $data);
    }
}
