<?php

namespace App\Repositories;

use App\Models\Model;
use App\Transformers\Transformer;
use Illuminate\Database\Eloquent\ModelNotFoundException;

abstract class Repository
{
    /** @var Model $model */
    protected $model;

    /** @var Transformer $transformer */
    protected $transformer;

    /** @var array $relations */
    protected $relations = [];

    public function setTransformer($transformer)
    {
        $this->transformer = $transformer;

        return $this;
    }

    public function setRelation($relations)
    {
        $this->relations = $relations;

        return $this;
    }

    public function create(array $payload)
    {
        return $this->model::create($payload);
    }

    public function createMany(array $payload)
    {
        return $this->model::insert($payload);
    }

    public function findOrFail($id)
    {
        $model = $this->model::with($this->relations)->findOrFail($id);

        return app($this->transformer)->exec($model);
    }

    public function findForLock($id)
    {
        $model = $this->model::lockForUpdate()
            ->with($this->relations)
            ->findOrFail($id);

        return app($this->transformer)->exec($model);
    }

    public function getByIds($ids)
    {
        $collection = $this->model::whereIn('id', $ids)
            ->with($this->relations)
            ->get();

        $transformer = app($this->transformer);

        return $collection->map(fn ($row) => $transformer->exec($row))->toArray();
    }

    public function getByIdsForLock($ids)
    {
        $collection = $this->model::whereIn('id', $ids)
            ->lockForUpdate()
            ->with($this->relations)
            ->get();

        $transformer = app($this->transformer);

        return $collection->map(fn ($row) => $transformer->exec($row))->toArray();
    }

    public function firstWhere($key, $value)
    {
        $model = $this->model::with($this->relations)->firstWhere($key, $value);

        if (is_null($model)) {
            throw new ModelNotFoundException("[{$key}:{$value}] 找不到資料");
        }

        return app($this->transformer)->exec($model);
    }

    public function search(array $args = [])
    {
        $query = $this->model::query();

        if (count($args)) {
            foreach ($args as $key => $value) {
                $query->where($key, $value);
            }
        }
        $collection = $query->with($this->relations)
            ->get();

        $transformer = app($this->transformer);

        return $collection->map(fn ($row) => $transformer->exec($row))->toArray();
    }

    public function update(int $id, array $payload)
    {
        return $this->model::where('id', $id)->update($payload);
    }

    public function updateMany(array $ids, array $payload)
    {
        return $this->model::whereIn('id', $ids)->update($payload);
    }

    public function delete(int $id)
    {
        return $this->model::destroy($id);
    }

    public function deleteByIds(array $ids)
    {
        return $this->model::whereIn('id', $ids)->delete();
    }

    public function deleteAll()
    {
        return $this->model::truncate();
    }

    public function createOrUpdate(array $row, array $uniqueKeys = [])
    {
        $uniqueData = [];
        foreach ($uniqueKeys as $uniqueKey) {
            $uniqueData[$uniqueKey] = $row[$uniqueKey];
        }
        $model = $this->model::firstOrCreate($uniqueData);
        $model->update($row);
        return $this->findOrFail($model->id);
    }

    public function findOrCreate(array $row, array $uniqueKeys = [])
    {
        $uniqueData = [];
        foreach ($uniqueKeys as $uniqueKey) {
            $uniqueData[$uniqueKey] = $row[$uniqueKey];
        }
        $queryBuilder = $this->model::query();
        foreach ($uniqueData as $uniqueKey => $uniqueValue) {
            $queryBuilder->where($uniqueKey, '=', $uniqueValue);
        }
        $model = $queryBuilder->first();
        if (is_null($model)) {
            $model = $this->model::create($row);
        }

        return $this->findOrFail($model->id);
    }

    public function searchByPaginate($filters = [], $perPage = 15, $page = 1)
    {
        $query = $this->model::query();
        if (count($filters)) {
            $query = $query->advancedFilter($filters);
        }
        $paginator = $query->with($this->relations)
            ->paginate($perPage)
            ->appends([
                'filters' => $filters,
                'perPage' => $perPage,
                'page' => $page
            ]);
        $transformer = app($this->transformer);
        $items = collect($paginator->getCollection())->map(fn ($row) => $transformer->exec($row))->toArray();

        return [
            'currentPage' => (int) $paginator->currentPage(),
            'perPage' => (int) $paginator->perPage(),
            'hasNextPage' => is_null($paginator->nextPageUrl())? false : true,
            'totalPages' => (int) ceil($paginator->total() / $paginator->perPage()),
            'total' => (int) $paginator->total(),
            'items' => $items,
        ];
    }

    public function searchAll($filters = [])
    {
        $query = $this->model::query();
        if (count($filters)) {
            $query = $query->advancedFilter($filters);
        }
        $collection = $query->with($this->relations)
            ->get();

        $transformer = app($this->transformer);

        return $collection->map(fn ($row) => $transformer->exec($row))->toArray();
    }
}
