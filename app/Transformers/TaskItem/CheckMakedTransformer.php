<?php

namespace App\Transformers\TaskItem;

use App\Models\TaskItem;
use App\Transformers\Transformer;

class CheckMakedTransformer extends Transformer
{
    /**
     * @param TaskItem $taskItemModel
     *
     * @return array
     */
    public function exec($taskItemModel)
    {
        $taskModel = $taskItemModel->task;
        $designMaterialModel = $taskItemModel->design->material;

        return [
            'id' => $taskItemModel->id,
            'taskId' => $taskItemModel->task_id,
            'havePrint' => $taskItemModel->have_print? true : false,
            'haveWeb' => $taskItemModel->have_web? true : false,
            'isAllowed' => $taskItemModel->is_allowed? true : false,
            'materialPrintPath' => $taskItemModel->material_print_path,
            'materialWebPath' => $taskItemModel->material_web_path,
            'task' => [
                'status' => $taskModel->status,
                'quantity' => (int) $taskModel->quantity,
                'ngQuantity' => (int) $taskModel->ng_quantity,
                'doneQuantity' => (int) $taskModel->done_quantity,
            ],
            'material' => [
                'printPath' => "material/print/{$designMaterialModel->filename}.pdf",
                'webPath' => "material/web/{$designMaterialModel->filename}.png"
            ]
        ];
    }
}
