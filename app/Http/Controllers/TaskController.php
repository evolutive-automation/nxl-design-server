<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Services\CreateDesignTaskService;
use App\Services\CreateDeviceTaskService;
use App\Services\DeletePendingTaskService;
use App\Services\GenerateNotAllowedZipService;
use App\Services\GetNotYetCheckTaskService;
use App\Services\MarkAsCancelledService;
use App\Services\MarkAsCheckedService;
use App\Services\MarkAsUrgentTaskService;
use Illuminate\Http\Request;

class TaskController extends Controller
{
    public function getNotYetCheckTasks()
    {
        $data = app(GetNotYetCheckTaskService::class)
            ->exec();

        return ok(['data' => $data]);
    }

    public function deleteTask($id)
    {
        app(DeletePendingTaskService::class)
            ->setTaskId($id)
            ->exec();

        return ok();
    }

    public function markAsChecked($id)
    {
        app(MarkAsCheckedService::class)
            ->setTaskId($id)
            ->exec();

        return ok();
    }

    public function markAsUrgent($id)
    {
        app(MarkAsUrgentTaskService::class)
            ->setTaskId($id)
            ->exec();

        return ok();
    }

    public function createDesignTask(Request $request)
    {
        $data = app(CreateDesignTaskService::class)
            ->setPayload($request->all())
            ->exec();

        return ok(['data' => $data]);
    }

    public function createDeviceTask(Request $request)
    {
        $data = app(CreateDeviceTaskService::class)
            ->setPayload($request->all())
            ->exec();

        return ok(['data' => $data]);
    }

    public function markAsCancelled($id)
    {
        app(MarkAsCancelledService::class)
            ->setTaskId($id)
            ->exec();

        return ok();
    }

    public function downloadZip($id)
    {
        $zipPath = app(GenerateNotAllowedZipService::class)
            ->setTaskId($id)
            ->exec();

        return response()->download($zipPath)->deleteFileAfterSend(true);
    }
}
