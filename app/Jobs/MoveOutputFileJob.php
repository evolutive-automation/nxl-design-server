<?php

namespace App\Jobs;

use App\Exceptions\CantMoveFileException;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\Repositories\TaskItemFailJobRepository;
use Illuminate\Validation\ValidationException;
use Exception;
use App\Notifications\SlackNotification;
use Illuminate\Support\Facades\Notification;
use Illuminate\Support\Arr;

class MoveOutputFileJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /** @var array $payload */
    public $payload;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(array $payload)
    {
        $this->payload = $payload;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $this->deleteFailJob($this->payload);
        foreach (['outputPrintPath', 'outputWebPath'] as $column) {
            $filename = basename($this->payload[$column]);
            $fileInfo = explode('-', $filename);
            $outputPath = env('DesignFileSourceDir')."/{$this->payload[$column]}";
            if (!file_exists($outputPath)) {
                continue;
            }
            $destFolder = ($column === 'outputPrintPath')? env('PrintFileSourceDir') : env('PreviewFileSourceDir');
            // 檢查並建立資料夾
            $this->checkFolder($destFolder);
            $this->checkFolder("{$destFolder}/{$fileInfo[0]}");
            $this->checkFolder("{$destFolder}/{$fileInfo[0]}/{$fileInfo[1]}");
            $this->checkFolder("{$destFolder}/{$fileInfo[0]}/{$fileInfo[1]}/{$fileInfo[2]}");
            // 搬移檔案
            $destPath = "{$destFolder}/{$fileInfo[0]}/{$fileInfo[1]}/{$fileInfo[2]}/{$filename}";
            $cmd = "mv -f {$outputPath} {$destPath}";
            exec($cmd, $output, $retval);
            if ($retval) {
                throw new CantMoveFileException("移動檔案到排圖資料夾失敗,{$outputPath}");
            }
        }
    }

    public function checkFolder($folder)
    {
        if (!is_dir($folder)) {
            mkdir($folder);
        }
    }

    public function failed(Exception $e)
    {
        $error_message = $e->getMessage();
        if ($e instanceof ValidationException) {
            $error_message = json_encode($e->validator->errors());
        }
        $form = [
            'job_name' => get_class($this),
            'job_key' => Arr::get($this->payload, 'id', ''),
            'payload' => json_encode($this->payload),
            'error_message' => $error_message
        ];
        app(TaskItemFailJobRepository::class)
            ->createOrUpdate($form, ['job_name', 'job_key']);
        $this->sendSlackNotification($form);
    }

    private function sendSlackNotification($form)
    {
        $slackNotification = app(SlackNotification::class)
            ->setChannel('#設計出圖系統')
            ->setTitle("Job名稱：{$form['job_name']}")
            ->setMessage("錯誤訊息：{$form['error_message']}");
        Notification::route('slack', env('SLACK_URL'))
            ->notify($slackNotification);
    }

    private function deleteFailJob($payload)
    {
        $jobName = get_class($this);
        $jobKey = $payload['id'];
        app(TaskItemFailJobRepository::class)
            ->deleteByJobKey($jobName, $jobKey);
    }
}
