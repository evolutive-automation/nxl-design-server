<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Services\GetNotYetCheckTaskItemService;
use App\Services\MarkAsNotAllowedService;
use Illuminate\Http\Request;

class TaskItemController extends Controller
{
    public function getNotYetCheckItem($taskId)
    {
        $data = app(GetNotYetCheckTaskItemService::class)
            ->setTaskId($taskId)
            ->exec();

        return ok(['data' => $data]);
    }

    public function markAsNotAllowed($id)
    {
        app(MarkAsNotAllowedService::class)
            ->setTaskItemId($id)
            ->exec();

        return ok();
    }
}
