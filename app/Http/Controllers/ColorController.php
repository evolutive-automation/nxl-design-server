<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Services\GetGmgColorService;
use Illuminate\Http\Request;

class ColorController extends Controller
{
    public function getGmgColor()
    {
        $data = app(GetGmgColorService::class)
            ->exec();

        return ok(['data' => $data]);
    }
}
