<?php

namespace Tests\Feature;

use App\Jobs\DeletePendingFileJob;
use App\Services\CheckMakingItemService;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Queue;
use Tests\DesignTestCase;

class CheckMakingItemServiceTest extends DesignTestCase
{
    use WithFaker, RefreshDatabase, DatabaseMigrations;

    public function test_it_can_check_making_item()
    {
        $expectedHaveOutput = true;
        $fakeDesignTask = $this->createFakeDesignTask('making');
        $task = $fakeDesignTask['task'];
        $designMaterial = $fakeDesignTask['designMaterial'];
        $taskItems = $this->createFakeDesignTaskItem($task, $designMaterial, $expectedHaveOutput);

        Queue::fake();
        app(CheckMakingItemService::class)
            ->exec();

        Queue::assertPushed(DeletePendingFileJob::class);
        $this->assertDatabaseHas('tasks', [
            'id' => $task['id'],
            'status' => 'made',
            'done_quantity' => count($taskItems)
        ]);
        foreach ($taskItems as $taskItem) {
            $this->assertDatabaseHas('task_items', [
                'task_id' => $taskItem['task_id'],
                'design_id' => $taskItem['design_id'],
                'have_print' => 1,
                'have_web' => 1
            ]);
        }
    }
}
