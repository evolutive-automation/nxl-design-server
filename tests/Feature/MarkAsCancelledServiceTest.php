<?php

namespace Tests\Feature;

use App\Exceptions\CantCancelledTaskException;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\WithFaker;
use App\Services\MarkAsCancelledService;
use Tests\DesignTestCase;
use Carbon\Carbon;

class MarkAsCancelledServiceTest extends DesignTestCase
{
    use WithFaker, RefreshDatabase, DatabaseMigrations;

    public function test_it_can_mark_as_cancelled_by_made()
    {
        $expectedHaveOutput = true;
        $fakeDesignTask = $this->createFakeDesignTask('made');
        $task = $fakeDesignTask['task'];
        $designMaterial = $fakeDesignTask['designMaterial'];
        $this->createFakeDesignTaskItem($task, $designMaterial, $expectedHaveOutput);
        $task->taskItems()->update([
            'have_print' => true,
            'have_web' => true
        ]);

        app(MarkAsCancelledService::class)
            ->setTaskId($task->id)
            ->exec();

        $this->assertDatabaseHas('tasks', [
            'id' => $task->id,
            'status' => 'cancelled',
            'ended_at' => Carbon::now()->toDateTimeString()
        ]);
        foreach ($task->taskItems as $taskItem) {
            $this->assertDatabaseHas('task_items', [
                'id' => $taskItem->id,
                'is_allowed' => 0
            ]);
        }
    }

    public function test_it_can_mark_as_cancelled_by_making()
    {
        $fakeDesignTask = $this->createFakeDesignTask('making');
        $task = $fakeDesignTask['task'];
        $designMaterial = $fakeDesignTask['designMaterial'];
        $this->createFakeDesignTaskItem($task, $designMaterial);

        app(MarkAsCancelledService::class)
            ->setTaskId($task->id)
            ->exec();

        $this->assertDatabaseHas('tasks', [
            'id' => $task->id,
            'status' => 'cancelled',
            'ended_at' => Carbon::now()->toDateTimeString()
        ]);
        foreach ($task->taskItems as $taskItem) {
            $this->assertDatabaseHas('task_items', [
                'id' => $taskItem->id,
                'is_allowed' => 0
            ]);
        }
    }

    public function test_it_can_throw_exception()
    {
        $fakeDesignTask = $this->createFakeDesignTask('checked');
        $task = $fakeDesignTask['task'];
        $designMaterial = $fakeDesignTask['designMaterial'];
        $this->createFakeDesignTaskItem($task, $designMaterial);

        $this->withExceptionHandling();
        $this->expectException(CantCancelledTaskException::class);
        app(MarkAsCancelledService::class)
            ->setTaskId($task->id)
            ->exec();
    }
}
