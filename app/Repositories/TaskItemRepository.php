<?php

namespace App\Repositories;

use App\Repositories\Repository;
use App\Models\TaskItem;
use App\Transformers\TaskItem\TaskItemTransformer;

class TaskItemRepository extends Repository
{
    protected $model = TaskItem::class;

    protected $transformer = TaskItemTransformer::class;

    public function getMakingTaskItems()
    {
        $collection = $this->model::where('is_allowed', 1)
            ->where(function ($q) {
                return $q->where('have_print', 0)
                    ->orWhere('have_web', 0);
            })
            ->lockForUpdate()
            ->with($this->relations)
            ->get();

        $transformer = app($this->transformer);

        return $collection->map(fn ($row) => $transformer->exec($row))->toArray();
    }

    public function getByTaskId($taskId)
    {
        $collection = $this->model::where('task_id', $taskId)
            ->with($this->relations)
            ->get();

        $transformer = app($this->transformer);

        return $collection->map(fn ($row) => $transformer->exec($row))->toArray();
    }

    public function updateByTaskId($taskId, $payload)
    {
        return $this->model::where('task_id', $taskId)->update($payload);
    }

    public function getNotAllowedByTaskId($taskId)
    {
        $collection = $this->model::where('task_id', $taskId)
            ->where('is_allowed', 0)
            ->where('have_web', 1)
            ->with($this->relations)
            ->get();

        $transformer = app($this->transformer);

        return $collection->map(fn ($row) => $transformer->exec($row))->toArray();
    }
}
