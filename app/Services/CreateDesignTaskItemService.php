<?php

namespace App\Services;

use App\Repositories\DesignMaterialRepository;
use App\Repositories\DesignRepository;
use App\Repositories\DeviceRepository;
use App\Repositories\TaskItemRepository;
use App\Repositories\TaskRepository;
use App\Services\Service;
use Carbon\Carbon;
use Exception;

class CreateDesignTaskItemService extends Service
{
    /** @var string $materialWebPath */
    protected $materialWebPath;

    /** @var array $task */
    protected $task;

    /** @var array $payload */
    protected $payload;

    /** @var TaskRepository $taskRepository */
    protected $taskRepository;

    /** @var TaskItemRepository $taskItemRepository */
    protected $taskItemRepository;

    /** @var DeviceRepository $deviceRepository */
    protected $deviceRepository;

    /** @var DesignRepository $designRepository */
    protected $designRepository;

    /** @var DesignMaterialRepository $designMaterialRepository */
    protected $designMaterialRepository;

    public function __construct(
        TaskRepository $taskRepository,
        TaskItemRepository $taskItemRepository,
        DeviceRepository $deviceRepository,
        DesignRepository $designRepository,
        DesignMaterialRepository $designMaterialRepository
    ) {
        $this->taskRepository = $taskRepository;
        $this->taskItemRepository = $taskItemRepository;
        $this->deviceRepository = $deviceRepository;
        $this->designRepository = $designRepository;
        $this->designMaterialRepository = $designMaterialRepository;
    }

    public function setTask($task)
    {
        $this->task = $task;

        return $this;
    }

    public function exec()
    {
        $this->payload = $this->task['payload'];
        $this->validateRule();
        $this->updateTaskStatus();
        $this->payload['devices'] = $this->convertDevices();
        $taskItems = $this->processTaskItems();
        $this->createTaskItems($taskItems);
        $this->updateTask($taskItems);
        return $taskItems;
    }

    private function validateRule()
    {
        $this->validate($this->payload, [
            'case_type' => 'required|string',
            'colors' => 'required|array',
            'devices' => 'required|array',
            'sort' => 'required|string',
            'code' => 'required|string',
            'print_type' => 'required|string',
            'version' => 'required|numeric'
        ]);
    }

    private function updateTaskStatus()
    {
        $payload = [
            'status' => 'making'
        ];

        $this->taskRepository->update($this->task['id'], $payload);
    }

    private function convertDevices()
    {
        $devices = $this->deviceRepository->search();
        $groups = collect($devices)->groupBy('type')->toArray();
        $data = [];
        foreach ($this->payload['devices'] as $item) {
            if ($item === 'iPhone全部') {
                $caseType = $this->payload['case_type'];
                $codes = collect($groups['iphone'])
                    ->filter(function ($device) use ($caseType) {
                        $result = true;
                        if ($caseType === 'SolidSuit' &&
                            $device['code'] === '001'
                        ) {
                            $result = false;
                        }
                        if ($caseType === 'ModNX' &&
                            in_array($device['code'], ['028','029'])
                        ) {
                            $result = false;
                        }
                        return $result;
                    })
                    ->pluck('code')
                    ->toArray();
                $data = array_merge($data, $codes);
            } elseif ($item === 'Android全部') {
                $codes = collect($groups['android'])->pluck('code')->toArray();
                $data = array_merge($data, $codes);
            } elseif ($item === 'Airpods全部') {
                $codes = collect($groups['airpods'])->pluck('code')->toArray();
                $data = array_merge($data, $codes);
            } elseif ($item === 'iPhone11-13全部') {
                $codes = ['147','148','149','184','185','187','230','228','231','232'];
                $data = array_merge($data, $codes);
            } else {
                array_push($data, $item);
            }
        }

        return collect($data)->unique()->toArray();
    }

    private function getDesignMaterial()
    {
        // 產品-設計編碼-版本
        $caseType = $this->payload['case_type'];
        $code = $this->payload['code'];
        $version = $this->payload['version'];
        $filename = "{$caseType}-{$code}-{$version}";

        return $this->designMaterialRepository->firstWhere('filename', $filename);
    }

    private function createOrUpdateDesign($designMaterial, $deviceCode, $colorCode)
    {
        $payload = [
            'design_material_id' => $designMaterial['id'],
            'case_type' => $this->payload['case_type'],
            'color_code' => $colorCode,
            'device_code' => $deviceCode,
            'sort' => $this->payload['sort'],
            'code' => $this->payload['code'],
            'print_type' => $this->payload['print_type'],
        ];

        return $this->designRepository->createOrUpdate($payload, ['case_type', 'color_code', 'device_code', 'code']);
    }

    private function processTaskItems()
    {
        $data = [];
        $now = Carbon::now();
        $shadowDevices = $this->deviceRepository->search(['have_shadow' => 1]);
        $shadowCodes = collect($shadowDevices)->pluck('code')->toArray();
        $designMaterial = $this->getDesignMaterial();
        foreach ($this->payload['devices'] as $deviceCode) {
            foreach ($this->payload['colors'] as $colorCode) {
                try {
                    $design = $this->createOrUpdateDesign($designMaterial, $deviceCode, $colorCode);
                    $relativePrintPath = $this->savePendingFile($design, $designMaterial['filename'], 'print');
                    $relativeWebPath = $this->savePendingFile($design, $designMaterial['filename'], 'web', $shadowCodes);
                    array_push($data, [
                        'task_id' => $this->task['id'],
                        'design_id'=> $design['id'],
                        'material_print_path' => $relativePrintPath,
                        'material_web_path' => $relativeWebPath,
                        'created_at' => $now,
                        'updated_at' => $now
                    ]);
                } catch (Exception $e) {
                    continue;
                }
            }
        }
        return $data;
    }

    private function savePendingFile($design, $materialFilename, $type, $shadowCodes = [])
    {
        $extension = ($type === 'print')? 'pdf' : 'png';
        $folder = "pending/{$type}";
        $prefixName = "{$design['caseType']}-{$design['colorCode']}-{$design['deviceCode']}-{$design['code']}-{$design['printType']}";
        $filename = "{$prefixName}.{$extension}";
        if ($type === 'web') {
            $hasShadow = in_array($design['deviceCode'], $shadowCodes)? '1' : '0';
            $filename = "{$prefixName}-{$hasShadow}.{$extension}";
        }
        $destPath = env('DesignFileSourceDir')."/{$folder}/{$filename}";
        $sourcePath = env('DesignFileSourceDir'). "/material/{$type}/{$materialFilename}.{$extension}";
        if (file_exists($sourcePath)) {
            file_put_contents($destPath, file_get_contents($sourcePath));
        }

        return "{$folder}/{$filename}";
    }

    private function createTaskItems($items)
    {
        return $this->taskItemRepository->createMany($items);
    }

    private function updateTask($items)
    {
        $payload = [
            'quantity' => count($items),
            'started_at' => Carbon::now()
        ];

        $this->taskRepository->update($this->task['id'], $payload);
    }
}
