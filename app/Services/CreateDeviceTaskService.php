<?php

namespace App\Services;

use App\Exceptions\DuplicateDeviceException;
use App\Repositories\DeviceRepository;
use App\Repositories\TaskRepository;
use App\Services\Service;

class CreateDeviceTaskService extends Service
{
    /** @var array $payload */
    protected $payload;

    /** @var TaskRepository $taskRepository */
    protected $taskRepository;

    /** @var DeviceRepository $deviceRepository */
    protected $deviceRepository;

    public function __construct(
        TaskRepository $taskRepository,
        DeviceRepository $deviceRepository
    ) {
        $this->taskRepository = $taskRepository;
        $this->deviceRepository = $deviceRepository;
    }

    public function setPayload($payload)
    {
        $this->payload = $payload;

        return $this;
    }

    public function exec()
    {
        $this->validateRule();
        $this->createOrUpdateDevice();
        $this->saveDesignMask('print');
        $this->saveDesignMask('web');
        if (isset($this->payload['shadow_web']) &&
            is_file($this->payload['shadow_web'])
        ) {
            $this->saveDesignShadow('web');
        }
        $this->createTask();
    }

    private function validateRule()
    {
        $this->validate($this->payload, [
            'version' => 'required|numeric',
            'brand_code' => 'required|exists:brands,code',
            'code' => 'required|string',
            'name' => 'required|string',
            'case_type' => 'required|exists:case_types,name',
            'colors' => 'required|array',
            'mask_print' => 'required|mimes:pdf',
            'mask_web' => 'required|mimes:png',
            'shadow_web' => 'nullable|mimes:png',
        ]);
    }

    private function createTask()
    {
        /**
         * 任務名稱：
         * 裝置任務 => 型號-顏色-產品
         * 設計任務 => 設計代碼-印程-型號-顏色-產品
         */
        $temp = $this->payload;
        unset($temp['mask_print']);
        unset($temp['mask_web']);
        unset($temp['shadow_web']);
        $colorText = implode(',', $this->payload['colors']);
        $caseType = $this->payload['case_type'];
        $taskName = "{$this->payload['code']}-{$colorText}-{$caseType}";
        $payload = [
            'type' => 'device',
            'name' => $taskName,
            'payload' => json_encode($temp)
        ];

        return $this->taskRepository->create($payload);
    }

    private function createOrUpdateDevice()
    {
        $payload = [
            'brand_code' => $this->payload['brand_code'],
            'code' => $this->payload['code'],
            'name' => $this->payload['name']
        ];
        if (isset($this->payload['shadow_web']) &&
            is_file($this->payload['shadow_web'])
        ) {
            $payload['have_shadow'] = true;
        }
        return $this->deviceRepository->createOrUpdate($payload, ['brand_code', 'code']);
    }

    private function saveDesignMask($type)
    {
        $destFolder = env('DesignFileSourceDir') . "/mask/{$type}";
        $filename = "mask-{$type}-{$this->payload['case_type']}-{$this->payload['code']}";
        $extension = ($type === 'print')? 'pdf' : 'png';
        $destPath = "{$destFolder}/{$filename}.{$extension}";
        file_put_contents($destPath, file_get_contents($this->payload["mask_{$type}"]));
    }

    private function saveDesignShadow($type)
    {
        $destFolder = env('DesignFileSourceDir') . "/shadow/{$type}";
        $filename = "shadow-{$type}-{$this->payload['case_type']}-{$this->payload['code']}";
        $extension = ($type === 'print')? 'pdf' : 'png';
        $destPath = "{$destFolder}/{$filename}.{$extension}";
        file_put_contents($destPath, file_get_contents($this->payload["shadow_{$type}"]));
    }
}
