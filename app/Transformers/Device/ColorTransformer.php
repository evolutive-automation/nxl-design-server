<?php

namespace App\Transformers\Device;

use App\Models\Color;
use App\Transformers\Transformer;

class ColorTransformer extends Transformer
{
    /**
     * @param Color $colorModel
     *
     * @return array
     */
    public function exec($colorModel)
    {
        return [
            'active' => $colorModel->active? true : false,
            'name' => $colorModel->english_name,
            'chineseName' => $colorModel->chinese_name,
            'colorHex' => $colorModel->color_hex,
            'code' => $colorModel->code,
            'originCode' => $colorModel->origin_code
        ];
    }
}
