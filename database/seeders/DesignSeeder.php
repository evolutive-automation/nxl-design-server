<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Design;
use App\Models\DesignMaterial;

class DesignSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'filename' => 'SolidSuit-PF16-1',
                'case_type' => 'SolidSuit',
                'device_code' => '185',
                'color_code' => '52',
                'sort' => 'PF',
                'code' => 'PF16',
                'print_type' => 'CW'
            ], [
                'filename' => 'ModNX-KRF13-1',
                'case_type' => 'ModNX',
                'device_code' => '147',
                'color_code' => '26',
                'sort' => 'KRF',
                'code' => 'KRF13',
                'print_type' => 'CW'
            ]
        ];

        foreach ($data as $row) {
            list($caseType, $code, $version) = explode('-', $row['filename']);
            $payload = [
                'filename' => $row['filename'],
                'case_type' => $caseType,
                'code' => $code,
                'version' => $version
            ];
            $material = DesignMaterial::create($payload);
            $payload = $row;
            $payload['design_material_id'] = $material->id;
            unset($payload['filename']);
            Design::create($payload);
        }
    }
}
