<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Services\NotifyTaskService;

class NotifyTask extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'notify:task';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '通知任務';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $start = microtime(true);
        app(NotifyTaskService::class)
            ->exec();
        $end = microtime(true);
        $total = round($end - $start, 2);
        echo "通知任務成功，總花費{$total}秒\n";
    }
}
