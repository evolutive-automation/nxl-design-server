<?php

namespace App\Exceptions;

use Exception;

class ZipException extends Exception
{
    protected $message;

    public function __construct($message = 'zip exception.')
    {
        parent::__construct($message);
    }

    public function getStatusCode()
    {
        return 500;
    }
}
