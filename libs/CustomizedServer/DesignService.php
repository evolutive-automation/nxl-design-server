<?php

namespace Libs\CustomizedServer;

use App\Repositories\ExternalApiLogRepository;
use Libs\CustomizedServer\Service;
use Illuminate\Support\Arr;
use Exception;

class DesignService extends Service
{
    public function updateDesign($form)
    {
        $url = "{$this->endpoint}/api/designs";
        try {
            $response = $this->sendRequest('POST', $url, $form);
            $this->saveLog($url, $form, $response);
            return $response;
        } catch (Exception $e) {
            $this->saveLog($url, $form, $e->getMessage(), false);
            throw $e;
        }
    }

    public function getDesignByFile($form)
    {
        $url = "{$this->endpoint}/api/designs/get-by-file";
        try {
            $response = $this->sendRequest('POST', $url, $form);
            $this->saveLog($url, $form, $response);
            return $response;
        } catch (Exception $e) {
            $this->saveLog($url, $form, $e->getMessage(), false);
            throw $e;
        }
    }

    private function saveLog($url, $form, $response, $isSuccess = true)
    {
        $payload = [
            'is_success' => $isSuccess,
            'url' => $url,
            'task_item_id' => Arr::get($form, 'id', null),
            'request' => json_encode($form),
            'response' => $isSuccess? json_encode($response) : $response
        ];
        if (env('APP_ENV') !== 'testing') {
            app(ExternalApiLogRepository::class)->create($payload);
        }
    }
}
