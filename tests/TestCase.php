<?php

namespace Tests;

use Illuminate\Foundation\Testing\TestCase as BaseTestCase;
use App\Models\User;
use GuzzleHttp\Psr7\Response;
use Illuminate\Http\UploadedFile;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication;

    public function actingAsUser()
    {
        $user = User::factory()->create();
        $this->actingAs($user, 'web');
        return $user;
    }

    public function getMockResponse($content = [], $statusCode = 200, $headers = [], $autoEncode = true)
    {
        $data = $autoEncode ? json_encode($content) : $content;

        return new Response($statusCode, $headers, $data);
    }

    public function jsonStub(string $filePath, $toArray = false)
    {
        $path = base_path($filePath);
        return json_decode(file_get_contents($path), $toArray);
    }

    public function fileStub(string $filePath)
    {
        $path = base_path($filePath);
        return file_get_contents($path);
    }

    public function mockUploadFile($filename, $mimeType, $folder = 'tests/Stub')
    {
        $path = "{$folder}/{$filename}";

        return new UploadedFile($path, $filename, $mimeType, null, true);
    }
}
