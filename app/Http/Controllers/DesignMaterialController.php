<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Services\GetDesignMaterialService;
use Illuminate\Http\Request;

class DesignMaterialController extends Controller
{
    public function search()
    {
        $data = app(GetDesignMaterialService::class)
            ->exec();

        return ok(['data' => $data]);
    }
}
