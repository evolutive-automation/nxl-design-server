<?php

namespace App\Services;

use App\Exceptions\AlreadyCheckedException;
use App\Repositories\TaskItemRepository;
use App\Repositories\TaskRepository;
use App\Services\Service;
use App\Transformers\TaskItem\DetailTaskItemTransformer;
use Carbon\Carbon;

class MarkAsNotAllowedService extends Service
{
    /** @var int $taskItemId */
    protected $taskItemId;

    /** @var TaskItemRepository $taskItemRepository */
    protected $taskItemRepository;

    /** @var TaskRepository $taskRepository */
    protected $taskRepository;

    public function __construct(
        TaskItemRepository $taskItemRepository,
        TaskRepository $taskRepository
    ) {
        $this->taskItemRepository = $taskItemRepository;
        $this->taskRepository = $taskRepository;
    }

    public function setTaskItemId($taskItemId)
    {
        $this->taskItemId = $taskItemId;

        return $this;
    }

    public function exec()
    {
        $taskItem = $this->getTaskItem();
        if ($taskItem['task']['status'] === 'checked') {
            throw new AlreadyCheckedException("該任務[{$taskItem['taskId']}]已被審核完成，不可標記不出狀態");
        }
        if ($taskItem['isAllowed']) {
            $this->updateTaskItem();
            $this->updateTask($taskItem['task']);
        }
    }

    private function getTaskItem()
    {
        return $this->taskItemRepository
            ->setRelation(['task', 'design'])
            ->setTransformer(DetailTaskItemTransformer::class)
            ->findOrFail($this->taskItemId);
    }

    private function updateTaskItem()
    {
        $payload = [
            'is_allowed' => false
        ];

        return $this->taskItemRepository->update($this->taskItemId, $payload);
    }

    private function updateTask($task)
    {
        $payload = [
            'ng_quantity' => $task['ngQuantity'] + 1
        ];
        if (($payload['ng_quantity'] + $task['doneQuantity']) >= $task['quantity']) {
            $payload['status'] = 'made';
            if (is_null($task['endedAt'])) {
                $payload['ended_at'] = Carbon::now()->toDateTimeString();
            }
        }
        $this->taskRepository->update($task['id'], $payload);
    }
}
