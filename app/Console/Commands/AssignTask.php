<?php

namespace App\Console\Commands;

use App\Services\CreateDesignTaskItemService;
use App\Services\CreateDeviceTaskItemService;
use Illuminate\Console\Command;
use App\Services\GetPendingTaskService;
use Exception;

class AssignTask extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'assign:task';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '分配製作檔案任務';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $start = microtime(true);
        $tasks = app(GetPendingTaskService::class)
            ->setLimit(2)
            ->exec();
        foreach ($tasks as $task) {
            try {
                if ($task['type'] === 'design') {
                    app(CreateDesignTaskItemService::class)
                        ->setTask($task)
                        ->exec();
                } elseif ($task['type'] === 'device') {
                    app(CreateDeviceTaskItemService::class)
                        ->setTask($task)
                        ->exec();
                }
            } catch (Exception $e) {
                continue;
            }
        }
        $end = microtime(true);
        $total = round($end - $start, 2);
        echo "分配任務成功，總花費{$total}秒\n";
    }
}
