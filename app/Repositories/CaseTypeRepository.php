<?php

namespace App\Repositories;

use App\Repositories\Repository;
use App\Models\CaseType;
use App\Transformers\CaseType\CaseTypeTransformer;

class CaseTypeRepository extends Repository
{
    protected $model = CaseType::class;

    protected $transformer = CaseTypeTransformer::class;
}
