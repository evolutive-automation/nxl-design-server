<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class SyncFile extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sync:file';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '同步合完殼的設計檔案到S3';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $start = microtime(true);
        $sourcePath = '/media/fs/printing-console/mergeCase';
        $destPath = 's3://s3.rhinoshield.io/customcase/designer';
        $cmd = "aws s3 sync {$sourcePath} {$destPath}";
        exec($cmd, $output, $retval);
        $end = microtime(true);
        $total = round($end - $start, 2);
        echo "同步合完殼的設計檔案到S3成功，總花費{$total}秒\n";
    }
}
