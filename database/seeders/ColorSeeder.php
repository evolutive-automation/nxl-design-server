<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Color;
use Illuminate\Support\Arr;

class ColorSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                "name" => "Black",
                "color_hex" => "#000000",
                "code" => "17",
                "case_code" => "",
                "active" => false
            ],
            [
                "name" => "White",
                "color_hex" => "#FFFFFF",
                "code" => "18",
                "case_code" => "",
                "active" => false
            ],
            [
                "name" => "Pink",
                "color_hex" => "",
                "code" => "19",
                "case_code" => "",
                "active" => false
            ],
            [
                "name" => "Dark Blue",
                "color_hex" => "#09253D",
                "code" => "20",
                "case_code" => "",
                "active" => false
            ],
            [
                "name" => "Red",
                "color_hex" => "#DC5455",
                "code" => "21",
                "case_code" => "",
                "active" => false
            ],
            [
                "name" => "Shell Pink",
                "color_hex" => "#FAD6D3",
                "code" => "22",
                "case_code" => "",
                "active" => false
            ],
            [
                "name" => "Coral Pink",
                "color_hex" => "#E17582",
                "code" => "23",
                "case_code" => "",
                "active" => false
            ],
            [
                "name" => "Clear",
                "color_hex" => "",
                "code" => "26",
                "case_code" => ""
            ],
            [
                "name" => "Peach Pink",
                "color_hex" => "",
                "code" => "31",
                "case_code" => "",
                "active" => false
            ],
            [
                "name" => "Powder Blue",
                "color_hex" => "",
                "code" => "32",
                "case_code" => "",
                "active" => false
            ],
            [
                "name" => "Clear Black",
                "color_hex" => "#000000",
                "code" => "43",
                "case_code" => "",
                "active" => false
            ],
            [
                "name" => "Full Clear",
                "color_hex" => "#E6E6E5",
                "code" => "44",
                "case_code" => "",
                "active" => false
            ],
            [
                "name" => "Clear Pink",
                "color_hex" => "#F8CBCC",
                "code" => "45",
                "case_code" => "",
                "active" => false
            ],
            [
                "name" => "Black / Carbon Fiber",
                "color_hex" => "#323232",
                "code" => "49",
                "case_code" => "",
                "active" => false
            ],
            [
                "name" => "Black / Basic Black",
                "chinese_name" => "黑",
                "color_hex" => "#1F1F1F",
                "code" => "52",
                "case_code" => ""
            ],
            [
                "name" => "White / Classic White",
                "chinese_name" => "白",
                "color_hex" => "#FEFDFC",
                "code" => "53",
                "case_code" => ""
            ],
            [
                "name" => "Gray / Classic Gray",
                "chinese_name" => "灰",
                "color_hex" => "#9BA3A5",
                "code" => "54",
                "case_code" => "",
                "active" => false
            ],
            [
                "name" => "Blush Pink",
                "chinese_name" => "粉",
                "color_hex" => "#F6D5C9",
                "code" => "57",
                "case_code" => ""
            ],
            [
                "name" => "Leather",
                "chinese_name" => "皮革",
                "color_hex" => "#202020",
                "code" => "61",
                "case_code" => ""
            ],
            [
                "name" => "Black / HotStamp Gold",
                "chinese_name" => "黑",
                "color_hex" => "#1F1F1F",
                "code" => "B0",
                "case_code" => "52"
            ],
            [
                "name" => "Black / HotStamp Silver",
                "chinese_name" => "黑",
                "color_hex" => "#1F1F1F",
                "code" => "B1",
                "case_code" => "52"
            ],
            [
                "name" => "White / HotStamp Gold",
                "chinese_name" => "白",
                "color_hex" => "#FEFDFC",
                "code" => "B2",
                "case_code" => "53"
            ],
            [
                "name" => "White / HotStamp Silver",
                "chinese_name" => "白",
                "color_hex" => "#FEFDFC",
                "code" => "B3",
                "case_code" => "53"
            ],
            [
                "name" => "Gray / HotStamp Gold",
                "chinese_name" => "灰",
                "color_hex" => "#9BA3A5",
                "code" => "B4",
                "case_code" => "54"
            ],
            [
                "name" => "Gray / HotStamp Silver",
                "chinese_name" => "灰",
                "color_hex" => "#9BA3A5",
                "code" => "B5",
                "case_code" => "54"
            ],
            [
                "name" => "Blush Pink / HotStamp Gold",
                "chinese_name" => "粉",
                "color_hex" => "#F6D5C9",
                "code" => "B6",
                "case_code" => "57"
            ],
            [
                "name" => "Blush Pink / HotStamp Silver",
                "chinese_name" => "粉",
                "color_hex" => "#F6D5C9",
                "code" => "B7",
                "case_code" => "57"
            ],
            [
                "name" => "Leather / HotStamp Gold",
                "chinese_name" => "皮革",
                "color_hex" => "#202020",
                "code" => "B8",
                "case_code" => "61"
            ],
            [
                "name" => "Leather / HotStamp Silver",
                "chinese_name" => "皮革",
                "color_hex" => "#202020",
                "code" => "B9",
                "case_code" => "61"
            ],
            [
                "name" => "Clear / HotStamp Gold",
                "chinese_name" => "透明",
                "color_hex" => "",
                "code" => "A8",
                "case_code" => "26"
            ],
            [
                "name" => "Clear / HotStamp Silver",
                "chinese_name" => "透明",
                "color_hex" => "",
                "code" => "A9",
                "case_code" => "26"
            ],
            [
                "name" => "Royal Blue",
                "chinese_name" => "雀藍",
                "color_hex" => "#2F648C",
                "code" => "77",
                "case_code" => ""
            ],
            [
                "name" => "Clay",
                "chinese_name" => "可可棕",
                "color_hex" => "#8B805E",
                "code" => "E6",
                "case_code" => ""
            ],
            [
                "name" => "Beige",
                "chinese_name" => "卡其",
                "color_hex" => "#B4985B",
                "code" => "E7",
                "case_code" => ""
            ],
            [
                "name" => "Royal Blue / HotStamp Gold",
                "chinese_name" => "雀藍",
                "color_hex" => "#2F648C",
                "code" => "E8",
                "case_code" => "77"
            ],
            [
                "name" => "Royal Blue / HotStamp Silver",
                "chinese_name" => "雀藍",
                "color_hex" => "#2F648C",
                "code" => "E9",
                "case_code" => "77"
            ],
            [
                "name" => "Clay / HotStamp Gold",
                "chinese_name" => "可可棕",
                "color_hex" => "#8B805E",
                "code" => "E0",
                "case_code" => "E6"
            ],
            [
                "name" => "Clay / HotStamp Silver",
                "chinese_name" => "可可棕",
                "color_hex" => "#8B805E",
                "code" => "F0",
                "case_code" => "E6"
            ],
            [
                "name" => "Beige / HotStamp Gold",
                "chinese_name" => "卡其",
                "color_hex" => "#B4985B",
                "code" => "F1",
                "case_code" => "E7"
            ],
            [
                "name" => "Beige / HotStamp Silver",
                "chinese_name" => "卡其",
                "color_hex" => "#B4985B",
                "code" => "F2",
                "case_code" => "E7"
            ],
            [
                "name" => "Charcoal Gray",
                "chinese_name" => "石墨黑",
                "color_hex" => "#575B5F",
                "code" => "H6",
                "case_code" => ""
            ],
            [
                "name" => "Charcoal Gray / HotStamp Gold",
                "chinese_name" => "石墨黑",
                "color_hex" => "#575B5F",
                "code" => "H7",
                "case_code" => "H6"
            ],
            [
                "name" => "Charcoal Gray / HotStamp Silver",
                "chinese_name" => "石墨黑",
                "color_hex" => "#575B5F",
                "code" => "H8",
                "case_code" => "H6"
            ],
            [
                "name" => "Alpine White",
                "chinese_name" => "高山白",
                "color_hex" => "#E2E2E2",
                "code" => "H9",
                "case_code" => ""
            ],
            [
                "name" => "Alpine White / HotStamp Gold",
                "chinese_name" => "高山白",
                "color_hex" => "#E2E2E2",
                "code" => "I0",
                "case_code" => "H9"
            ],
            [
                "name" => "Alpine White / HotStamp Silver",
                "chinese_name" => "高山白",
                "color_hex" => "#E2E2E2",
                "code" => "I1",
                "case_code" => "H9"
            ],
            [
                "name" => "Royal Blue",
                "chinese_name" => "雀藍",
                "color_hex" => "#607790",
                "code" => "I2",
                "case_code" => ""
            ],
            [
                "name" => "Royal Blue / HotStamp Gold",
                "chinese_name" => "雀藍",
                "color_hex" => "#607790",
                "code" => "I3",
                "case_code" => "I2"
            ],
            [
                "name" => "Royal Blue / HotStamp Silver",
                "chinese_name" => "雀藍",
                "color_hex" => "#607790",
                "code" => "I4",
                "case_code" => "I2"
            ],
            [
                "name" => "Shell Pink",
                "chinese_name" => "櫻花粉",
                "color_hex" => "#EAD4C9",
                "code" => "I5",
                "case_code" => ""
            ],
            [
                "name" => "Shell Pink / HotStamp Gold",
                "chinese_name" => "櫻花粉",
                "color_hex" => "#EAD4C9",
                "code" => "I6",
                "case_code" => "I5"
            ],
            [
                "name" => "Shell Pink / HotStamp Silver",
                "chinese_name" => "櫻花粉",
                "color_hex" => "#EAD4C9",
                "code" => "I7",
                "case_code" => "I5"
            ],
            [
                "name" => "Mint Green",
                "chinese_name" => "薄荷綠",
                "color_hex" => "#D2EFD1",
                "code" => "I8",
                "case_code" => ""
            ],
            [
                "name" => "Mint Green / HotStamp Gold",
                "chinese_name" => "薄荷綠",
                "color_hex" => "#D2EFD1",
                "code" => "I9",
                "case_code" => "I8"
            ],
            [
                "name" => "Mint Green / HotStamp Silver",
                "chinese_name" => "薄荷綠",
                "color_hex" => "#D2EFD1",
                "code" => "J0",
                "case_code" => "I8"
            ],
            [
                "name" => "Canary Yellow",
                "chinese_name" => "鵝黃",
                "color_hex" => "#F4D788",
                "code" => "J1",
                "case_code" => ""
            ],
            [
                "name" => "Canary Yellow / HotStamp Gold",
                "chinese_name" => "鵝黃",
                "color_hex" => "#F4D788",
                "code" => "J2",
                "case_code" => "J1"
            ],
            [
                "name" => "Canary Yellow / HotStamp Silver",
                "chinese_name" => "鵝黃",
                "color_hex" => "#F4D788",
                "code" => "J3",
                "case_code" => "J1"
            ],
            [
                "name" => "Navy Blue",
                "chinese_name" => "海軍藍",
                "color_hex" => "#143E70",
                "code" => "J5",
                "case_code" => ""
            ],
            [
                "name" => "Navy Blue / HotStamp Gold",
                "chinese_name" => "海軍藍",
                "color_hex" => "#143E70",
                "code" => "J6",
                "case_code" => "J5"
            ],
            [
                "name" => "Navy Blue / HotStamp Silver",
                "chinese_name" => "海軍藍",
                "color_hex" => "#143E70",
                "code" => "J7",
                "case_code" => "J5"
            ],
            [
                "name" => "(Hans專用)黑/黑色荔枝皮 / HotStamp Gold",
                "chinese_name" => "(Hans專用)黑/黑色荔枝皮",
                "color_hex" => "#1A1A1D",
                "code" => "J8",
                "case_code" => "52"
            ],
            [
                "name" => "(Hans專用)黑/黑色鱷魚皮 / HotStamp Silver",
                "chinese_name" => "(Hans專用)黑/黑色鱷魚皮",
                "color_hex" => "#212329",
                "code" => "J9",
                "case_code" => "52"
            ],
            [
                "name" => "(Hans專用)海軍藍/藍色荔枝皮 / HotStamp Silver",
                "chinese_name" => "(Hans專用)海軍藍/藍色荔枝皮",
                "color_hex" => "#263357",
                "code" => "K0",
                "case_code" => "J5"
            ],
            [
                "name" => "(Hans專用)黑/棕色荔枝皮 / HotStamp Gold",
                "chinese_name" => "(Hans專用)黑/棕色荔枝皮",
                "color_hex" => "#976831",
                "code" => "K1",
                "case_code" => "52"
            ],
            [
                "name" => "(Hans專用)可可棕/大象灰荔枝皮 - HotStamp Silver",
                "chinese_name" => "(Hans專用)可可棕/大象灰荔枝皮",
                "color_hex" => "#A09D96",
                "code" => "K2",
                "case_code" => "E6"
            ],
            [
                "name" => "Dark Teal",
                "chinese_name" => "暗夜綠",
                "color_hex" => "#39444A",
                "code" => "K8",
                "case_code" => ""
            ],
            [
                "name" => "Poppy Orange",
                "chinese_name" => "橙紅",
                "color_hex" => "#C65F4E",
                "code" => "K9",
                "case_code" => ""
            ],
            [
                "name" => "Sand Beige",
                "chinese_name" => "奶茶",
                "color_hex" => "#CEBFAA",
                "code" => "L0",
                "case_code" => ""
            ],
            [
                "name" => "Berry Red",
                "chinese_name" => "血石紅",
                "color_hex" => "#A94D58",
                "code" => "L1",
                "case_code" => ""
            ],
            [
                "name" => "Dark Teal / HotStamp Gold",
                "chinese_name" => "暗夜綠",
                "color_hex" => "#39444A",
                "code" => "L2",
                "case_code" => "K8"
            ],
            [
                "name" => "Dark Teal / HotStamp Silver",
                "chinese_name" => "暗夜綠",
                "color_hex" => "#39444A",
                "code" => "L3",
                "case_code" => "K8"
            ],
            [
                "name" => "Poppy Orange / HotStamp Gold",
                "chinese_name" => "橙紅",
                "color_hex" => "#C65F4E",
                "code" => "L4",
                "case_code" => "K9"
            ],
            [
                "name" => "Poppy Orange / HotStamp Silver",
                "chinese_name" => "橙紅",
                "color_hex" => "#C65F4E",
                "code" => "L5",
                "case_code" => "K9"
            ],
            [
                "name" => "Sand Beige / HotStamp Gold",
                "chinese_name" => "奶茶",
                "color_hex" => "#CEBFAA",
                "code" => "L6",
                "case_code" => "L0"
            ],
            [
                "name" => "Sand Beige / HotStamp Silver",
                "chinese_name" => "奶茶",
                "color_hex" => "#CEBFAA",
                "code" => "L7",
                "case_code" => "L0"
            ],
            [
                "name" => "Berry Red / HotStamp Gold",
                "chinese_name" => "血石紅",
                "color_hex" => "#A94D58",
                "code" => "L8",
                "case_code" => "L1"
            ],
            [
                "name" => "Berry Red / HotStamp Silver",
                "chinese_name" => "血石紅",
                "color_hex" => "#A94D58",
                "code" => "L9",
                "case_code" => "L1"
            ],
            [
                "name" => "Topaz Yellow",
                "chinese_name" => "黃玉黃",
                "color_hex" => "#D38005",
                "code" => "F3",
                "case_code" => ""
            ],
            [
                "name" => "Ruby Red",
                "chinese_name" => "寶石紅",
                "color_hex" => "#7A1212",
                "code" => "F4",
                "case_code" => ""
            ],
            [
                "name" => "Moonstone Gray",
                "chinese_name" => "月光石灰",
                "color_hex" => "#857C79",
                "code" => "F5",
                "case_code" => ""
            ],
            [
                "name" => "Amber Orange",
                "chinese_name" => "琥珀橙",
                "color_hex" => "#FF7E00",
                "code" => "F6",
                "case_code" => ""
            ],
            [
                "name" => "Sapphire Blue",
                "chinese_name" => "寶石藍",
                "color_hex" => "#024189",
                "code" => "F7",
                "case_code" => ""
            ],
            [
                "name" => "Rose Quartz",
                "chinese_name" => "玫瑰石英",
                "color_hex" => "#E5B1A6",
                "code" => "F8",
                "case_code" => ""
            ],
            [
                "name" => "Crystal Clear",
                "chinese_name" => "透明",
                "color_hex" => "",
                "code" => "F9",
                "case_code" => ""
            ],
            [
                "name" => "Emerald Green",
                "chinese_name" => "翡翠綠",
                "color_hex" => "#133835",
                "code" => "G0",
                "case_code" => ""
            ],
            [
                "name" => "Citrine Yellow",
                "chinese_name" => "黃水晶黃",
                "color_hex" => "#E3CF09",
                "code" => "G1",
                "case_code" => ""
            ]
        ];

        foreach ($data as $row) {
            $colorPayload = [
                'english_name' => $row['name'],
                'chinese_name' => Arr::get($row, 'chinese_name', ''),
                'color_hex' => $row['color_hex'],
                'code' => $row['code'],
                'origin_code' => $row['case_code'],
                'active' => Arr::get($row, 'active', true)
            ];
            $colorModel = Color::firstOrCreate(['code' => $colorPayload['code']]);
            $colorModel->update($colorPayload);
        }
    }
}
