<?php

namespace App\Repositories;

use App\Repositories\Repository;
use App\Models\CaseTypeItem;
use App\Transformers\CaseType\CaseTypeItemTransformer;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class CaseTypeItemRepository extends Repository
{
    protected $model = CaseTypeItem::class;

    protected $transformer = CaseTypeItemTransformer::class;

    protected $relations = ['caseType'];

    public function getCaseTypeByCode($code)
    {
        $model = $this->model::where('code', $code)
            ->with($this->relations)
            ->first();

        if (is_null($model)) {
            throw new ModelNotFoundException("找不到資料:{$code}");
        }

        return app($this->transformer)->exec($model);
    }
}
