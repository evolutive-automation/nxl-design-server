<?php

namespace App\Providers;

use App\Events\NotifyCheckedDesignEvent;
use App\Listeners\MergeCaseAndMovePrintingRoomListener;
use App\Listeners\SendDesignToCustomizedServerListener;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        NotifyCheckedDesignEvent::class => [
            SendDesignToCustomizedServerListener::class,
            MergeCaseAndMovePrintingRoomListener::class
        ]
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
