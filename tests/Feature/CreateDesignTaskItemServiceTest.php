<?php

namespace Tests\Feature;

use App\Jobs\SavePendingFileJob;
use App\Services\CreateDesignTaskItemService;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\DesignTestCase;
use Carbon\Carbon;
use Illuminate\Support\Facades\Queue;

class CreateDesignTaskItemServiceTest extends DesignTestCase
{
    use WithFaker, RefreshDatabase, DatabaseMigrations;

    public function test_it_can_create_design_task_item()
    {
        $fakeDesignTask = $this->createFakeDesignTask('pending');
        $task = $fakeDesignTask['task'];
        $task['payload'] = json_decode($task['payload'], true);
        $payload = $task['payload'];

        $taskItems = app(CreateDesignTaskItemService::class)
            ->setTask($task)
            ->exec();

        $this->assertDatabaseHas('tasks', [
            'id' => $task['id'],
            'status' => 'making',
            'quantity' => count($taskItems),
            'started_at' => Carbon::now()
        ]);

        foreach ($taskItems as $taskItem) {
            $this->assertDatabaseHas('task_items', [
                'task_id' => $taskItem['task_id'],
                'design_id' => $taskItem['design_id'],
                'material_print_path' => $taskItem['material_print_path'],
                'material_web_path' => $taskItem['material_web_path']
            ]);
        }
        foreach ($payload['devices'] as $deviceCode) {
            foreach ($payload['colors'] as $colorCode) {
                $this->assertDatabaseHas('designs', [
                    'design_material_id' => $fakeDesignTask['designMaterial']['id'],
                    'case_type' => $payload['case_type'],
                    'color_code' => $colorCode,
                    'device_code' => $deviceCode,
                    'sort' => $payload['sort'],
                    'code' => $payload['code'],
                    'print_type' => $payload['print_type'],
                ]);
            }
        }
    }
}
