<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\TaskItemFailJob;
use Exception;

class RetryJob extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'retry:job';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '重跑 failed job';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $start = microtime(true);
        $failJobs = TaskItemFailJob::select(['job_name', 'payload'])
            ->where('job_name', '<>', 'App\Listeners\SendDesignToCustomizedServerListener')
            ->get();
        foreach ($failJobs as $failJob) {
            try {
                if (is_null($failJob->payload)) {
                    continue;
                }
                $payload = json_decode($failJob->payload, true);
                dispatch(new $failJob->job_name($payload))
                    ->onQueue('design-default');
            } catch (Exception $e) {
                continue;
            }
        }
        $end = microtime(true);
        $total = round($end - $start, 2);
        echo "重跑Job成功，總花費{$total}秒\n";
    }
}
