<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\SlackMessage;
use Illuminate\Notifications\Notification;

class SlackNotification extends Notification
{
    use Queueable;

    /** @var string $title */
    private $title;

    /** @var string $message */
    private $message;

    /** @var array $fields */
    private $fields;

    /** @var string $color */
    private $color;

    /** @var array $tags */
    private $tags;

    /** @var string $channel */
    private $channel;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['slack'];
    }

    /**
     * Get the Slack representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\SlackMessage
     */
    public function toSlack($notification)
    {
        $color = is_null($this->color)? 'error' : $this->color;
        $tag = (is_null($this->tags) || empty($this->tags))? config('slack.tags.channel') : implode('', $this->tags);

        return (new SlackMessage)
            ->{$color}()
            ->from('RD自動化', ':ditto:')
            ->to($this->channel)
            ->content($tag)
            ->attachment(function ($attachment) {
                $attachment->title($this->title)
                    ->content($this->message);
                if (!is_null($this->fields) && is_array($this->fields)) {
                    $attachment->color('#2eb886');
                    $attachment->fields($this->fields);
                }
            });
    }

    public function setChannel($channel)
    {
        $this->channel = $channel;

        return $this;
    }

    public function setTags($tags)
    {
        $this->tags = $tags;

        return $this;
    }

    public function setColor($color)
    {
        $this->color = $color;

        return $this;
    }

    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    public function setMessage($message)
    {
        $this->message = $message;

        return $this;
    }

    public function setFields($fields)
    {
        $this->fields = $fields;

        return $this;
    }
}
