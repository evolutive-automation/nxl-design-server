<?php

namespace App\Listeners;

use App\Events\NotifyCheckedDesignEvent;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use App\Repositories\TaskItemFailJobRepository;
use App\Services\MergeCaseAndMoveFileService;
use Illuminate\Validation\ValidationException;
use Exception;
use Illuminate\Support\Arr;

class MergeCaseAndMovePrintingRoomListener implements ShouldQueue
{
    public $connection = 'redis';

    public $queue = 'design-merge-case';

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
    }

    /**
     * Handle the event.
     *
     * @param  NotifyCheckedDesignEvent  $event
     * @return void
     */
    public function handle(NotifyCheckedDesignEvent $event)
    {
        $payload = $event->payload;
        try {
            app(MergeCaseAndMoveFileService::class)
                ->setPayload($payload)
                ->exec();
        } catch (Exception $e) {
            $this->logError($e, $payload);
        }
    }

    private function logError($e, $payload)
    {
        $error_message = $e->getMessage();
        if ($e instanceof ValidationException) {
            $error_message = json_encode($e->validator->errors());
        }
        $form = [
            'job_name' => get_class($this),
            'task_item_id' => Arr::get($payload, 'id', null),
            'payload' => json_encode($payload),
            'error_message' => $error_message
        ];
        app(TaskItemFailJobRepository::class)
            ->createOrUpdate($form, ['job_name', 'task_item_id']);
    }
}
