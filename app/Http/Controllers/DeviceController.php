<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Services\GetDeviceService;
use Illuminate\Http\Request;

class DeviceController extends Controller
{
    public function search(Request $request)
    {
        $data = app(GetDeviceService::class)
            ->setArgs($request->all())
            ->exec();

        return ok(['data' => $data]);
    }
}
