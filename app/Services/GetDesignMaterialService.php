<?php

namespace App\Services;

use App\Repositories\DesignMaterialRepository;
use App\Services\Service;

class GetDesignMaterialService extends Service
{
    /** @var DesignMaterialRepository $designMaterialRepository */
    protected $designMaterialRepository;

    public function __construct(
        DesignMaterialRepository $designMaterialRepository
    ) {
        $this->designMaterialRepository = $designMaterialRepository;
    }

    public function exec()
    {
        return $this->getDesignMaterial();
    }

    private function getDesignMaterial()
    {
        $data = $this->designMaterialRepository->search([]);

        $defaultSort = [['caseType', 'asc'], ['version', 'asc']];
        return collect($data)->sortBy($defaultSort)->values()->toArray();
    }
}
