<?php

namespace App\Services;

use App\Repositories\DeviceRepository;
use App\Services\Service;

class GetDeviceService extends Service
{
    /** @var array $args */
    protected $args;

    /** @var DeviceRepository $deviceRepository */
    protected $deviceRepository;

    public function __construct(
        DeviceRepository $deviceRepository
    ) {
        $this->deviceRepository = $deviceRepository;
    }

    public function setArgs($args)
    {
        $this->args = $args;

        return $this;
    }

    public function exec()
    {
        return $this->deviceRepository->search($this->args);
    }
}
