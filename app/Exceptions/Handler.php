<?php

namespace App\Exceptions;

use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Validation\ValidationException;
use Throwable;
use Symfony\Component\HttpKernel\Exception\HttpExceptionInterface;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Register the exception handling callbacks for the application.
     *
     * @return void
     */
    public function register()
    {
        $this->renderable(function (Throwable $e, $request) {
            if ($e instanceof ValidationException) {
                return response()->json([
                    'ok' => false,
                    'messages' => $e->validator->errors()
                ], 422);
            } else {
                $statusCode = ($e instanceof HttpExceptionInterface) ? $e->getStatusCode() : 500;
                return response()->json([
                    'ok' => false,
                    'message' => $e->getMessage()
                ], $statusCode);
            }
        });

        $this->reportable(function (Throwable $e) {
            //
        });
    }
}
