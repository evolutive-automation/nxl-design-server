<?php

namespace App\Models;

use App\Models\Model;
use App\Models\CaseType;

class CaseTypeItem extends Model
{
    public $table = 'case_type_items';

    public function caseType()
    {
        return $this->belongsTo(CaseType::class, 'type', 'name');
    }
}
