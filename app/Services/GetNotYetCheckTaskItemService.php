<?php

namespace App\Services;

use App\Repositories\TaskItemRepository;
use App\Services\Service;
use App\Transformers\TaskItem\MoveTaskItemTransformer;

class GetNotYetCheckTaskItemService extends Service
{
    /** @var int $taskId */
    protected $taskId;

    /** @var TaskItemRepository $taskItemRepository */
    protected $taskItemRepository;

    public function __construct(
        TaskItemRepository $taskItemRepository
    ) {
        $this->taskItemRepository = $taskItemRepository;
    }

    public function setTaskId($taskId)
    {
        $this->taskId = $taskId;

        return $this;
    }

    public function exec()
    {
        $items = $this->getTaskItems();

        return $this->sortTaskItems($items);
    }

    private function getTaskItems()
    {
        return $this->taskItemRepository
            ->setRelation(['design.material', 'design.device'])
            ->setTransformer(MoveTaskItemTransformer::class)
            ->getByTaskId($this->taskId);
    }

    private function sortTaskItems($items)
    {
        $defaultSort = [['design.sort', 'asc'], ['design.code', 'asc'], ['design.deviceCode', 'asc']];
        return collect($items)
            ->sortBy($defaultSort)
            ->toArray();
    }
}
