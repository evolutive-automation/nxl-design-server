<?php

namespace App\Repositories;

use App\Repositories\Repository;
use App\Models\Design;
use App\Transformers\Design\DesignTransformer;

class DesignRepository extends Repository
{
    protected $model = Design::class;

    protected $transformer = DesignTransformer::class;

    public function findDesign($caseType, $deviceCode, $colorCode, $code)
    {
        $model = $this->model::where('case_type', $caseType)
            ->where('device_code', $deviceCode)
            ->where('color_code', $colorCode)
            ->where('code', $code)
            ->first();

        return is_null($model)? null : app($this->transformer)->exec($model);
    }

    public function getDesigns($caseType, $code, $devices, $colors)
    {
        $collection = $this->model::where('case_type', $caseType)
            ->where('code', $code)
            ->whereIn('device_code', $devices)
            ->whereIn('color_code', $colors)
            ->get();

        $transformer = app($this->transformer);

        return $collection->map(fn ($row) => $transformer->exec($row))->toArray();
    }

    public function getDesignByMaterial($caseType, $colors, $version)
    {
        $collection = $this->model::where('case_type', $caseType)
            ->whereIn('color_code', $colors)
            ->whereHas('material', function ($q) use ($version) {
                return $q->where('version', $version);
            })
            ->distinct()
            ->get([
                'design_material_id', 'case_type', 'color_code',
                'sort', 'code', 'print_type'
            ]);

        $transformer = app($this->transformer);

        return $collection->map(fn ($row) => $transformer->exec($row))->toArray();
    }
}
