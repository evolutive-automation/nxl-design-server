<?php

namespace App\Exceptions;

use Exception;

class DuplicateDeviceException extends Exception
{
    protected $message;

    public function __construct($message = 'duplicate device exception.')
    {
        parent::__construct($message);
    }

    public function getStatusCode()
    {
        return 400;
    }
}
