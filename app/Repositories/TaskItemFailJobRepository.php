<?php

namespace App\Repositories;

use App\Models\TaskItemFailJob;
use App\Repositories\Repository;
use App\Transformers\TaskItemFailJob\TaskItemFailJobTransformer;

class TaskItemFailJobRepository extends Repository
{
    protected $model = TaskItemFailJob::class;

    protected $transformer = TaskItemFailJobTransformer::class;

    public function deleteByJobKey($jobName, $jobKey)
    {
        return $this->model::where('job_name', $jobName)
            ->where('job_key', $jobKey)
            ->delete();
    }
}
