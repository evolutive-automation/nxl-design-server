<?php

namespace App\Services;

use App\Jobs\CreateTaskItemJob;
use App\Repositories\TaskRepository;
use App\Services\Service;

class MarkAsUrgentTaskService extends Service
{
    /** @var int $taskId */
    protected $taskId;

    /** @var TaskRepository $taskRepository */
    protected $taskRepository;

    public function __construct(
        TaskRepository $taskRepository
    ) {
        $this->taskRepository = $taskRepository;
    }

    public function setTaskId($taskId)
    {
        $this->taskId = $taskId;

        return $this;
    }

    public function exec()
    {
        $task = $this->getTask();
        $this->dispatchJob($task);
    }

    private function getTask()
    {
        return $this->taskRepository->findOrFail($this->taskId);
    }

    private function dispatchJob($task)
    {
        dispatch(new CreateTaskItemJob($task))
            ->onQueue('design-default');
    }
}
