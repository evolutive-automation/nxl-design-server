<?php

namespace App\Models;

use App\Models\Model;

class ExternalApiLog extends Model
{
    public $table = 'external_api_logs';
}
