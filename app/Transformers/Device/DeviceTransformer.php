<?php

namespace App\Transformers\Device;

use App\Models\Device;
use App\Transformers\Transformer;

class DeviceTransformer extends Transformer
{
    /**
     * @param Device $deviceModel
     *
     * @return array
     */
    public function exec($deviceModel)
    {
        return [
            'type' => $this->getType($deviceModel),
            'brandCode' => $deviceModel->brand_code,
            'name' => $deviceModel->name,
            'code' => $deviceModel->code,
            'haveShadow' => $deviceModel->have_shadow? true : false,
        ];
    }

    private function getType($deviceModel)
    {
        $result = '';
        if ($deviceModel->brand_code === '01') {
            $result = 'iphone';
        } else {
            $result = 'android';
        }
        if (in_array($deviceModel->code, ['170', '171'])) {
            $result = 'airpods';
        }
        return $result;
    }
}
