<?php

namespace App\Transformers\CaseType;

use App\Models\CaseTypeItem;
use App\Transformers\Transformer;

class CaseTypeItemTransformer extends Transformer
{
    /**
     * @param CaseTypeItem $caseTypeItemModel
     *
     * @return array
     */
    public function exec($caseTypeItemModel)
    {
        return [
            'type' => $caseTypeItemModel->type,
            'code' => $caseTypeItemModel->code,
            'pack' => $caseTypeItemModel->pack
        ];
    }
}
