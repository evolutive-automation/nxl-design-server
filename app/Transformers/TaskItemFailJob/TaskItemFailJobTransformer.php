<?php

namespace App\Transformers\TaskItemFailJob;

use App\Transformers\Transformer;
use App\Models\TaskItemFailJob;

class TaskItemFailJobTransformer extends Transformer
{
    /**
     * @param TaskItemFailJob $taskItemFailJobModel
     *
     * @return array
     */
    public function exec($taskItemFailJobModel)
    {
        return [
            'jobName' => $taskItemFailJobModel->job_name,
            'taskItemId' => $taskItemFailJobModel->task_item_id,
            'errorMessage' => $taskItemFailJobModel->error_message
        ];
    }
}
