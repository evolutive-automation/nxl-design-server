<?php

namespace Tests\Feature;

use App\Models\DesignMaterial;
use App\Models\Device;
use App\Services\CreateDesignTaskService;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\DesignTestCase;

class CreateDesignTaskServiceTest extends DesignTestCase
{
    use WithFaker, RefreshDatabase, DatabaseMigrations;

    public function test_it_can_create_design_type_task_by_specific_device()
    {
        $this->createMetaData();
        $payload = [
            'version' => 1,
            'case_type' => 'ModNX',
            'colors' => ['26'],
            'devices' => ['054','055'],
            'sort' => 'OB',
            'code' => 'OB01',
            'print_type' => 'CW',
            'created_user' => 'tony',
            'material_print' => $this->mockUploadFile('material-print-OB01.pdf', 'application/pdf'),
            'material_web' => $this->mockUploadFile('material-web-OB01.png', 'image/png')
        ];
        $colorText = implode(',', $payload['colors']);
        $deviceText = implode(',', $payload['devices']);
        $expectedName = "{$payload['code']}-{$payload['print_type']}-{$deviceText}-{$colorText}-{$payload['case_type']}";
        app(CreateDesignTaskService::class)
            ->setPayload($payload)
            ->exec();
        $temp = $payload;
        unset($temp['material_print']);
        unset($temp['material_web']);
        $this->assertDatabaseHas('tasks', [
            'type' => 'design',
            'status' => 'pending',
            'name' => $expectedName,
            'payload' => json_encode($temp),
            'quantity' => 0,
            'done_quantity' => 0
        ]);
        $filename = "ModNX-{$payload['code']}-1";
        $this->assertDatabaseHas('design_materials', [
            'filename' => $filename,
        ]);
        $this->assertTrue(file_exists(env('DesignFileSourceDir')."/material/print/{$filename}.pdf"));
        $this->assertTrue(file_exists(env('DesignFileSourceDir')."/material/web/{$filename}.png"));
    }

    public function test_it_can_create_design_type_task_by_iphone_all()
    {
        $this->createMetaData();
        $payload = [
            'version' => 1,
            'case_type' => 'ModNX',
            'colors' => ['26'],
            'devices' => ['iPhone全部'],
            'sort' => 'OB',
            'code' => 'OB01',
            'print_type' => 'CW',
            'created_user' => 'tony',
            'material_print' => $this->mockUploadFile('material-print-OB01.pdf', 'application/pdf'),
            'material_web' => $this->mockUploadFile('material-web-OB01.png', 'image/png')
        ];
        $colorText = implode(',', $payload['colors']);
        $deviceText = implode(',', $payload['devices']);
        $expectedName = "{$payload['code']}-{$payload['print_type']}-{$deviceText}-{$colorText}-{$payload['case_type']}";
        app(CreateDesignTaskService::class)
            ->setPayload($payload)
            ->exec();
        $temp = $payload;
        unset($temp['material_print']);
        unset($temp['material_web']);
        $this->assertDatabaseHas('tasks', [
            'type' => 'design',
            'status' => 'pending',
            'name' => $expectedName,
            'payload' => json_encode($temp),
            'quantity' => 0,
            'done_quantity' => 0
        ]);
        $filename = "ModNX-{$payload['code']}-1";
        $this->assertDatabaseHas('design_materials', [
            'filename' => $filename,
        ]);
        $this->assertTrue(file_exists(env('DesignFileSourceDir')."/material/print/{$filename}.pdf"));
        $this->assertTrue(file_exists(env('DesignFileSourceDir')."/material/web/{$filename}.png"));
    }
}
