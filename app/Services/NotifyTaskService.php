<?php

namespace App\Services;

use App\Repositories\TaskRepository;
use App\Services\Service;
use Carbon\Carbon;
use Illuminate\Support\Facades\Notification;
use App\Notifications\SlackNotification;

class NotifyTaskService extends Service
{
    /** @var TaskRepository $taskRepository */
    protected $taskRepository;

    public function __construct(
        TaskRepository $taskRepository
    ) {
        $this->taskRepository = $taskRepository;
    }

    public function exec()
    {
        $tasks = $this->getTasks();
        $notifiedTasks = $this->notifySlack($tasks);
        $this->updateTasks($notifiedTasks);
    }

    private function getTasks()
    {
        return $this->taskRepository->getNeedNotifyTasks();
    }

    private function notifySlack($tasks)
    {
        $defaultSort = [['endedAt', 'asc']];
        $sortData = collect($tasks)->sortBy($defaultSort)->values()->toArray();
        $now = Carbon::now();
        $data = [];
        foreach ($sortData as $row) {
            $message = '';
            $color = 'error';
            if (is_null($row['endedAt'])) {
                $minutes = $now->diffInMinutes(Carbon::parse($row['startedAt'], 'Asia/Taipei'));
                if ($minutes >= 5 && $row['doneQuantity'] > 0) {
                    $message = "任務[{$row['name']}]距離開始執行超過5分鐘，請注意是否卡住";
                    array_push($data, $row);
                }
            } else {
                $color = 'success';
                $message = "任務[{$row['name']}]已經製作完成，花費時間:{$row['spentTime']}";
                array_push($data, $row);
            }
            if ($message !== '') {
                $user = $row['createdUser'];
                $tags = [config("slack.tags.{$user}", '<!channel>')];
                $slackNotification = app(SlackNotification::class)
                    ->setTags($tags)
                    ->setChannel('#學樺995')
                    ->setTitle("任務回報")
                    ->setColor($color)
                    ->setMessage($message);
                Notification::route('slack', env('SLACK_URL'))
                    ->notify($slackNotification);
            }
        }
        return $data;
    }

    private function updateTasks($tasks)
    {
        $payload = [
            'notified_at' => Carbon::now()->toDateTimeString()
        ];

        $ids = collect($tasks)->pluck('id')->toArray();

        $this->taskRepository->updateMany($ids, $payload);
    }
}
