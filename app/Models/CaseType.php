<?php

namespace App\Models;

use App\Models\Model;
use App\Models\CaseTypeItem;

class CaseType extends Model
{
    public $table = 'case_types';

    public function items()
    {
        return $this->hasMany(CaseTypeItem::class, 'type', 'name');
    }
}
