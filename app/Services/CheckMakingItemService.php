<?php

namespace App\Services;

use App\Jobs\DeletePendingFileJob;
use App\Repositories\TaskItemRepository;
use App\Repositories\TaskRepository;
use App\Services\Service;
use App\Transformers\TaskItem\CheckMakedTransformer;
use App\Transformers\TaskItem\DetailTaskItemTransformer;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class CheckMakingItemService extends Service
{
    /** @var TaskRepository $taskRepository */
    protected $taskRepository;

    /** @var TaskItemRepository $taskItemRepository */
    protected $taskItemRepository;

    public function __construct(
        TaskRepository $taskRepository,
        TaskItemRepository $taskItemRepository
    ) {
        $this->taskRepository = $taskRepository;
        $this->taskItemRepository = $taskItemRepository;
    }

    public function exec()
    {
        $existsItems = DB::transaction(function () {
            $taskItems = $this->getMakingTaskItems();
            $existsItems = $this->getExistsItems($taskItems);
            $this->updateTaskItems($existsItems);
            $this->updateTasks($existsItems);
            return $existsItems;
        });
        $this->deletePendingFile($existsItems);
    }

    private function getMakingTaskItems()
    {
        return $this->taskItemRepository
            ->setRelation(['design.material'])
            ->setTransformer(CheckMakedTransformer::class)
            ->getMakingTaskItems();
    }

    private function getExistsItems($taskItems)
    {
        $items = [];
        foreach ($taskItems as $taskItem) {
            $taskItem['isChanged'] = false;
            $havePrint = $this->checkOutputFile($taskItem, 'print');
            $haveWeb = $this->checkOutputFile($taskItem, 'web');
            if ($havePrint) {
                $taskItem['havePrint'] = true;
                $taskItem['isChanged'] = true;
            }
            if ($haveWeb) {
                $taskItem['haveWeb'] = true;
                $taskItem['isChanged'] = true;
            }
            if ($taskItem['isChanged']) {
                array_push($items, $taskItem);
            }
        }
        return $items;
    }

    private function checkOutputFile($taskItem, $type)
    {
        $folder = env('DesignFileSourceDir'). "/output/{$type}";
        $filename = ($type === 'print')? basename($taskItem['materialPrintPath']) : basename($taskItem['materialWebPath']);
        if ($type === 'web') {
            $filename = str_replace(['-0.png', '-1.png'], '.png', $filename);
            $filename = str_replace(['-K.png','-W.png','-CW.png'], '.png', $filename);
        }
        $outputPath = "{$folder}/{$filename}";

        $fileExists = file_exists($outputPath);

        if ($fileExists && filesize($outputPath) === 0) {
            $fileExists = false;
            $this->reCreatePendingFile($taskItem, $type);
        }

        return $fileExists;
    }

    private function updateTaskItems($taskItems)
    {
        $itemIds = collect($taskItems)->where('havePrint', true)->pluck('id')->toArray();
        $this->taskItemRepository->updateMany($itemIds, ['have_print' => true]);

        $itemIds = collect($taskItems)->where('haveWeb', true)->pluck('id')->toArray();
        $this->taskItemRepository->updateMany($itemIds, ['have_web' => true]);
    }

    private function updateTasks($taskItems)
    {
        $tasks = collect($taskItems)
            ->filter(function ($item) {
                return ($item['havePrint'] && $item['haveWeb']);
            })
            ->groupBy('taskId')
            ->map(function ($groups) {
                $task = $groups[0]['task'];
                $task['doneQuantity'] += count($groups);
                $quantity = $task['quantity'];
                $doneQuantity = $task['doneQuantity'];
                $ngQuantity = $task['ngQuantity'];
                if ($doneQuantity > $quantity) {
                    $doneQuantity = $quantity;
                }
                return [
                    "id" => $groups[0]['taskId'],
                    "status" => (($doneQuantity + $ngQuantity) >= $quantity)? 'made' : $task['status'],
                    "doneQuantity" => $doneQuantity
                ];
            })
            ->values()
            ->toArray();
        $now = Carbon::now();
        foreach ($tasks as $task) {
            $payload = [
                'status' => $task['status'],
                'done_quantity' => $task['doneQuantity']
            ];
            if ($payload['status'] === 'made') {
                $payload['ended_at'] = $now;
            }
            $this->taskRepository->update($task['id'], $payload);
        }
    }

    private function deletePendingFile($taskItems)
    {
        foreach ($taskItems as $taskItem) {
            dispatch(new DeletePendingFileJob($taskItem))
                ->onQueue('design-default');
        }
    }

    private function reCreatePendingFile($taskItem, $type)
    {
        $pendingPath = ($type === 'print')? $taskItem['materialPrintPath'] : $taskItem['materialWebPath'];
        $materialPath = ($type === 'print')? $taskItem['material']['printPath'] : $taskItem['material']['webPath'];

        $destPath = env('DesignFileSourceDir')."/{$pendingPath}";
        $sourcePath = env('DesignFileSourceDir')."/{$materialPath}";

        if (file_exists($destPath)) {
            unlink($destPath);
        }
        file_put_contents($destPath, file_get_contents($sourcePath));
    }
}
