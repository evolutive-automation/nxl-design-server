<?php

namespace App\Services;

use App\Exceptions\ZipException;
use App\Repositories\TaskItemRepository;
use App\Services\Service;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use ZipArchive;

class GenerateNotAllowedZipService extends Service
{
    /** @var int $taskId */
    protected $taskId;

    /** @var TaskItemRepository $taskItemRepository */
    protected $taskItemRepository;

    public function __construct(
        TaskItemRepository $taskItemRepository
    ) {
        $this->taskItemRepository = $taskItemRepository;
    }

    public function setTaskId($taskId)
    {
        $this->taskId = $taskId;

        return $this;
    }

    public function exec()
    {
        $taskItems = $this->getTaskItems();
        if (empty($taskItems)) {
            throw new ModelNotFoundException("該任務{$this->taskId}目前沒有不出的設計");
        }
        return $this->packIntoZip($taskItems);
    }

    private function getTaskItems()
    {
        return $this->taskItemRepository->getNotAllowedByTaskId($this->taskId);
    }

    private function packIntoZip($taskItems)
    {
        $zipPath = storage_path('app/public')."/Task{$this->taskId}.zip";
        $zip = new ZipArchive();
        $res = $zip->open($zipPath, ZipArchive::CREATE);
        if (!$res) {
            throw new ZipException("錯誤，無法建立 Zip 壓縮檔");
        }
        foreach ($taskItems as $taskItem) {
            $webPath = env('DesignFileSourceDir')."/{$taskItem['outputWebPath']}";
            if (file_exists($webPath)) {
                $zip->addFile($webPath, basename($webPath));
            }
        }
        $zip->close();

        return $zipPath;
    }
}
