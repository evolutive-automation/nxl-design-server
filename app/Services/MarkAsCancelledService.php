<?php

namespace App\Services;

use App\Exceptions\CantCancelledTaskException;
use App\Repositories\TaskItemRepository;
use App\Repositories\TaskRepository;
use App\Services\Service;
use Carbon\Carbon;

class MarkAsCancelledService extends Service
{
    /** @var int $taskId */
    protected $taskId;

    /** @var TaskRepository $taskRepository */
    protected $taskRepository;

    /** @var TaskItemRepository $taskItemRepository */
    protected $taskItemRepository;

    public function __construct(
        TaskRepository $taskRepository,
        TaskItemRepository $taskItemRepository
    ) {
        $this->taskRepository = $taskRepository;
        $this->taskItemRepository = $taskItemRepository;
    }

    public function setTaskId($taskId)
    {
        $this->taskId = $taskId;

        return $this;
    }

    public function exec()
    {
        $task = $this->getTask();
        if ($task['status'] === 'checked') {
            throw new CantCancelledTaskException("該任務[{$task['id']}]已經審核完成，無法被取消");
        }
        $this->updateTask();
        $this->updateTaskItem();
        $taskItems = $this->getTaskItem();
        $this->deleteFiles($taskItems);
    }

    private function getTask()
    {
        return $this->taskRepository->findOrFail($this->taskId);
    }

    private function updateTask()
    {
        $payload = [
            'status' => 'cancelled',
            'ended_at' => Carbon::now()->toDateTimeString()
        ];

        $this->taskRepository->update($this->taskId, $payload);
    }

    private function updateTaskItem()
    {
        $payload = [
            'is_allowed' => false
        ];

        $this->taskItemRepository->updateByTaskId($this->taskId, $payload);
    }

    private function getTaskItem()
    {
        return $this->taskItemRepository->getByTaskId($this->taskId);
    }

    private function deleteFiles($taskItems)
    {
        foreach ($taskItems as $taskItem) {
            $path = env('DesignFileSourceDir'). "/{$taskItem['materialPrintPath']}";
            if (file_exists($path)) {
                unlink($path);
            }
            $path = env('DesignFileSourceDir'). "/{$taskItem['materialWebPath']}";
            if (file_exists($path)) {
                unlink($path);
            }
            if ($taskItem['havePrint']) {
                $path = env('DesignFileSourceDir'). "/{$taskItem['outputPrintPath']}";
                if (file_exists($path)) {
                    unlink($path);
                }
            }
            if ($taskItem['haveWeb']) {
                $path = env('DesignFileSourceDir'). "/{$taskItem['outputWebPath']}";
                if (file_exists($path)) {
                    unlink($path);
                }
            }
        }
    }
}
