<?php

namespace App\Console\Commands;

use App\Models\DesignMaterial;
use App\Models\Design;
use Illuminate\Console\Command;
use Libs\CustomizedServer\DesignService;

class SyncDesign extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sync:design {caseType} {deviceCode} {version}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '同步設計';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $start = microtime(true);
        $caseType = $this->argument('caseType');
        $deviceCode = $this->argument('deviceCode');
        $version = $this->argument('version');
        $materials = DesignMaterial::where('case_type', $caseType)
            ->where('version', $version)
            ->get()
            ->map(function ($item) {
                return [
                    'id' => $item->id,
                    'filename' => $item->filename
                ];
            })
            ->keyBy('filename')
            ->toArray();
        $filenames = collect($materials)->keys()->toArray();
        $form = [
            'deviceCode' => $deviceCode,
            'filenames' => $filenames
        ];
        $data = app(DesignService::class)
            ->getDesignByFile($form)['data'];
        foreach ($data as $row) {
            $designModel = Design::firstOrCreate([
                'case_type' => $row['type'],
                'device_code' => $row['deviceCode'],
                'color_code' => $row['colorCode'],
                'sort' => $row['sort'],
                'code' => $row['code']
            ]);
            $filename = "{$row['type']}-{$row['code']}-{$version}";
            $material = $materials[$filename];
            $designModel->design_material_id = $material['id'];
            $designModel->print_type = $row['printType'];
            $designModel->save();
        }
        $end = microtime(true);
        $total = round($end - $start, 2);
        echo "同步設計成功，總花費{$total}秒\n";
    }
}
