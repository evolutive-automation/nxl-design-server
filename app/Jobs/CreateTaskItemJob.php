<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\Repositories\TaskItemFailJobRepository;
use Illuminate\Validation\ValidationException;
use Exception;
use App\Notifications\SlackNotification;
use App\Services\CreateDesignTaskItemService;
use App\Services\CreateDeviceTaskItemService;
use Illuminate\Support\Facades\Notification;

class CreateTaskItemJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /** @var array $task */
    public $task;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(array $task)
    {
        $this->task = $task;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $this->deleteFailJob($this->task);
        if ($this->task['type'] === 'design') {
            app(CreateDesignTaskItemService::class)
                ->setTask($this->task)
                ->exec();
        } elseif ($this->task['type'] === 'device') {
            app(CreateDeviceTaskItemService::class)
                ->setTask($this->task)
                ->exec();
        }
    }

    public function failed(Exception $e)
    {
        $error_message = $e->getMessage();
        if ($e instanceof ValidationException) {
            $error_message = json_encode($e->validator->errors());
        }
        $form = [
            'job_name' => get_class($this),
            'job_key' => "task_{$this->task['id']}",
            'payload' => json_encode($this->task),
            'error_message' => $error_message
        ];
        app(TaskItemFailJobRepository::class)
            ->create($form);
        $this->sendSlackNotification($form);
    }

    private function sendSlackNotification($form)
    {
        $slackNotification = app(SlackNotification::class)
            ->setChannel('#設計出圖系統')
            ->setTitle("Job名稱：{$form['job_name']}")
            ->setMessage("錯誤訊息：{$form['error_message']}");
        Notification::route('slack', env('SLACK_URL'))
            ->notify($slackNotification);
    }

    private function deleteFailJob($task)
    {
        $jobName = get_class($this);
        $jobKey = "task_{$task['id']}";
        app(TaskItemFailJobRepository::class)
            ->deleteByJobKey($jobName, $jobKey);
    }
}
