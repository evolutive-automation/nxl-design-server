<?php

namespace App\Repositories;

use App\Repositories\Repository;
use App\Models\Color;
use App\Transformers\Device\ColorTransformer;

class ColorRepository extends Repository
{
    protected $model = Color::class;

    protected $transformer = ColorTransformer::class;
}
