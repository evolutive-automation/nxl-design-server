<?php

namespace Database\Factories;

use App\Models\Batch;
use Illuminate\Database\Eloquent\Factories\Factory;

class BatchFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Batch::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'status' => $this->faker->randomElement(['wait_file','wait_rip','wait_feed','feeding','wait_print','printing','done_print','done_pqc','done_pack']),
            'active' => $this->faker->randomElement([true, false]),
            'pirority' => 'normal',
            'case_type' => $this->faker->regexify('[A-Z]{3,5}'),
            'print_type' => $this->faker->randomElement(['K', 'W', 'CW-design'])
        ];
    }
}
