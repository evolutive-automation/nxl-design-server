<?php

namespace Tests\Feature;

use App\Models\Device;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\WithFaker;
use App\Services\GetDeviceService;
use Tests\DesignTestCase;

class GetDeviceServiceTest extends DesignTestCase
{
    use WithFaker, RefreshDatabase, DatabaseMigrations;

    public function test_it_can_get_device_list()
    {
        $this->createMetaData();

        $data = app(GetDeviceService::class)
            ->setArgs([])
            ->exec();

        $devices = Device::get();
        $this->assertCount(count($devices), $data);

        foreach ($data as $row) {
            $keys = collect($row)->keys()->toArray();
            $this->assertEquals(['type','brandCode','name','code','haveShadow'], $keys);
        }
    }
}
