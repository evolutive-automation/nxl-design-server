<?php

namespace App\Services;

use App\Repositories\BrandRepository;
use App\Services\Service;

class GetBrandService extends Service
{
    /** @var array $args */
    protected $args;

    /** @var BrandRepository $brandRepository */
    protected $brandRepository;

    public function __construct(
        BrandRepository $brandRepository
    ) {
        $this->brandRepository = $brandRepository;
    }

    public function setArgs($args)
    {
        $this->args = $args;

        return $this;
    }

    public function exec()
    {
        return $this->brandRepository->search($this->args);
    }
}
