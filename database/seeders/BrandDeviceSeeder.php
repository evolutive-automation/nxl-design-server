<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Brand;
use App\Models\Device;
use Illuminate\Support\Arr;

class BrandDeviceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                "name" => "Apple",
                "code" => "01",
                "devices" => [
                    [
                        "name" => "iPhone 5 / 5s / SE",
                        "code" => "001"
                    ],
                    [
                        "name" => "iPhone 6 / 6s",
                        "code" => "028",
                        "film" => "4.7",
                        "cardboard" => "",
                        "shape" => "D",
                        "clamshell" => "",
                        "kroma_clamshell" => ""
                    ],
                    [
                        "name" => "iPhone 6 Plus / 6s Plus",
                        "code" => "029",
                        "film" => "5.5",
                        "cardboard" => "",
                        "shape" => "E",
                        "clamshell" => "",
                        "kroma_clamshell" => ""
                    ],
                    [
                        "name" => "iPhone 7 / 7s",
                        "code" => "054",
                        "film" => "4.7",
                        "cardboard" => "4.7",
                        "shape" => "D",
                        "clamshell" => "4.7",
                        "kroma_clamshell" => "中(B)"
                    ],
                    [
                        "name" => "iPhone 7 Plus / 7s Plus",
                        "code" => "055",
                        "film" => "5.5",
                        "cardboard" => "5.5",
                        "shape" => "E",
                        "clamshell" => "6.7",
                        "kroma_clamshell" => "大(A)"
                    ],
                    [
                        "name" => "iPhone X",
                        "code" => "064",
                        "film" => "5.8",
                        "cardboard" => "5.8",
                        "shape" => "F",
                        "clamshell" => "4.7",
                        "kroma_clamshell" => "中(B)"
                    ],
                    [
                        "name" => "iPhone XS",
                        "code" => "084",
                        "film" => "5.8",
                        "cardboard" => "5.8",
                        "shape" => "F",
                        "clamshell" => "4.7",
                        "kroma_clamshell" => "中(B)"
                    ],
                    [
                        "name" => "iPhone XR",
                        "code" => "085",
                        "film" => "6.1",
                        "cardboard" => "6.1",
                        "shape" => "K",
                        "clamshell" => "6.1",
                        "kroma_clamshell" => "大(A)"
                    ],
                    [
                        "name" => "iPhone XS Max",
                        "code" => "086",
                        "film" => "5.5",
                        "cardboard" => "5.5",
                        "shape" => "E",
                        "clamshell" => "6.7",
                        "kroma_clamshell" => "大(A)"
                    ],
                    [
                        "name" => "iPhone 11 Pro",
                        "code" => "147",
                        "film" => "5.8",
                        "cardboard" => "5.8",
                        "shape" => "F",
                        "clamshell" => "4.7",
                        "kroma_clamshell" => "中(B)"
                    ],
                    [
                        "name" => "iPhone 11",
                        "code" => "148",
                        "film" => "6.1",
                        "cardboard" => "6.1",
                        "shape" => "K",
                        "clamshell" => "6.1",
                        "kroma_clamshell" => "大(A)"
                    ],
                    [
                        "name" => "iPhone 11 Pro Max",
                        "code" => "149",
                        "film" => "5.5",
                        "cardboard" => "5.5",
                        "shape" => "E",
                        "clamshell" => "6.7",
                        "kroma_clamshell" => "大(A)"
                    ],
                    [
                        "name" => "AirPods 1 / 2",
                        "code" => "170",
                        "cardboard" => "AirPods"
                    ],
                    [
                        "name" => "AirPods Pro",
                        "code" => "171",
                        "cardboard" => "AirPods Pro"
                    ],
                    [
                        "name" => "iPhone 12 mini",
                        "code" => "184",
                        "film" => "4.7",
                        "cardboard" => "4.7",
                        "shape" => "G",
                        "clamshell" => "4.7",
                        "kroma_clamshell" => "中(B)"
                    ],
                    [
                        "name" => "iPhone 12 / 12 Pro",
                        "code" => "185",
                        "film" => "6.1",
                        "cardboard" => "6.1",
                        "shape" => "F",
                        "clamshell" => "6.1",
                        "kroma_clamshell" => "大(A)"
                    ],
                    [
                        "name" => "iPhone 12 Pro Max",
                        "code" => "187",
                        "film" => "NOTE9",
                        "cardboard" => "6.7",
                        "shape" => "E",
                        "clamshell" => "6.7",
                        "kroma_clamshell" => "6.7泡殼"
                    ],
                    [
                        "name" => "iPhone 13",
                        "code" => "230",
                        "film" => "6.1",
                        "cardboard" => "6.1",
                        "shape" => "F",
                        "clamshell" => "6.1",
                        "kroma_clamshell" => "大(A)"
                    ],
                    [
                        "name" => "iPhone 13 mini",
                        "code" => "228",
                        "film" => "4.7",
                        "cardboard" => "4.7",
                        "shape" => "G",
                        "clamshell" => "4.7",
                        "kroma_clamshell" => "中(B)"
                    ],
                    [
                        "name" => "iPhone 13 Pro",
                        "code" => "231",
                        "film" => "6.1",
                        "cardboard" => "6.1",
                        "shape" => "F",
                        "clamshell" => "6.1",
                        "kroma_clamshell" => "大(A)"
                    ],
                    [
                        "name" => "iPhone 13 Pro Max",
                        "code" => "232",
                        "film" => "6.7",
                        "cardboard" => "6.7",
                        "shape" => "E",
                        "clamshell" => "6.7",
                        "kroma_clamshell" => "6.7泡殼"
                    ]
                ]
            ],
            [
                "name" => "Google",
                "code" => "02",
                "devices" => [
                    [
                        "name" => "Google Pixel 3",
                        "code" => "090",
                        "film" => "5.8",
                        "cardboard" => "",
                        "shape" => "F",
                        "clamshell" => "",
                        "kroma_clamshell" => "",
                        "have_shadow" => true
                    ],
                    [
                        "name" => "Google Pixel 3 XL",
                        "code" => "091",
                        "film" => "5.5",
                        "cardboard" => "",
                        "shape" => "E",
                        "clamshell" => "",
                        "kroma_clamshell" => "",
                        "have_shadow" => true
                    ],
                    [
                        "name" => "Google Pixel 3a",
                        "code" => "102",
                        "film" => "6.1",
                        "cardboard" => "",
                        "shape" => "I",
                        "clamshell" => "",
                        "kroma_clamshell" => "",
                        "have_shadow" => true
                    ],
                    [
                        "name" => "Google Pixel 3a XL",
                        "code" => "103",
                        "film" => "5.5",
                        "cardboard" => "",
                        "shape" => "E",
                        "clamshell" => "",
                        "kroma_clamshell" => "",
                        "have_shadow" => true
                    ],
                    [
                        "name" => "Google Pixel 4",
                        "code" => "150",
                        "film" => "6.1",
                        "cardboard" => "",
                        "shape" => "F",
                        "clamshell" => "",
                        "kroma_clamshell" => ""
                    ],
                    [
                        "name" => "Google Pixel 4 XL",
                        "code" => "151",
                        "film" => "5.5",
                        "cardboard" => "",
                        "shape" => "E",
                        "clamshell" => "",
                        "kroma_clamshell" => ""
                    ],
                    [
                        "name" => "Google Pixel 4a",
                        "code" => "175",
                        "film" => "5.8",
                        "cardboard" => "",
                        "shape" => "F",
                        "clamshell" => "",
                        "kroma_clamshell" => ""
                    ],
                    [
                        "name" => "Google Pixel 5",
                        "code" => "193",
                        "film" => "4.7",
                        "cardboard" => "",
                        "shape" => "F",
                        "clamshell" => "",
                        "kroma_clamshell" => ""
                    ],
                    [
                        "name" => "Google Pixel 4a (5G)",
                        "code" => "194",
                        "film" => "5.8",
                        "cardboard" => "",
                        "shape" => "E",
                        "clamshell" => "",
                        "kroma_clamshell" => ""
                    ],
                    [
                        "name" => "Google Pixel 5a",
                        "code" => "214",
                        "film" => "6.1",
                        "cardboard" => "",
                        "shape" => "E",
                        "clamshell" => "",
                        "kroma_clamshell" => ""
                    ],
                    [
                        "name" => "Google Pixel 6",
                        "code" => "233",
                        "film" => "",
                        "cardboard" => "",
                        "shape" => "",
                        "clamshell" => "",
                        "kroma_clamshell" => ""
                    ],
                    [
                        "name" => "Google Pixel 6 Pro",
                        "code" => "234",
                        "film" => "NOTE9",
                        "cardboard" => "",
                        "shape" => "E",
                        "clamshell" => "",
                        "kroma_clamshell" => ""
                    ]
                ]
            ],
            [
                "name" => "Samsung",
                "code" => "03",
                "devices" => [
                    [
                        "name" => "Samsung S9",
                        "code" => "074",
                        "film" => "6.1",
                        "shape" => "F"
                    ],
                    [
                        "name" => "Samsung S9 Plus",
                        "code" => "075",
                        "film" => "5.5",
                        "shape" => "E"
                    ],
                    [
                        "name" => "Samsung Note 9",
                        "code" => "082",
                        "film" => "NOTE9",
                        "shape" => "E"
                    ],
                    [
                        "name" => "Samsung S10",
                        "code" => "099",
                        "film" => "6.1",
                        "shape" => "K"
                    ],
                    [
                        "name" => "Samsung S10 Plus",
                        "code" => "100",
                        "film" => "5.5",
                        "shape" => "E"
                    ],
                    [
                        "name" => "Samsung S10e",
                        "code" => "101",
                        "film" => "5.8",
                        "shape" => "F"
                    ],
                    [
                        "name" => "Samsung Note 10",
                        "code" => "142",
                        "film" => "5.5",
                        "shape" => "I"
                    ],
                    [
                        "name" => "Samsung Note 10 Plus",
                        "code" => "143",
                        "film" => "NOTE9",
                        "shape" => "L"
                    ],
                    [
                        "name" => "Samsung S20+",
                        "code" => "158",
                        "film" => "5.5",
                        "shape" => "E"
                    ],
                    [
                        "name" => "Samsung S20 Ultra",
                        "code" => "159",
                        "film" => "NOTE9",
                        "shape" => "L"
                    ],
                    [
                        "name" => "Samsung S20",
                        "code" => "160",
                        "film" => "6.1",
                        "shape" => "I"
                    ],
                    [
                        "name" => "Samsung Galaxy A51",
                        "code" => "172",
                        "film" => "5.5",
                        "shape" => "E"
                    ],
                    [
                        "name" => "Samsung Galaxy A71",
                        "code" => "173",
                        "film" => "NOTE9",
                        "shape" => "L"
                    ],
                    [
                        "name" => "Samsung Galaxy A41",
                        "code" => "182",
                        "film" => "6.1",
                        "shape" => "I"
                    ],
                    [
                        "name" => "Samsung Galaxy Note20",
                        "code" => "188",
                        "film" => "NOTE9",
                        "shape" => "L"
                    ],
                    [
                        "name" => "Samsung Galaxy Note20 Ultra",
                        "code" => "189",
                        "film" => "NOTE9",
                        "shape" => "L"
                    ],
                    [
                        "name" => "Samsung S20 FE",
                        "code" => "195",
                        "film" => "NOTE9",
                        "shape" => "E"
                    ],
                    [
                        "name" => "Samsung Galaxy S21",
                        "code" => "199",
                        "film" => "6.1",
                        "shape" => "I"
                    ],
                    [
                        "name" => "Samsung Galaxy S21+",
                        "code" => "200",
                        "film" => "NOTE9",
                        "shape" => "L"
                    ],
                    [
                        "name" => "Samsung Galaxy S21 Ultra",
                        "code" => "201",
                        "film" => "NOTE9",
                        "shape" => "L"
                    ],
                    [
                        "name" => "Samsung Galaxy A52",
                        "code" => "204",
                        "film" => "5.5",
                        "shape" => "E"
                    ],
                    [
                        "name" => "Samsung Galaxy A42",
                        "code" => "205",
                        "film" => "NOTE9",
                        "shape" => "L"
                    ],
                    [
                        "name" => "Samsung Galaxy A72",
                        "code" => "208",
                        "film" => "NOTE9",
                        "shape" => "L"
                    ],
                    [
                        "name" => "Samsung Galaxy S21 FE",
                        "code" => "225",
                        "film" => "",
                        "shape" => ""
                    ]
                ]
            ],
            [
                "name" => "OnePlus",
                "code" => "09",
                "devices" => [
                    [
                        "name" => "OnePlus 6",
                        "code" => "081",
                        "film" => "5.5",
                        "shape" => "E"
                    ],
                    [
                        "name" => "OnePlus 6T",
                        "code" => "093",
                        "film" => "5.5",
                        "shape" => "E"
                    ],
                    [
                        "name" => "OnePlus 7",
                        "code" => "132",
                        "film" => "5.5",
                        "shape" => "E"
                    ],
                    [
                        "name" => "OnePlus 7 Pro",
                        "code" => "133",
                        "film" => "NOTE9",
                        "shape" => "E"
                    ],
                    [
                        "name" => "OnePlus 7T",
                        "code" => "155",
                        "film" => "NOTE9",
                        "shape" => "E"
                    ],
                    [
                        "name" => "OnePlus 7T Pro",
                        "code" => "156",
                        "film" => "NOTE9",
                        "shape" => "E"
                    ],
                    [
                        "name" => "OnePlus 8",
                        "code" => "167",
                        "film" => "NOTE9",
                        "shape" => "E"
                    ],
                    [
                        "name" => "OnePlus 8 Pro",
                        "code" => "168",
                        "film" => "NOTE9",
                        "shape" => "L"
                    ],
                    [
                        "name" => "OnePlus Nord",
                        "code" => "192",
                        "film" => "5.5",
                        "shape" => "E"
                    ],
                    [
                        "name" => "OnePlus 8T",
                        "code" => "196",
                        "film" => "NOTE9",
                        "shape" => "E"
                    ],
                    [
                        "name" => "OnePlus 9",
                        "code" => "209",
                        "film" => "NOTE9",
                        "shape" => "E"
                    ],
                    [
                        "name" => "OnePlus 9 Pro",
                        "code" => "210",
                        "film" => "NOTE9",
                        "shape" => "L"
                    ]
                ]
            ],
            [
                "name" => "Huawei",
                "code" => "10",
                "devices" => [
                    [
                        "name" => "Huawei P20",
                        "code" => "079",
                        "film" => "6.1",
                        "shape" => "K",
                        "have_shadow" => true
                    ],
                    [
                        "name" => "Huawei P30",
                        "code" => "104",
                        "film" => "6.1",
                        "shape" => "K"
                    ],
                    [
                        "name" => "Huawei P30 Pro",
                        "code" => "105",
                        "film" => "5.5",
                        "shape" => "E"
                    ],
                    [
                        "name" => "Huawei P30 Lite",
                        "code" => "106",
                        "film" => "5.5",
                        "shape" => "E"
                    ]
                ]
            ],
            [
                "name" => "ASUS",
                "code" => "11",
                "devices" => [
                    [
                        "name" => "ASUS Zenfone 5",
                        "code" => "076",
                        "film" => "5.5",
                        "shape" => "E",
                        "have_shadow" => true
                    ],
                    [
                        "name" => "ASUS Zenfone 6",
                        "code" => "107",
                        "film" => "5.5",
                        "shape" => "E"
                    ],
                    [
                        "name" => "ASUS Zenfone 7 / 7 Pro",
                        "code" => "190",
                        "film" => "5.5",
                        "shape" => "E"
                    ],
                    [
                        "name" => "ASUS Zenfone 8 Flip",
                        "code" => "206",
                        "film" => "5.5",
                        "shape" => "L"
                    ],
                    [
                        "name" => "ASUS Zenfone 8",
                        "code" => "207",
                        "film" => "6.1",
                        "shape" => "F"
                    ]
                ]
            ],
            [
                "name" => "OPPO",
                "code" => "13",
                "devices" => [
                    [
                        "name" => "OPPO Reno2",
                        "code" => "154",
                        "film" => "5.5",
                        "shape" => "E"
                    ]
                ]
            ],
            [
                "name" => "Xiaomi",
                "code" => "14",
                "devices" => [
                    [
                        "name" => "Xiaomi Mi 11",
                        "code" => "211",
                        "film" => "NOTE9",
                        "shape" => "L"
                    ],
                    [
                        "name" => "Xiaomi Mi 10 / Mi 10 Pro",
                        "code" => "215",
                        "film" => "5.5",
                        "shape" => "E"
                    ],
                    [
                        "name" => "Xiaomi Mi 10T / Mi 10T Pro",
                        "code" => "216",
                        "film" => "NOTE9",
                        "shape" => "E"
                    ],
                    [
                        "name" => "Xiaomi Redmi Note 9 Pro (4G) / 9S",
                        "code" => "217",
                        "film" => "NOTE9",
                        "shape" => "L"
                    ],
                    [
                        "name" => "Xiaomi Redmi Note 10 (4G) / 10S",
                        "code" => "218",
                        "film" => "5.5",
                        "shape" => "E"
                    ],
                    [
                        "name" => "Xiaomi Redmi Note 10 Pro (Global)",
                        "code" => "219"
                    ],
                    [
                        "name" => "Xiaomi Redmi Note 9 (4G Global)",
                        "code" => "220",
                        "film" => "5.5",
                        "shape" => "E"
                    ],
                    [
                        "name" => "Xiaomi Mi 11 Ultra",
                        "code" => "224"
                    ]
                ]
            ]
        ];

        foreach ($data as $row) {
            $brandPayload = [
                'name' => $row['name'],
                'code' => $row['code']
            ];
            $brandModel = Brand::firstOrCreate(['code' => $brandPayload['code']]);
            $brandModel->update($brandPayload);
            foreach ($row['devices'] as $device) {
                $haveShadow = false;
                $arr = ["090","091","102","103","079","076","233"];
                if ($brandModel->code !== '01' && !in_array($device['code'], $arr)) {
                    $haveShadow = true;
                }
                $devicePayload = [
                    'brand_code' => $brandModel->code,
                    'name' => $device['name'],
                    'code' => $device['code'],
                    'have_shadow' => $haveShadow
                ];
                $deviceModel = Device::firstOrCreate([
                    'code' => $devicePayload['code'],
                    'brand_code' => $devicePayload['brand_code'],
                ]);
                $deviceModel->update($devicePayload);
            }
        }
    }
}
