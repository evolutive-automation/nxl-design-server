<?php

namespace App\Transformers\CaseType;

use App\Models\CaseType;
use App\Transformers\Transformer;

class CaseTypeTransformer extends Transformer
{
    /**
     * @param CaseType $caseTypeModel
     *
     * @return array
     */
    public function exec($caseTypeModel)
    {
        return [
            'id' => $caseTypeModel->id,
            'active' => $caseTypeModel->active? true : false,
            'name' => $caseTypeModel->name,
            'printFace' => $caseTypeModel->print_face,
            'version' => $caseTypeModel->version
        ];
    }
}
