<?php

namespace App\Repositories;

use App\Repositories\Repository;
use App\Models\DesignMaterial;
use App\Transformers\DesignMaterial\DesignMaterialTransformer;

class DesignMaterialRepository extends Repository
{
    protected $model = DesignMaterial::class;

    protected $transformer = DesignMaterialTransformer::class;
}
