<?php

namespace App\Services;

use App\Repositories\CaseTypeRepository;
use App\Services\Service;

class GetCaseTypeService extends Service
{
    /** @var array $args */
    protected $args;

    /** @var CaseTypeRepository $caseTypeRepository */
    protected $caseTypeRepository;

    public function __construct(
        CaseTypeRepository $caseTypeRepository
    ) {
        $this->caseTypeRepository = $caseTypeRepository;
    }

    public function setArgs($args)
    {
        $this->args = $args;

        return $this;
    }

    public function exec()
    {
        $args = $this->processArgs();

        return $this->caseTypeRepository->search($args);
    }

    private function processArgs()
    {
        $args = [];
        foreach ($this->args as $key => $value) {
            $args[$key] = $value;
            if ($key === 'active') {
                $args[$key] = ($value === 'true' || $value === true)? true : false;
            }
        }
        return $args;
    }
}
