<?php

namespace App\Repositories;

use App\Repositories\Repository;
use App\Models\ExternalApiLog;
use App\Transformers\ExternalLog\ExternalApiLogTransformer;

class ExternalApiLogRepository extends Repository
{
    protected $model = ExternalApiLog::class;

    protected $transformer = ExternalApiLogTransformer::class;
}
