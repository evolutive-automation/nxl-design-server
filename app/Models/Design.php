<?php

namespace App\Models;

use App\Models\Device;
use App\Models\DesignMaterial;
use App\Models\Model;

class Design extends Model
{
    public $table = 'designs';

    public function device()
    {
        return $this->belongsTo(Device::class, 'device_code', 'code');
    }

    public function material()
    {
        return $this->belongsTo(DesignMaterial::class, 'design_material_id', 'id');
    }
}
