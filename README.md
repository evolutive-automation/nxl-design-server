本專案用文章的 CRUD 為範例來說明程式的撰寫風格，如果有什麼覺得需要改善的地方，歡迎提出來一起討論，讓我們一起進步。

## 開發環境

[factory-docker](https://bitbucket.org/evolutive-automation/factory-docker/src/master/)

## VS Code 安裝套件

- [Better PHPUnit](https://marketplace.visualstudio.com/items?itemName=calebporzio.better-phpunit)
- [EditorConfig for VS Code](https://marketplace.visualstudio.com/items?itemName=EditorConfig.EditorConfig)
- [GitLens](https://marketplace.visualstudio.com/items?itemName=eamodio.gitlens)
- [PHP Intelephense](https://marketplace.visualstudio.com/items?itemName=bmewburn.vscode-intelephense-client)
- [Trailing Spaces](https://marketplace.visualstudio.com/items?itemName=shardulm94.trailing-spaces)

## 資料夾介紹
檔名都用大駝峰式(Upper Camel Case)命名，每一個單字的首字母都採用大寫字母，例如：HelloWorld.php

- Controllers：接收路由的 Request，並分配類別去執行程式邏輯。
- Models：[Eloquent Model](https://laravel.com/docs/8.x/eloquent#generating-model-classes) 用來定義 relation、資料庫欄位或連線資訊等等，切記檔名不加s，但資料表要加s。
- Repositories：封裝與資料庫相關 SQL 的邏輯。
- Services：按照行為撰寫主要邏輯程式，切記檔名開頭用"動作"+"資源"命名且該 Service 務必使用 setter、getter 來取得邏輯參數，並對外 Public 執行方法只有 exec，方便維護人員維護。
- Transformers：針對資料庫顯示的結果進行轉換，來符合實際情境的需求。

## 開發流程
1. 撰寫該 API 相關內容(google 文件或 draw.io 畫流程圖)
2. 建立資料表(如果有需要)
3. 建立 Factory 假資料，盡可能與真實資料貼近，這樣測試參考價值越高
4. 根據文件內容撰寫測試案例，藉由測試紅燈到綠燈慢慢把程式補完
5. 建立 Controller 方法去分配要執行的 Service
6. 建立 API 路由去指定 Controller 方法
7. 按照 Git Flow 去部署程式

## Code Style

各種命名規則介紹，可參考[此連結](https://shunnien.github.io/2017/06/07/naming-conventions/)

#### PSR2 原則
詳細可參考[此連結](https://skyyen999.gitbooks.io/-study-design-pattern-in-java/content/oodPrinciple.html)，基本原則如下：
- 程式碼必須用 4 個空格做縮排
- class 和 method 的開始左大括弧必須要換下一行，結束右大括弧必須要換到程式碼下一行
- 控制結構(if else 或 try catch)的開始左大括弧必須要在同一行，結束右大括弧必須要換到程式碼下一行
- 所有的 PHP 檔案最後要空一行
- [關鍵字](https://www.php.net/manual/en/reserved.keywords.php)或 true, false 和 null 一定要用小寫
- 資料表的欄位用 Snake Case
- 程式的變數及方法名稱用 Lower Camel Case
- 靜態變數用全大寫的 Snake Case

#### SOLID 原則
詳細可參考[此連結](https://skyyen999.gitbooks.io/-study-design-pattern-in-java/content/oodPrinciple.html)，常用原則如下：
- 單一職責原則(SRP)：每個物件，不管是類別、函數，負責的功能，都應該只做一件事。
- 開放封閉原則(OCP)：當需求有異動時，藉由繼承、相依性注入等方式，增加新的程式碼，來實作新的需求。

---
## 前端開發

用系統切分 vue 實例，每個系統的都有一個 app.js
### 資料夾介紹

- apis：後端 API
- components：共用元件
- config：設定檔
- mixins：共用邏輯程式
- plugins：Vue 插件
- router：路由路徑
- views：頁面
- utils：輔助方法

### URL path

- /pqc#：印刷室主控台
- /assigner#/:version/:ink/:printer/wait_feed：/assigner#/2/soft/1/wait_feed(二代機-軟墨-1號機-主控台)
