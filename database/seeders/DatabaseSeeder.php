<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Database\Seeders\CaseTypeSeeder;
use Database\Seeders\BrandDeviceSeeder;
use Database\Seeders\ColorSeeder;
use Database\Seeders\PrinterSeeder;
use Database\Seeders\WareSeeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        app(CaseTypeSeeder::class)->run();
        app(BrandDeviceSeeder::class)->run();
        app(ColorSeeder::class)->run();
        app(DesignSeeder::class)->run();
    }
}
