<?php

namespace App\Transformers\Task;

use App\Models\Task;
use App\Transformers\TaskItem\MoveTaskItemTransformer;
use App\Transformers\Transformer;
use Carbon\Carbon;

class DetailTaskTransformer extends Transformer
{
    /**
     * @param Task $taskModel
     *
     * @return array
     */
    public function exec($taskModel)
    {
        $transformer = app(MoveTaskItemTransformer::class);
        $taskItems = $taskModel->taskItems->map(fn ($taskItemModel) => $transformer->exec($taskItemModel))->toArray();
        return [
            'type' => $taskModel->type,
            'status' => $taskModel->status,
            'name' => $taskModel->name,
            'payload' => json_decode($taskModel->payload, true),
            'quantity' => (int) $taskModel->quantity,
            'doneQuantity' => (int) $taskModel->done_quantity,
            'startedAt' => is_null($taskModel->started_at)? null : Carbon::parse($taskModel->started_at, 'UTC')->setTimezone('Asia/Taipei')->toDateTimeString(),
            'endedAt' => is_null($taskModel->ended_at)? null : Carbon::parse($taskModel->ended_at, 'UTC')->setTimezone('Asia/Taipei')->toDateTimeString(),
            'items' => $taskItems
        ];
    }
}
