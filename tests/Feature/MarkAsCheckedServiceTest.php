<?php

namespace Tests\Feature;

use App\Events\NotifyCheckedDesignEvent;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\WithFaker;
use App\Services\MarkAsCheckedService;
use Tests\DesignTestCase;
use Illuminate\Support\Facades\Event;

class MarkAsCheckedServiceTest extends DesignTestCase
{
    use WithFaker, RefreshDatabase, DatabaseMigrations;

    public function test_it_can_mark_as_checked()
    {
        $expectedHavePrint = true;
        $fakeDesignTask = $this->createFakeDesignTask('made');
        $task = $fakeDesignTask['task'];
        $designMaterial = $fakeDesignTask['designMaterial'];
        $this->createFakeDesignTaskItem($task, $designMaterial, $expectedHavePrint);
        $task->taskItems()->update([
            'have_print' => true,
            'have_web' => true,
            'is_allowed' => true,
        ]);
        Event::fake();
        app(MarkAsCheckedService::class)
            ->setTaskId($task->id)
            ->exec();

        $this->assertDatabaseHas('tasks', [
            'id' => $task->id,
            'status' => 'checked'
        ]);
        Event::assertDispatched(NotifyCheckedDesignEvent::class);
    }
}
