<?php

namespace App\Models;

use App\Models\Model;
use App\Models\Device;

class Brand extends Model
{
    public $table = 'brands';

    public function devices()
    {
        return $this->hasMany(Device::class, 'brand_code', 'code');
    }
}
