import axios from "axios";
import { domain, apiToken } from '@design/config'

export const getTasks = async () => {
  return (
    await axios.get(`${domain}/api/tasks/no-check`, {
      headers: {
        "DESIGN-SERVER-API-TOKEN": apiToken
      }
    })
  ).data
}

export const deleteTask = async (id) => {
  return (
    await axios.delete(`${domain}/api/tasks/${id}`, {
      headers: {
        "DESIGN-SERVER-API-TOKEN": apiToken
      }
    })
  ).data
}

export const getTaskItems = async (taskId) => {
  return (
    await axios.get(`${domain}/api/task-items/no-check/${taskId}`, {
      headers: {
        "DESIGN-SERVER-API-TOKEN": apiToken
      }
    })
  ).data
}

export const markAsNotAllowed = async (id) => {
  return (
    await axios.get(`${domain}/api/task-items/${id}/mark-not-allowed`, {
      headers: {
        "DESIGN-SERVER-API-TOKEN": apiToken
      }
    })
  ).data
}

export const markAsChecked = async (id) => {
  return (
    await axios.get(`${domain}/api/tasks/${id}/mark-checked`, {
      headers: {
        "DESIGN-SERVER-API-TOKEN": apiToken
      }
    })
  ).data
}

export const markAsCancelled = async (id) => {
  return (
    await axios.get(`${domain}/api/tasks/${id}/mark-cancelled`, {
      headers: {
        "DESIGN-SERVER-API-TOKEN": apiToken
      }
    })
  ).data
}

export const createDesignTask = async (formData) => {
  return (
    await axios.post(`${domain}/api/tasks/create-design`, formData, {
      headers: {
        "DESIGN-SERVER-API-TOKEN": apiToken
      }
    })
  ).data
}

export const createDeviceTask = async (formData) => {
  return (
    await axios.post(`${domain}/api/tasks/create-device`, formData, {
      headers: {
        "DESIGN-SERVER-API-TOKEN": apiToken
      }
    })
  ).data
}

export const getCaseTypes = async (params) => {
  return (
    await axios.get(`${domain}/api/case-types/search`, {
      headers: {
        "DESIGN-SERVER-API-TOKEN": apiToken
      },
      params
    })
  ).data
}

export const getGmgColors = async () => {
  return (
    await axios.get(`${domain}/api/colors/gmg`, {
      headers: {
        "DESIGN-SERVER-API-TOKEN": apiToken
      }
    })
  ).data
}

export const getBrands = async () => {
  return (
    await axios.get(`${domain}/api/brands/search`, {
      headers: {
        "DESIGN-SERVER-API-TOKEN": apiToken
      }
    })
  ).data
}

export const getDevices = async () => {
  return (
    await axios.get(`${domain}/api/devices/search`, {
      headers: {
        "DESIGN-SERVER-API-TOKEN": apiToken
      }
    })
  ).data
}

export const getMaterials = async () => {
  return (
    await axios.get(`${domain}/api/design-material/search`, {
      headers: {
        "DESIGN-SERVER-API-TOKEN": apiToken
      }
    })
  ).data
}

export const markAsUrgent = async (id) => {
  return (
    await axios.get(`${domain}/api/tasks/${id}/mark-urgent`, {
      headers: {
        "DESIGN-SERVER-API-TOKEN": apiToken
      }
    })
  ).data
}


export const downloadZip = async (id) => {
  return (
    await axios.get(`${domain}/api/tasks/${id}/download-zip`, {
      responseType: 'blob',
      headers: {
        "DESIGN-SERVER-API-TOKEN": apiToken
      }
    })
  ).data
}
