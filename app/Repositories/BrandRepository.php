<?php

namespace App\Repositories;

use App\Repositories\Repository;
use App\Models\Brand;
use App\Transformers\Brand\BrandTransformer;

class BrandRepository extends Repository
{
    protected $model = Brand::class;

    protected $transformer = BrandTransformer::class;
}
