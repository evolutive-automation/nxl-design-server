<?php

namespace Tests\Feature;

use App\Jobs\CreateTaskItemJob;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\WithFaker;
use App\Services\MarkAsUrgentTaskService;
use Tests\DesignTestCase;
use Illuminate\Support\Facades\Queue;

class MarkAsUrgentTaskServiceTest extends DesignTestCase
{
    use WithFaker, RefreshDatabase, DatabaseMigrations;

    public function test_it_can_mark_as_urgent_task()
    {
        $fakeDesignTask = $this->createFakeDesignTask('pending');
        $task = $fakeDesignTask['task'];

        Queue::fake();
        app(MarkAsUrgentTaskService::class)
            ->setTaskId($task->id)
            ->exec();

        Queue::assertPushed(CreateTaskItemJob::class);
    }
}
