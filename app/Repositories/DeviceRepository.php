<?php

namespace App\Repositories;

use App\Repositories\Repository;
use App\Models\Device;
use App\Transformers\Device\DeviceTransformer;

class DeviceRepository extends Repository
{
    protected $model = Device::class;

    protected $transformer = DeviceTransformer::class;

    protected $relations = ['brand'];
}
