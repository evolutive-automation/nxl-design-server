<?php

namespace Libs\CustomizedServer;

use GuzzleHttp\Client;

abstract class Service
{
    /** @var Client $clienct */
    protected $client;

    /** @var string $endpoint */
    protected $endpoint;

    /** @var string $token */
    protected $token;

    public function __construct(Client $client)
    {
        $this->client = $client;
        $this->endpoint = env('CUSTOMIZED_SERVER_DOMAIN');
        $this->token = env('CUSTOMIZED_SERVER_API_TOKEN');
    }

    protected function sendRequest($method, $url, $form)
    {
        $options = [
            'headers' => [
                'Content-Type' => 'application/json',
                'CUSTOMIZED-SERVER-API-TOKEN' => $this->token
            ]
        ];
        if ($method === 'GET') {
            $options['query'] = $form;
        } else {
            $options['json'] = $form;
        }

        $response = $this->client->request($method, $url, $options);

        return json_decode($response->getBody()->getContents(), true);
    }
}
