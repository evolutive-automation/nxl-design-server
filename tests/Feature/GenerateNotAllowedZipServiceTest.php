<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\WithFaker;
use App\Services\GenerateNotAllowedZipService;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Tests\DesignTestCase;

class GenerateNotAllowedZipServiceTest extends DesignTestCase
{
    use WithFaker, RefreshDatabase, DatabaseMigrations;

    public function test_it_can_generate_not_allowed_task_item_zip()
    {
        $fakeDesignTask = $this->createFakeDesignTask('made');
        $task = $fakeDesignTask['task'];
        $designMaterial = $fakeDesignTask['designMaterial'];
        $this->createFakeDesignTaskItem($task, $designMaterial, true);
        $task->taskItems()->update([
            'have_print' => 1,
            'have_web' => 1,
            'is_allowed' => 0
        ]);

        $zipPath = app(GenerateNotAllowedZipService::class)
            ->setTaskId($task->id)
            ->exec();

        $this->assertEquals(true, file_exists($zipPath));
    }

    public function test_it_can_throw_exception()
    {
        $fakeDesignTask = $this->createFakeDesignTask('made');
        $task = $fakeDesignTask['task'];
        $designMaterial = $fakeDesignTask['designMaterial'];
        $this->createFakeDesignTaskItem($task, $designMaterial, true);
        $task->taskItems()->update([
            'have_print' => 1,
            'have_web' => 0,
            'is_allowed' => 0
        ]);
        $this->withExceptionHandling();
        $this->expectException(ModelNotFoundException::class);

        app(GenerateNotAllowedZipService::class)
            ->setTaskId($task->id)
            ->exec();
    }
}
