const path = require("path");
const mix = require('laravel-mix');

function resolveAssetsPath(p) {
  return path.resolve(__dirname, srcAssetDir + "/" + p);
}

const srcAssetDir = "./resources/assets";
const assetsDirectory = "./public/assets";

const alias = {
  "@design": resolveAssetsPath("js/design")
};

mix.webpackConfig({ resolve: { alias } });

mix.js(`${srcAssetDir}/js/design/app.js`, assetsDirectory + "/design.js")

require("dotenv").config();

mix.webpackConfig(webpack => {
  return {
    plugins: [
      new webpack.DefinePlugin({
        "process.env": {
          APP_URL: JSON.stringify(process.env.APP_URL),
          CUSTOMIZED_SERVER_API_TOKEN: JSON.stringify(process.env.CUSTOMIZED_SERVER_API_TOKEN),
          FileServerDomain: JSON.stringify(process.env.FileServerDomain)
        }
      })
    ]
  }
})

