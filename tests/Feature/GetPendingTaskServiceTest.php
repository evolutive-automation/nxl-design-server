<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\WithFaker;
use App\Services\GetPendingTaskService;
use Tests\DesignTestCase;

class GetPendingTaskServiceTest extends DesignTestCase
{
    use WithFaker, RefreshDatabase, DatabaseMigrations;

    public function test_it_can_get_pending_design_task()
    {
        $this->createFakeDesignTask('pending');
        $expectedLimit = 3;
        $tasks = app(GetPendingTaskService::class)
            ->setLimit($expectedLimit)
            ->exec();

        $this->assertLessThan($expectedLimit, count($tasks));
        foreach ($tasks as $task) {
            $this->assertEquals('design', $task['type']);
            $this->assertEquals('pending', $task['status']);
        }
    }

    public function test_it_can_get_pending_device_task()
    {
        $this->createFakeDeviceTask('pending');
        $expectedLimit = 3;
        $tasks = app(GetPendingTaskService::class)
            ->setLimit($expectedLimit)
            ->exec();

        $this->assertLessThan($expectedLimit, count($tasks));
        foreach ($tasks as $task) {
            $this->assertEquals('device', $task['type']);
            $this->assertEquals('pending', $task['status']);
        }
    }
}
