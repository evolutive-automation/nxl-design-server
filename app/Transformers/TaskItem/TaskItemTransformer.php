<?php

namespace App\Transformers\TaskItem;

use App\Models\TaskItem;
use App\Transformers\Transformer;

class TaskItemTransformer extends Transformer
{
    /**
     * @param TaskItem $taskItemModel
     *
     * @return array
     */
    public function exec($taskItemModel)
    {
        return [
            'id' => $taskItemModel->id,
            'taskId' => $taskItemModel->task_id,
            'havePrint' => $taskItemModel->have_print? true : false,
            'haveWeb' => $taskItemModel->have_web? true : false,
            'isAllowed' => $taskItemModel->is_allowed? true : false,
            'materialPrintPath' => $taskItemModel->material_print_path,
            'materialWebPath' => $taskItemModel->material_web_path,
            'outputPrintPath' => $this->getOutputPath($taskItemModel, 'print'),
            'outputWebPath' => $this->getOutputPath($taskItemModel, 'web'),
        ];
    }

    private function getOutputPath($taskItemModel, $type)
    {
        $haveColumn = ($type === 'print')? 'have_print' : 'have_web';
        $pathColumn = ($type === 'print')? 'material_print_path' : 'material_web_path';
        $folder = "output/{$type}";
        $filename = basename($taskItemModel->{$pathColumn});
        if ($type === 'web') {
            $filename = str_replace(['-0.png', '-1.png'], '.png', $filename);
            $filename = str_replace(['-K.png','-W.png','-CW.png'], '.png', $filename);
        }

        return ($taskItemModel->{$haveColumn})? "{$folder}/{$filename}" : "";
    }
}
