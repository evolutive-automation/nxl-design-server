<?php

namespace App\Exceptions;

use Exception;

class CantCancelledTaskException extends Exception
{
    protected $message;

    public function __construct($message = 'cant cancelled task exception.')
    {
        parent::__construct($message);
    }

    public function getStatusCode()
    {
        return 400;
    }
}
