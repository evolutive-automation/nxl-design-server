<?php

namespace App\Models;

use App\Models\Task;
use App\Models\Design;
use App\Models\Model;

class TaskItem extends Model
{
    public $table = 'task_items';

    public function task()
    {
        return $this->belongsTo(Task::class, 'task_id', 'id');
    }

    public function design()
    {
        return $this->belongsTo(Design::class, 'design_id', 'id');
    }
}
