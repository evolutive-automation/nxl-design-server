<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\Repositories\TaskItemFailJobRepository;
use Illuminate\Validation\ValidationException;
use Exception;
use App\Notifications\SlackNotification;
use Illuminate\Support\Facades\Notification;

class SavePendingFileJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /** @var array $payload */
    public $payload;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(array $payload)
    {
        $this->payload = $payload;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $this->deleteFailJob($this->payload);
        file_put_contents($this->payload['destPath'], file_get_contents($this->payload['sourcePath']));
    }

    public function failed(Exception $e)
    {
        $error_message = $e->getMessage();
        if ($e instanceof ValidationException) {
            $error_message = json_encode($e->validator->errors());
        }
        $form = [
            'job_name' => get_class($this),
            'job_key' => "{$this->payload['taskId']}_{$this->payload['designId']}",
            'payload' => json_encode($this->payload),
            'error_message' => $error_message
        ];
        app(TaskItemFailJobRepository::class)
            ->create($form);
        $this->sendSlackNotification($form);
    }

    private function sendSlackNotification($form)
    {
        $slackNotification = app(SlackNotification::class)
            ->setChannel('#設計出圖系統')
            ->setTitle("Job名稱：{$form['job_name']}")
            ->setMessage("錯誤訊息：{$form['error_message']}");
        Notification::route('slack', env('SLACK_URL'))
            ->notify($slackNotification);
    }

    private function deleteFailJob($payload)
    {
        $jobName = get_class($this);
        $jobKey = "{$payload['taskId']}_{$payload['designId']}";
        app(TaskItemFailJobRepository::class)
            ->deleteByJobKey($jobName, $jobKey);
    }
}
