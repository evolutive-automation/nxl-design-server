<?php

namespace App\Transformers\Design;

use App\Models\Design;
use App\Transformers\Transformer;

class DesignTransformer extends Transformer
{
    /**
     * @param Design $designModel
     *
     * @return array
     */
    public function exec($designModel)
    {
        return [
            'id' => $designModel->id,
            'designMaterialId' => (int) $designModel->design_material_id,
            'caseType' => $designModel->case_type,
            'colorCode' => $designModel->color_code,
            'deviceCode' => $designModel->device_code,
            'sort' => $designModel->sort,
            'code' => $designModel->code,
            'printType' => $designModel->print_type
        ];
    }
}
