<?php

namespace Tests;

use Tests\TestCase;
use Database\Seeders\CaseTypeSeeder;
use Database\Seeders\BrandDeviceSeeder;
use Database\Seeders\ColorSeeder;
use Database\Seeders\DesignSeeder;
use App\Models\Color;
use App\Models\Brand;
use App\Models\CaseType;
use App\Models\Task;
use App\Models\Design;
use App\Models\DesignMaterial;
use App\Models\Device;
use App\Models\TaskItem;

abstract class DesignTestCase extends TestCase
{
    protected function setUp(): void
    {
        parent::setUp();
        $designPrefix = 'tests/Stub/design';
        $dirs = ['mask','material','output','pending','shadow'];
        $subDirs = ['print', 'web'];
        if (!is_dir("{$designPrefix}")) {
            mkdir("{$designPrefix}");
        }
        foreach ($dirs as $dir) {
            if (!is_dir("{$designPrefix}/{$dir}")) {
                mkdir("{$designPrefix}/{$dir}");
            }
            foreach ($subDirs as $subDir) {
                if (!is_dir("{$designPrefix}/{$dir}/{$subDir}")) {
                    mkdir("{$designPrefix}/{$dir}/{$subDir}");
                }
            }
        }
        $customizedPrefix = 'tests/Stub/printing-console';
        if (!is_dir("{$customizedPrefix}")) {
            mkdir("{$customizedPrefix}");
        }
        $dirs = ['original', 'preview'];
        foreach ($dirs as $dir) {
            if (!is_dir("{$customizedPrefix}/{$dir}")) {
                mkdir("{$customizedPrefix}/{$dir}");
            }
            if (!is_dir("{$customizedPrefix}/{$dir}/ModNX")) {
                mkdir("{$customizedPrefix}/{$dir}/ModNX");
            }
            if (!is_dir("{$customizedPrefix}/{$dir}/ModNX/26")) {
                mkdir("{$customizedPrefix}/{$dir}/ModNX/26");
            }
            if (!is_dir("{$customizedPrefix}/{$dir}/ModNX/26/054")) {
                mkdir("{$customizedPrefix}/{$dir}/ModNX/26/054");
            }
        }
    }

    protected function createMetaData()
    {
        if (CaseType::all()->count() === 0) {
            app(CaseTypeSeeder::class)->run();
        }
        if (Brand::all()->count() === 0) {
            app(BrandDeviceSeeder::class)->run();
        }
        if (Color::all()->count() === 0) {
            app(ColorSeeder::class)->run();
        }
        if (Design::all()->count() === 0) {
            app(DesignSeeder::class)->run();
        }
    }

    protected function createFakeDesignTask($status, $caseType = 'ModNX', $colors = ['26'], $devices = ['054'])
    {
        $this->createMetaData();
        // 設計編碼-設計印程-手機型號-顏色-產品
        $payload = [
            'case_type' => $caseType,
            'colors' => $colors,
            'devices' => $devices,
            'sort' => 'OB',
            'code' => 'OB01',
            'print_type' => 'CW',
            'version' => 1
        ];
        $colorText = implode(',', $payload['colors']);
        $deviceText = implode(',', $payload['devices']);
        $taskName = "{$payload['code']}-{$payload['print_type']}-{$deviceText}-{$colorText}-{$payload['case_type']}";
        $filename = "{$payload['case_type']}-{$payload['code']}-{$payload['version']}";
        $designMaterialModel = $this->createFakeDesignMaterial($filename);
        $this->saveDesignMaterial($payload['code'], $designMaterialModel->filename, 'print');
        $this->saveDesignMaterial($payload['code'], $designMaterialModel->filename, 'web');
        $taskModel = Task::create([
            'type' => 'design',
            'status' => $status,
            'name' => $taskName,
            'payload' => json_encode($payload),
        ]);

        return [
            'task' => $taskModel,
            'designMaterial' => $designMaterialModel
        ];
    }

    protected function saveDesignMaterial($designCode, $filename, $type)
    {
        $extension = ($type === 'print')? 'pdf' : 'png';
        $folder = "tests/Stub/design/material/{$type}";
        $filename = "{$filename}.{$extension}";
        $sourcePath = "tests/Stub/material-{$type}-{$designCode}.{$extension}";
        file_put_contents("{$folder}/{$filename}", file_get_contents($sourcePath));
    }

    protected function createFakeDesignTaskItem($task, $designMaterial, $haveOutput = false)
    {
        $data = [];
        $payload = json_decode($task['payload'], true);
        $filename = "{$payload['case_type']}-{$payload['code']}-1";
        $this->createFakeDesignMaterial($filename);
        foreach ($payload['devices'] as $deviceCode) {
            foreach ($payload['colors'] as $colorCode) {
                $relativePrintPath = $this->savePendingFile($payload, $filename, $colorCode, $deviceCode, 'print');
                $relativeWebPath = $this->savePendingFile($payload, $filename, $colorCode, $deviceCode, 'web');
                $design = Design::create([
                    'design_material_id' => $designMaterial['id'],
                    'case_type' => $payload['case_type'],
                    'color_code' => $colorCode,
                    'device_code' => $deviceCode,
                    'sort' => $payload['sort'],
                    'code' => $payload['code'],
                    'print_type' => $payload['print_type'],
                ]);
                array_push($data, [
                    'task_id' => $task['id'],
                    'design_id'=> $design->id,
                    'material_print_path' => $relativePrintPath,
                    'material_web_path' => $relativeWebPath,
                    'is_allowed' => true
                ]);
                if ($haveOutput) {
                    $this->moveOutputFile($relativePrintPath, 'print');
                    $this->moveOutputFile($relativeWebPath, 'web');
                }
            }
        }
        Task::where('id', $task['id'])->update(['quantity' => count($data)]);
        TaskItem::insert($data);
        return $data;
    }

    protected function savePendingFile($payload, $sourceFilename, $colorCode, $deviceCode, $type)
    {
        $extension = ($type === 'print')? 'pdf' : 'png';
        $sourceFolder = env('DesignFileSourceDir') . "/material/{$type}";
        $sourceFilename = "{$sourceFilename}.{$extension}";
        $sourcePath = "{$sourceFolder}/{$sourceFilename}";
        $folder = "pending/{$type}";
        $filename = "{$payload['case_type']}-{$colorCode}-{$deviceCode}-{$payload['code']}-{$payload['print_type']}.{$extension}";
        $destPath = env('DesignFileSourceDir')."/{$folder}/{$filename}";
        file_put_contents($destPath, file_get_contents($sourcePath));
        return "{$folder}/{$filename}";
    }

    protected function moveOutputFile($filepath, $type)
    {
        $sourcePath = env('DesignFileSourceDir'). "/{$filepath}";
        $folder = env('DesignFileSourceDir'). "/output/{$type}";
        $filename = basename($filepath);
        if ($type === 'web') {
            $filename = str_replace(['-0.png', '-1.png'], '.png', $filename);
            $filename = str_replace(['-K','-W','-CW'], '', $filename);
        }
        $outputPath = "{$folder}/{$filename}";
        file_put_contents($outputPath, file_get_contents($sourcePath));
    }

    protected function createFakeDeviceTask($status)
    {
        $this->createMetaData();
        Device::where('code', '074')->delete();
        // 手機型號-顏色-產品
        $payload = [
            'brand_code' => '03',
            'code' => '074',
            'name' => 'Samsung S9',
            'case_type' => 'SolidSuit',
            'colors' => ['52','53'],
            'version' => 1
        ];
        $colorText = implode(',', $payload['colors']);
        $taskName = "{$payload['code']}-{$colorText}-{$payload['case_type']}";
        $this->saveDesignMask($payload['case_type'], $payload['code'], 'print');
        $this->saveDesignMask($payload['case_type'], $payload['code'], 'web');
        $this->saveDesignShadow($payload['case_type'], $payload['code'], 'web');
        Device::create([
            'brand_code' => $payload['brand_code'],
            'code' => $payload['code'],
            'name' => $payload['name'],
            'have_shadow' => 1
        ]);
        return Task::create([
            'type' => 'device',
            'status' => $status,
            'name' => $taskName,
            'payload' => json_encode($payload),
        ]);
    }

    protected function createFakeDesignMaterial($filename)
    {
        $this->createMetaData();
        list($caseType, $code, $version) = explode('-', $filename);

        return DesignMaterial::create([
            'filename' => $filename,
            'case_type' => $caseType,
            'code' => $code,
            'version' => $version
        ]);
    }

    protected function saveDesignMask($caseType, $deviceCode, $type)
    {
        $extension = ($type === 'print')? 'pdf' : 'png';
        $folder = "tests/Stub/design/mask/{$type}";
        $filename = "mask-{$type}-{$caseType}-{$deviceCode}.{$extension}";
        $sourcePath = "tests/Stub/mask-{$type}-{$caseType}-{$deviceCode}.{$extension}";
        file_put_contents("{$folder}/{$filename}", file_get_contents($sourcePath));
    }

    protected function saveDesignShadow($caseType, $deviceCode, $type)
    {
        $extension = ($type === 'print')? 'pdf' : 'png';
        $folder = "tests/Stub/design/shadow/{$type}";
        $filename = "shadow-{$type}-{$caseType}-{$deviceCode}.{$extension}";
        $sourcePath = "tests/Stub/shadow-{$type}-{$caseType}-{$deviceCode}.{$extension}";
        file_put_contents("{$folder}/{$filename}", file_get_contents($sourcePath));
    }
}
