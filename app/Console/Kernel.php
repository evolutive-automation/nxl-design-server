<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        'App\Console\Commands\DeleteLog',
        'App\Console\Commands\AssignTask',
        'App\Console\Commands\CheckPendingFile',
        'App\Console\Commands\SyncDesign',
        'App\Console\Commands\SyncFile'
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->command('check:pendingFile')->timezone('Asia/Taipei')->everyTwoMinutes()->withoutOverlapping(10);
        $schedule->command('assign:task')->timezone('Asia/Taipei')->everyMinute()->withoutOverlapping(10);
        $schedule->command('notify:task')->timezone('Asia/Taipei')->everyMinute()->withoutOverlapping(10);
        $schedule->command('delete:log --days=2')->timezone('Asia/Taipei')->dailyAt('00:00')->withoutOverlapping();
        $schedule->command('sync:file')->timezone('Asia/Taipei')->cron('*/20 * * * *')->withoutOverlapping();
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
