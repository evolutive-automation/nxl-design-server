<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\WithFaker;
use App\Services\GetDesignMaterialService;
use Tests\DesignTestCase;

class GetDesignMaterialServiceTest extends DesignTestCase
{
    use WithFaker, RefreshDatabase, DatabaseMigrations;

    public function test_it_can_get_design_material_list()
    {
        $this->createMetaData();

        $data = app(GetDesignMaterialService::class)
            ->exec();

        $this->assertCount(2, $data);
    }
}
