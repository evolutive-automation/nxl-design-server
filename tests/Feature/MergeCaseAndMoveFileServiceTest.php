<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\WithFaker;
use App\Services\MergeCaseAndMoveFileService;
use Tests\DesignTestCase;
use App\Transformers\Task\DetailTaskTransformer;
use Intervention\Image\Facades\Image;

class MergeCaseAndMoveFileServiceTest extends DesignTestCase
{
    use WithFaker, RefreshDatabase, DatabaseMigrations;

    public function test_it_can_merge_modnx_case()
    {
        $expectedHavePrint = true;
        $expectedCaseType = 'ModNX';
        $expectedColors = ['26'];
        $expectedCaseColor = config('enum.caseColor.ModNX')[0];
        $fakeDesignTask = $this->createFakeDesignTask('made', $expectedCaseType, $expectedColors);
        $task = $fakeDesignTask['task'];
        $designMaterial = $fakeDesignTask['designMaterial'];
        $this->createFakeDesignTaskItem($task, $designMaterial, $expectedHavePrint);
        $task->taskItems()->update([
            'have_print' => true,
            'have_web' => true,
            'is_allowed' => true,
        ]);
        $fakeTask = app(DetailTaskTransformer::class)->exec($task);
        $expectedPayload = $this->handleExpectedPayload($fakeTask);
        $expectedPaths = $this->handleExpectedPaths($expectedCaseColor, $expectedPayload);
        $this->mockInterventionImage($expectedCaseType, $expectedPaths);
        $result = app(MergeCaseAndMoveFileService::class)
            ->setPayload($expectedPayload)
            ->exec();

        foreach ($result as $path) {
            $this->assertEquals($expectedPaths['destPath'], $path);
        }
    }

    public function test_it_can_merge_clear_case()
    {
        $expectedHavePrint = true;
        $expectedCaseType = 'Clear';
        $expectedColors = ['F5'];
        $expectedDevices = ['232'];
        $expectedCaseColor = config('enum.caseColor.Clear')[0];
        $fakeDesignTask = $this->createFakeDesignTask('made', $expectedCaseType, $expectedColors, $expectedDevices);
        $task = $fakeDesignTask['task'];
        $designMaterial = $fakeDesignTask['designMaterial'];
        $this->createFakeDesignTaskItem($task, $designMaterial, $expectedHavePrint);
        $task->taskItems()->update([
            'have_print' => true,
            'have_web' => true,
            'is_allowed' => true,
        ]);
        $fakeTask = app(DetailTaskTransformer::class)->exec($task);
        $expectedPayload = $this->handleExpectedPayload($fakeTask);
        $expectedPaths = $this->handleExpectedPaths($expectedCaseColor, $expectedPayload);
        $this->mockInterventionImage($expectedCaseType, $expectedPaths);
        $result = app(MergeCaseAndMoveFileService::class)
            ->setPayload($expectedPayload)
            ->exec();

        foreach ($result as $path) {
            $this->assertEquals($expectedPaths['destPath'], $path);
        }
    }

    public function test_it_can_merge_airpods_170_case()
    {
        $expectedHavePrint = true;
        $expectedCaseType = 'AirPodsCase';
        $expectedColors = ['H6'];
        $expectedDevices = ['171'];
        $expectedCaseColor = config('enum.caseColor.AirPodsCase')[0];
        $fakeDesignTask = $this->createFakeDesignTask('made', $expectedCaseType, $expectedColors, $expectedDevices);
        $task = $fakeDesignTask['task'];
        $designMaterial = $fakeDesignTask['designMaterial'];
        $this->createFakeDesignTaskItem($task, $designMaterial, $expectedHavePrint);
        $task->taskItems()->update([
            'have_print' => true,
            'have_web' => true,
            'is_allowed' => true,
        ]);
        $fakeTask = app(DetailTaskTransformer::class)->exec($task);
        $expectedPayload = $this->handleExpectedPayload($fakeTask);
        $expectedPaths = $this->handleExpectedPaths($expectedCaseColor, $expectedPayload);
        $this->mockInterventionImage($expectedCaseType, $expectedPaths);
        $result = app(MergeCaseAndMoveFileService::class)
            ->setPayload($expectedPayload)
            ->exec();

        foreach ($result as $path) {
            $this->assertEquals($expectedPaths['destPath'], $path);
        }
    }

    public function test_it_can_merge_airpods_171_case()
    {
        $expectedHavePrint = true;
        $expectedCaseType = 'AirPodsCase';
        $expectedColors = ['H6'];
        $expectedDevices = ['170'];
        $expectedCaseColor = config('enum.caseColor.AirPodsCase')[0];
        $fakeDesignTask = $this->createFakeDesignTask('made', $expectedCaseType, $expectedColors, $expectedDevices);
        $task = $fakeDesignTask['task'];
        $designMaterial = $fakeDesignTask['designMaterial'];
        $this->createFakeDesignTaskItem($task, $designMaterial, $expectedHavePrint);
        $task->taskItems()->update([
            'have_print' => true,
            'have_web' => true,
            'is_allowed' => true,
        ]);
        $fakeTask = app(DetailTaskTransformer::class)->exec($task);
        $expectedPayload = $this->handleExpectedPayload($fakeTask);
        $expectedPaths = $this->handleExpectedPaths($expectedCaseColor, $expectedPayload);
        $this->mockInterventionImage($expectedCaseType, $expectedPaths);
        $result = app(MergeCaseAndMoveFileService::class)
            ->setPayload($expectedPayload)
            ->exec();

        foreach ($result as $path) {
            $this->assertEquals($expectedPaths['destPath'], $path);
        }
    }

    public function test_it_can_merge_solidsuit_case()
    {
        $expectedHavePrint = true;
        $expectedCaseType = 'SolidSuit';
        $expectedColors = ['52'];
        $expectedCaseColor = config('enum.caseColor.SolidSuit')[0];
        $fakeDesignTask = $this->createFakeDesignTask('made', $expectedCaseType, $expectedColors);
        $task = $fakeDesignTask['task'];
        $designMaterial = $fakeDesignTask['designMaterial'];
        $this->createFakeDesignTaskItem($task, $designMaterial, $expectedHavePrint);
        $task->taskItems()->update([
            'have_print' => true,
            'have_web' => true,
            'is_allowed' => true,
        ]);
        $fakeTask = app(DetailTaskTransformer::class)->exec($task);
        $expectedPayload = $this->handleExpectedPayload($fakeTask);
        $expectedPaths = $this->handleExpectedPaths($expectedCaseColor, $expectedPayload);
        $this->mockInterventionImage($expectedCaseType, $expectedPaths);
        $result = app(MergeCaseAndMoveFileService::class)
            ->setPayload($expectedPayload)
            ->exec();

        foreach ($result as $path) {
            $this->assertEquals($expectedPaths['destPath'], $path);
        }
    }

    private function handleExpectedPayload($fakeTask)
    {
        $item = $fakeTask['items'][0];
        $design = $item['design'];
        $device = $design['device'];

        return [
            'id' => $item['id'],
            'task' => $fakeTask['type'],
            'case_type' => $design['caseType'],
            'color_code' => $design['colorCode'],
            'device_code' => $design['deviceCode'],
            'sort' => $design['sort'],
            'code' => $design['code'],
            'print_type' => $design['printType'],
            'device' => [
                'brand_code' => $device['brandCode'],
                'name' => $device['name'],
                'code' => $device['code']
            ],
            'outputPrintPath' => $item['outputPrintPath'],
            'outputWebPath' => $item['outputWebPath']
        ];
    }

    private function handleExpectedPaths($caseColor, $payload)
    {
        $caseType = $payload['case_type'];
        $deviceCode = $payload['device_code'];
        $result = [
            'casePath' => env('CaseFileSourceDir')."/{$caseType}/{$caseColor}/{$caseType}-{$caseColor}-{$deviceCode}.png",
            'designPath' => env('DesignFileSourceDir')."/{$payload['outputWebPath']}",
            'destPath' => env('DesignFileSourceDir')."/mergeCase/web/{$caseType}-{$caseColor}-{$deviceCode}-{$payload['code']}.png"
        ];
        if ($caseType === 'ModNX') {
            $result['backPath'] = env('CaseFileSourceDir')."/{$caseType}/26/{$caseType}-26-{$deviceCode}.png";
            $result['destPath'] = env('DesignFileSourceDir')."/mergeCase/web/{$caseType}-26-{$caseColor}-{$deviceCode}-{$payload['code']}.png";
        }
        return $result;
    }

    private function mockInterventionImage($caseType, $paths)
    {
        $imageFacade = Image::spy();
        if ($caseType === 'ModNX') {
            Image::shouldReceive('make')
                ->with($paths['backPath'])
                ->once()
                ->andReturn($imageFacade);
            Image::shouldReceive('insert')
                ->with($paths['designPath'])
                ->once()
                ->andReturn($imageFacade);
            Image::shouldReceive('insert')
                ->with($paths['casePath'])
                ->once()
                ->andReturn($imageFacade);
        } else {
            Image::shouldReceive('make')
                ->with($paths['casePath'])
                ->once()
                ->andReturn($imageFacade);
            Image::shouldReceive('insert')
                ->with($paths['designPath'])
                ->once()
                ->andReturn($imageFacade);
        }
        Image::shouldReceive('save')
            ->with($paths['destPath'])
            ->once();
    }
}
