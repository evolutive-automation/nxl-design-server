<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['prefix' => 'tasks'], function () {
    Route::get('/no-check', 'TaskController@getNotYetCheckTasks');
    Route::get('/{id}/mark-cancelled', 'TaskController@markAsCancelled');
    Route::get('/{id}/mark-urgent', 'TaskController@markAsUrgent');
    Route::get('/{id}/mark-checked', 'TaskController@markAsChecked');
    Route::post('/create-design', 'TaskController@createDesignTask');
    Route::post('/create-device', 'TaskController@createDeviceTask');
    Route::delete('/{id}', 'TaskController@deleteTask');
    Route::get('/{id}/download-zip', 'TaskController@downloadZip');
});

Route::group(['prefix' => 'task-items'], function () {
    Route::get('/no-check/{taskId}', 'TaskItemController@getNotYetCheckItem');
    Route::get('/{id}/mark-not-allowed', 'TaskItemController@markAsNotAllowed');
});

Route::group(['prefix' => 'case-types'], function () {
    Route::get('/search', 'CaseTypeController@search');
});

Route::group(['prefix' => 'colors'], function () {
    Route::get('/gmg', 'ColorController@getGmgColor');
});

Route::group(['prefix' => 'brands'], function () {
    Route::get('/search', 'BrandController@search');
});

Route::group(['prefix' => 'devices'], function () {
    Route::get('/search', 'DeviceController@search');
});

Route::group(['prefix' => 'design-material'], function () {
    Route::get('/search', 'DesignMaterialController@search');
});

Route::group(['prefix' => 'livetest'], function () {
    Route::get('/jeffery', 'LiveController@jeffery');
});
