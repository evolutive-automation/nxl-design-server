<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\CaseType;
use App\Models\CaseTypeItem;
use Illuminate\Support\Str;

class CaseTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                "name" => "PlayProof",
                "print_face" => "front",
                "items" => [
                    "PPA"
                ]
            ],
            [
                "name" => "SolidSuit",
                "print_face" => "front",
                "items" => [
                    "SSA",
                    "BCS",
                    "SMS",
                    "HKS",
                    "KSS",
                    "MMS",
                    "PXS",
                    "TSS",
                    "SWS",
                    "SBS",
                    "PMS",
                    "BTS",
                    "NBS",
                    "DSS",
                    "MKS",
                    "FDS",
                    "HPS",
                ]
            ],
            [
                "name" => "Mod",
                "print_face" => "back",
                "items" => [
                    "3PB",
                    "EC",
                    "KC"
                ]
            ],
            [
                "name" => "ModNX",
                "print_face" => "back",
                "items" => [
                    "NPB",
                    "NX",
                    "KPB",
                    "KI",
                    "SMN",
                    "SM",
                    "BCN",
                    "BC",
                    "KX",
                    "MMN",
                    "MM",
                    "PXN",
                    "PX",
                    "TSN",
                    "TS",
                    "SWN",
                    "SW",
                    "SBN",
                    "SB",
                    "PMN",
                    "PM",
                    "BTN",
                    "BT",
                    "NBN",
                    "NB",
                    "DSN",
                    "DS",
                    "MKN",
                    "MK",
                    "FDN",
                    "FD",
                    "HPN",
                    "HP"
                ]
            ],
            [
                "name" => "AirPodsCase",
                "print_face" => "front",
                "items" => [
                    "ABA",
                    "PMA",
                    "KPA",
                    "NBA",
                    "DSA",
                    "MKA",
                    "FDA",
                    "HPA"
                ]
            ],
            [
                "name" => 'Clear',
                "print_face" => "front",
                "items" => [
                    "CCA"
                ]
            ]
        ];
        foreach ($data as $row) {
            $caseTypePayload = [
                'name' => $row['name'],
                'print_face' => $row['print_face']
            ];
            if (in_array($row['name'], ['ModNX','SolidSuit','AirPodsCase'])) {
                $caseTypePayload['active'] = true;
            }
            $caseTypeModel = CaseType::firstOrCreate(['name' => $caseTypePayload['name']]);
            $caseTypeModel->update($caseTypePayload);
            foreach ($row['items'] as $item) {
                // $packs = $this->getPack($row['name'], $item);
                $itemPayload = [
                    'type' => $caseTypeModel->name,
                    'code' => $item,
                    // 'pack' => implode('、', $packs)
                ];
                $caseTypeItemModel = CaseTypeItem::firstOrCreate([
                    'type' => $itemPayload['type'],
                    'code' => $itemPayload['code']
                ]);
                $caseTypeItemModel->update($itemPayload);
            }
        }
    }

    private function getPack($type, $code)
    {
        $pack = ['特殊彩盒'];
        if (
            ($type === 'SolidSuit' && $code === 'SSA') ||
            ($type === 'AirPodsCase' && $code === 'ABA')
        ) {
            $pack = ['一般彩盒'];
        }

        if ($type === 'ModNX' &&
            ((Str::length($code) === 2) || $code === 'NPB')
        ) {
            $pack = ['泡殼'];
            if ($code === 'KX') {
                $pack = ['Kroma泡殼', 'Kroma信封套'];
            }
            if ($code === 'KI') {
                array_push($pack, 'Kitty信封套');
            }
        }
        return $pack;
    }
}
