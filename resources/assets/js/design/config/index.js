export const domain = process.env.APP_URL
export const apiToken = process.env.CUSTOMIZED_SERVER_API_TOKEN
export const fileServer = process.env.FileServerDomain + '/'
export const designDir = 'design'
export const caseDir = 'case'
