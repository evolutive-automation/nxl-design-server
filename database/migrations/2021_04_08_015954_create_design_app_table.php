<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDesignAppTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('case_types', function (Blueprint $table) {
            $table->id();
            $table->string('name')->index()->unique();
            $table->string('print_face')->default('');
            $table->string('version')->default('V001');
            $table->boolean('active')->default(false);
            $table->timestamps();
        });

        Schema::create('case_type_items', function (Blueprint $table) {
            $table->id();
            $table->string('type');
            $table->string('code')->index()->unique();
            $table->timestamps();

            $table->foreign('type')->references('name')->on('case_types');
        });

        Schema::create('brands', function (Blueprint $table) {
            $table->id();
            $table->string('name')->default('');
            $table->string('code')->index()->unique();
            $table->timestamps();
        });

        Schema::create('devices', function (Blueprint $table) {
            $table->id();
            $table->string('brand_code');
            $table->string('name')->default('');
            $table->string('code')->index()->unique();
            $table->boolean('have_shadow')->default(false);
            $table->timestamps();

            $table->foreign('brand_code')->references('code')->on('brands');
        });

        Schema::create('colors', function (Blueprint $table) {
            $table->id();
            $table->boolean('active')->default(true);
            $table->string('english_name')->default('');
            $table->string('chinese_name')->default('');
            $table->string('color_hex')->default('');
            $table->string('code')->index()->unique();
            $table->string('origin_code')->default('');
            $table->timestamps();
        });

        Schema::create('design_materials', function (Blueprint $table) {
            $table->id();
            $table->string('filename');
            $table->string('case_type')->default('');
            $table->string('code')->default('');
            $table->integer('version')->default(1);
            $table->timestamps();
        });

        Schema::create('designs', function (Blueprint $table) {
            $table->id();
            $table->foreignId('design_material_id')->nullable();
            $table->string('case_type');
            $table->string('color_code');
            $table->string('device_code');
            $table->string('sort')->default('');
            $table->string('code');
            $table->string('print_type')->default('');
            $table->timestamps();

            $table->foreign('design_material_id')->references('id')->on('design_materials');
            $table->foreign('case_type')->references('name')->on('case_types');
            $table->foreign('color_code')->references('code')->on('colors');
            $table->foreign('device_code')->references('code')->on('devices');
        });

        Schema::create('tasks', function (Blueprint $table) {
            $table->id();
            $table->string('type');
            $table->string('status')->default('pending');
            $table->string('name');
            $table->json('payload');
            $table->integer('quantity')->default(0);
            $table->integer('done_quantity')->default(0);
            $table->integer('ng_quantity')->default(0);
            $table->timestamp('started_at')->nullable();
            $table->timestamp('ended_at')->nullable();
            $table->timestamp('checked_at')->nullable();
            $table->timestamp('notified_at')->nullable();
            $table->timestamps();
        });

        Schema::create('task_items', function (Blueprint $table) {
            $table->id();
            $table->foreignId('task_id');
            $table->foreignId('design_id');
            $table->boolean('have_print')->default(false);
            $table->boolean('have_web')->default(false);
            $table->boolean('is_allowed')->default(true)->index();
            $table->string('material_print_path');
            $table->string('material_web_path');
            $table->timestamps();

            $table->foreign('task_id')->references('id')->on('tasks');
            $table->foreign('design_id')->references('id')->on('designs');
        });

        Schema::create('external_api_logs', function (Blueprint $table) {
            $table->id();
            $table->boolean('is_success');
            $table->string('url');
            $table->bigInteger('task_item_id')->nullable();
            $table->longText('request')->nullable();
            $table->longText('response')->nullable();
            $table->timestamps();
        });

        Schema::create('task_item_fail_jobs', function (Blueprint $table) {
            $table->id();
            $table->string('job_name');
            $table->string('job_key')->default('');
            $table->longText('payload')->nullable();
            $table->longText('error_message')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('task_items');
        Schema::dropIfExists('tasks');
        Schema::dropIfExists('designs');
        Schema::dropIfExists('colors');
        Schema::dropIfExists('devices');
        Schema::dropIfExists('brands');
        Schema::dropIfExists('case_type_items');
        Schema::dropIfExists('case_types');
        Schema::dropIfExists('design_materials');
        Schema::dropIfExists('external_api_logs');
        Schema::dropIfExists('task_item_fail_jobs');
    }
}
