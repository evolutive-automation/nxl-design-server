<?php

namespace Tests\Feature;

use App\Exceptions\AlreadyCheckedException;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\WithFaker;
use App\Services\MarkAsNotAllowedService;
use Tests\DesignTestCase;
use Carbon\Carbon;

class MarkAsNotAllowedServiceTest extends DesignTestCase
{
    use WithFaker, RefreshDatabase, DatabaseMigrations;

    public function test_it_can_mark_as_not_allowed_task_item()
    {
        $fakeDesignTask = $this->createFakeDesignTask('pending');
        $task = $fakeDesignTask['task'];
        $designMaterial = $fakeDesignTask['designMaterial'];
        $this->createFakeDesignTaskItem($task, $designMaterial);
        $taskItem = $task->taskItems->first();

        app(MarkAsNotAllowedService::class)
            ->setTaskItemId($taskItem->id)
            ->exec();

        $this->assertDatabaseHas('task_items', [
            'id' => $taskItem->id,
            'is_allowed' => 0
        ]);

        $this->assertDatabaseHas('tasks', [
            'id' => $task->id,
            'status' => 'made',
            'ng_quantity' => 1,
            'ended_at' => Carbon::now()->toDateTimeString()
        ]);
    }

    public function test_it_can_throw_exception()
    {
        $this->withExceptionHandling();
        $this->expectException(AlreadyCheckedException::class);
        $fakeDesignTask = $this->createFakeDesignTask('checked');
        $task = $fakeDesignTask['task'];
        $designMaterial = $fakeDesignTask['designMaterial'];
        $this->createFakeDesignTaskItem($task, $designMaterial);
        $taskItem = $task->taskItems->first();

        app(MarkAsNotAllowedService::class)
            ->setTaskItemId($taskItem->id)
            ->exec();
    }
}
