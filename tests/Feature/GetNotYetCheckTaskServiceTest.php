<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\WithFaker;
use App\Services\GetNotYetCheckTaskService;
use Carbon\Carbon;
use Tests\DesignTestCase;

class GetNotYetCheckTaskServiceTest extends DesignTestCase
{
    use WithFaker, RefreshDatabase, DatabaseMigrations;

    public function test_it_can_get_wait_check_task()
    {
        $this->createFakeDesignTask('pending');

        $tasks = app(GetNotYetCheckTaskService::class)
            ->exec();

        foreach ($tasks as $task) {
            $this->assertNotEquals('checked', $task['status']);
        }
    }

    public function test_it_can_get_made_task()
    {
        $expectedMinute = 3;
        $fakeDesignTask = $this->createFakeDesignTask('made');
        $task = $fakeDesignTask['task'];
        $task->started_at = Carbon::now();
        $task->ended_at = Carbon::now()->addMinutes($expectedMinute);
        $task->save();

        $tasks = app(GetNotYetCheckTaskService::class)
            ->exec();

        foreach ($tasks as $task) {
            $this->assertEquals("{$expectedMinute}分鐘", $task['spentTime']);
            $this->assertNotEquals('checked', $task['status']);
        }
    }
}
