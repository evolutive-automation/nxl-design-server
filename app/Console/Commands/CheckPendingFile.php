<?php

namespace App\Console\Commands;

use App\Services\CheckMakingItemService;
use Illuminate\Console\Command;

class CheckPendingFile extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'check:pendingFile';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '檢查 Pending 的任務品項';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $start = microtime(true);
        app(CheckMakingItemService::class)
            ->exec();
        $end = microtime(true);
        $total = round($end - $start, 2);
        echo "檢查 Pending 任務品項成功，總花費{$total}秒\n";
    }
}
