<?php

namespace App\Exceptions;

use Exception;

class DuplicateDesignException extends Exception
{
    protected $message;

    public function __construct($message = 'duplicate design exception.')
    {
        parent::__construct($message);
    }

    public function getStatusCode()
    {
        return 400;
    }
}
