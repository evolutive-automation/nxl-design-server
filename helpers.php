<?php

use App\Exceptions\CantMoveFileException;
use App\Notifications\SlackNotification;
use Illuminate\Support\Facades\Notification;

if (!function_exists('ok')) {
    /**
     * @param  string|array|object  $data
     * @param  int  $status
     * @param  array  $headers
     * @param  int  $options
     * @return \Illuminate\Http\JsonResponse
     */
    function ok(array $data = [], int $status = 200, array $headers = [], $options = 0)
    {
        $data = array_merge(['ok' => true], $data);
        return response()->json($data, $status, $headers, $options);
    }
}

if (!function_exists('checkFolder')) {
    /**
     * @param string $folder
     * @return void
     */
    function checkFolder(string $folder)
    {
        if (!is_dir($folder)) {
            mkdir($folder);
        }
    }
}

if (!function_exists('moveFile')) {
    /**
     * @param string $path
     * @return void
     */
    function moveFile(string $sourcePath, string $destPath)
    {
        $cmd = "mv -f {$sourcePath} {$destPath}";
        exec($cmd, $output, $retval);
        if ($retval) {
            throw new CantMoveFileException("移動檔案到排圖資料夾失敗,{$sourcePath}");
        }
    }
}

if (!function_exists('sendSlackNotification')) {
    /**
     * @param string $path
     * @return void
     */
    function sendSlackNotification($title, $message)
    {
        $slackNotification = app(SlackNotification::class)
            ->setChannel('#設計出圖系統')
            ->setTitle($title)
            ->setMessage($message);
        Notification::route('slack', env('SLACK_URL'))
            ->notify($slackNotification);
    }
}
