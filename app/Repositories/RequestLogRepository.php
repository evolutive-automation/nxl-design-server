<?php

namespace App\Repositories;

use App\Models\RequestLog;
use App\Repositories\Repository;
use App\Transformers\RequestLog\RequestLogTransformer;

class RequestLogRepository extends Repository
{
    protected $model = RequestLog::class;

    protected $transformer = RequestLogTransformer::class;

    public function deleteBeforeDate($date)
    {
        return $this->model::where('created_at', '<', $date)
            ->delete();
    }
}
