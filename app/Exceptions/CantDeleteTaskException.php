<?php

namespace App\Exceptions;

use Exception;

class CantDeleteTaskException extends Exception
{
    protected $message;

    public function __construct($message = 'cant delete task exception.')
    {
        parent::__construct($message);
    }

    public function getStatusCode()
    {
        return 400;
    }
}
