<?php

namespace Tests\Feature;

use App\Jobs\SavePendingFileJob;
use App\Services\CreateDeviceTaskItemService;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\DesignTestCase;
use Carbon\Carbon;
use Illuminate\Support\Facades\Queue;

class CreateDeviceTaskItemServiceTest extends DesignTestCase
{
    use WithFaker, RefreshDatabase, DatabaseMigrations;

    public function test_it_can_create_device_task_item()
    {
        $taskModel = $this->createFakeDeviceTask('pending');
        $task = $taskModel->toArray();
        $task['payload'] = json_decode($task['payload'], true);
        $this->saveDesignMaterial('PF16', 'SolidSuit-PF16-1', 'print');
        $this->saveDesignMaterial('PF16', 'SolidSuit-PF16-1', 'web');

        $taskItems = app(CreateDeviceTaskItemService::class)
            ->setTask($task)
            ->exec();

        $this->assertDatabaseHas('tasks', [
            'id' => $task['id'],
            'status' => 'making',
            'quantity' => count($taskItems),
            'started_at' => Carbon::now()
        ]);
        foreach ($taskItems as $taskItem) {
            $this->assertDatabaseHas('task_items', [
                'task_id' => $taskItem['task_id'],
                'design_id' => $taskItem['design_id'],
                'material_print_path' => $taskItem['material_print_path'],
                'material_web_path' => $taskItem['material_web_path']
            ]);
        }
    }
}
