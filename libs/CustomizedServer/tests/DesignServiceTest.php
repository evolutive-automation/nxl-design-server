<?php

namespace Libs\CustomizedServer\tests;

use Tests\TestCase;
use Libs\CustomizedServer\DesignService;
use GuzzleHttp\Client;

class DesignServiceTest extends TestCase
{
    public function test_it_can_update_design()
    {
        $form = $this->mockUpdateDesignForm();
        $domain = env('CUSTOMIZED_SERVER_DOMAIN');
        $token = env('CUSTOMIZED_SERVER_API_TOKEN');
        $mock = $this->mock(Client::class);
        $mock->shouldReceive('request')
            ->with('POST', "{$domain}/api/designs", [
                'headers' => [
                    'Content-Type' => 'application/json',
                    'CUSTOMIZED-SERVER-API-TOKEN' => $token
                ],
                'json' => $form
            ])
            ->once()
            ->andReturn($this->getMockResponse([
                'ok' => true
            ]));

        $data = app(DesignService::class)
            ->updateDesign($form);

        $this->assertEquals(['ok' => true], $data);
    }

    public function test_it_can_get_design()
    {
        $form = $this->mockGetDesignForm();
        $domain = env('CUSTOMIZED_SERVER_DOMAIN');
        $token = env('CUSTOMIZED_SERVER_API_TOKEN');
        $mock = $this->mock(Client::class);
        $mock->shouldReceive('request')
            ->with('POST', "{$domain}/api/designs/get-by-file", [
                'headers' => [
                    'Content-Type' => 'application/json',
                    'CUSTOMIZED-SERVER-API-TOKEN' => $token
                ],
                'json' => $form
            ])
            ->once()
            ->andReturn($this->getMockResponse([
                'ok' => true
            ]));

        $data = app(DesignService::class)
            ->getDesignByFile($form);

        $this->assertEquals(['ok' => true], $data);
    }

    private function mockGetDesignForm()
    {
        return [
            'deviceCode' => 148,
            'filenames' => ["ModNX-A002","ModNX-A003"]
        ];
    }

    private function mockUpdateDesignForm()
    {
        return [
            "id" => 1,
            "task" => "design",
            "case_type" => "ModNX",
            "color_code" => "26",
            "device_code" => "054",
            "sort" => "OB",
            "code" => "OB01",
            "print_type" => "CW",
            "device" => [
              "brand_code" => "01",
              "name" => "iPhone 7 / 7s",
              "code" => "054"
            ]
        ];
    }
}
