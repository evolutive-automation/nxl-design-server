<?php

namespace App\Services;

use App\Exceptions\CantDeleteTaskException;
use App\Repositories\TaskRepository;
use App\Services\Service;

class DeletePendingTaskService extends Service
{
    /** @var int $taskId */
    protected $taskId;

    /** @var TaskRepository $taskRepository */
    protected $taskRepository;

    public function __construct(
        TaskRepository $taskRepository
    ) {
        $this->taskRepository = $taskRepository;
    }

    public function setTaskId($taskId)
    {
        $this->taskId = $taskId;

        return $this;
    }

    public function exec()
    {
        $task = $this->getTask();
        if ($task['status'] !== 'pending') {
            throw new CantDeleteTaskException("該Task狀態為{$task['status']}，不可被刪除");
        }
        $this->deleteTask();
    }

    private function getTask()
    {
        return $this->taskRepository->findOrFail($this->taskId);
    }

    private function deleteTask()
    {
        return $this->taskRepository->delete($this->taskId);
    }
}
