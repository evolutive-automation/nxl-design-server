<?php

namespace App\Transformers\ExternalLog;

use App\Transformers\Transformer;
use App\Models\ExternalApiLog;
use Carbon\Carbon;

class ExternalApiLogTransformer extends Transformer
{
    /**
     * @param ExternalApiLog $externalApiLogModel
     *
     * @return array
     */
    public function exec($externalApiLogModel)
    {
        return [
            'isSuccess' => $externalApiLogModel->is_success? true : false,
            'url' => $externalApiLogModel->url,
            'taskItemId' => $externalApiLogModel->task_item_id,
            'request' => json_decode($externalApiLogModel->request, true),
            'response' => json_decode($externalApiLogModel->response, true),
            'createdAt' => Carbon::parse($externalApiLogModel->created_at, 'UTC')->setTimezone('Asia/Taipei')->toDateTimeString()
        ];
    }
}
