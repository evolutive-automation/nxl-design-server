<?php

namespace Database\Factories;

use App\Models\CaseType;
use App\Models\Design;
use Illuminate\Database\Eloquent\Factories\Factory;

class DesignFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Design::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'type' => CaseType::factory()->create()->name,
            'device_code' => $this->faker->regexify('[0-9]{3}'),
            'color_code' => $this->faker->regexify('[0-9]{2}'),
            'code' => $this->faker->regexify('[A-Z]{1}[0-9]{3}'),
            'print_type' => $this->faker->randomElement(['F1','W2','M2'])
        ];
    }
}
