<?php

namespace App\Repositories;

use App\Repositories\Repository;
use App\Models\Task;
use App\Transformers\Task\TaskTransformer;

class TaskRepository extends Repository
{
    protected $model = Task::class;

    protected $transformer = TaskTransformer::class;

    public function getLimitWithPendingStatus($limitQuantity)
    {
        $collection = $this->model::where('status', 'pending')
            ->with($this->relations)
            ->take($limitQuantity)
            ->get();

        $transformer = app($this->transformer);

        return $collection->map(fn ($row) => $transformer->exec($row))->toArray();
    }

    public function getNotCheckedTasks()
    {
        $collection = $this->model::whereNotIn('status', ['checked', 'cancelled'])
            ->with($this->relations)
            ->get();

        $transformer = app($this->transformer);

        return $collection->map(fn ($row) => $transformer->exec($row))->toArray();
    }

    public function getNeedNotifyTasks()
    {
        $collection = $this->model::whereNull('notified_at')
            ->whereNotIn('status', ['penidng', 'cancelled'])
            ->with($this->relations)
            ->get();

        $transformer = app($this->transformer);

        return $collection->map(fn ($row) => $transformer->exec($row))->toArray();
    }
}
