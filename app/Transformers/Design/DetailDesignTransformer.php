<?php

namespace App\Transformers\Design;

use App\Models\Design;
use App\Transformers\DesignMaterial\DesignMaterialTransformer;
use App\Transformers\Device\DeviceTransformer;
use App\Transformers\Transformer;

class DetailDesignTransformer extends Transformer
{
    /**
     * @param Design $designModel
     *
     * @return array
     */
    public function exec($designModel)
    {
        return [
            'id' => $designModel->id,
            'caseType' => $designModel->case_type,
            'colorCode' => $designModel->color_code,
            'deviceCode' => $designModel->device_code,
            'sort' => $designModel->sort,
            'code' => $designModel->code,
            'printType' => $designModel->print_type,
            'device' => app(DeviceTransformer::class)->exec($designModel->device),
            'material' => app(DesignMaterialTransformer::class)->exec($designModel->material)
        ];
    }
}
