<?php

namespace App\Services;

use App\Repositories\DesignMaterialRepository;
use App\Repositories\DesignRepository;
use App\Repositories\DeviceRepository;
use App\Repositories\TaskRepository;
use App\Services\Service;

class CreateDesignTaskService extends Service
{
    /** @var array $payload */
    protected $payload;

    /** @var TaskRepository $taskRepository */
    protected $taskRepository;

    /** @var DesignRepository $designRepository */
    protected $designRepository;

    /** @var DeviceRepository $deviceRepository */
    protected $deviceRepository;

    /** @var DesignMaterialRepository $designMaterialRepository */
    protected $designMaterialRepository;

    public function __construct(
        TaskRepository $taskRepository,
        DesignRepository $designRepository,
        DeviceRepository $deviceRepository,
        DesignMaterialRepository $designMaterialRepository
    ) {
        $this->taskRepository = $taskRepository;
        $this->designRepository = $designRepository;
        $this->deviceRepository = $deviceRepository;
        $this->designMaterialRepository = $designMaterialRepository;
    }

    public function setPayload($payload)
    {
        $this->payload = $payload;

        return $this;
    }

    public function exec()
    {
        ini_set('memory_limit', '-1');
        $this->validateRule();
        $filename = $this->getDesignFilename();
        $task = $this->createTask();
        $this->payload['devices'] = $this->convertDevices();
        $this->createOrUpdateDesignMaterial($filename);
        $this->saveDesignMaterial($filename, 'print');
        $this->saveDesignMaterial($filename, 'web');

        return $task;
    }

    private function validateRule()
    {
        $this->validate($this->payload, [
            'case_type' => 'required|exists:case_types,name',
            'colors' => 'required|array',
            'devices' => 'required|array',
            'sort' => 'required|string',
            'code' => 'required|string',
            'print_type' => 'required|string',
            'version' => 'required|numeric',
            'material_print' => 'required|mimes:pdf',
            'material_web' => 'required|mimes:png'
        ]);
    }

    private function getDesignFilename()
    {
        // 產品-設計編碼-版本
        $caseType = $this->payload['case_type'];
        $code = $this->payload['code'];
        $version = $this->payload['version'];

        return "{$caseType}-{$code}-{$version}";
    }

    private function convertDevices()
    {
        $devices = $this->deviceRepository->search();
        $groups = collect($devices)->groupBy('type')->toArray();
        $data = [];
        foreach ($this->payload['devices'] as $item) {
            if ($item === 'iPhone全部') {
                $caseType = $this->payload['case_type'];
                $codes = collect($groups['iphone'])
                    ->filter(function ($device) use ($caseType) {
                        $result = true;
                        if ($caseType === 'SolidSuit' &&
                            $device['code'] === '001'
                        ) {
                            $result = false;
                        }
                        if ($caseType === 'ModNX' &&
                            in_array($device['code'], ['028','029'])
                        ) {
                            $result = false;
                        }
                        return $result;
                    })
                    ->pluck('code')
                    ->toArray();
                $data = array_merge($data, $codes);
            } elseif ($item === 'Android全部') {
                $codes = collect($groups['android'])->pluck('code')->toArray();
                $data = array_merge($data, $codes);
            } elseif ($item === 'Airpods全部') {
                $codes = collect($groups['airpods'])->pluck('code')->toArray();
                $data = array_merge($data, $codes);
            } elseif ($item === 'iPhone11-13全部') {
                $codes = ['147','148','149','184','185','187','230','228','231','232'];
                $data = array_merge($data, $codes);
            } else {
                array_push($data, $item);
            }
        }
        return collect($data)->unique()->toArray();
    }

    private function createTask()
    {
        /**
         * 任務名稱：
         * 裝置任務 => 型號-顏色-產品
         * 設計任務 => 設計代碼-印程-型號-顏色-產品
         */
        $temp = $this->payload;
        unset($temp['material_print']);
        unset($temp['material_web']);
        $deviceText = implode(',', $this->payload['devices']);
        $colorText = implode(',', $this->payload['colors']);
        $caseType = $this->payload['case_type'];
        $code = $this->payload['code'];
        $printType = $this->payload['print_type'];
        $taskName = "{$code}-{$printType}-{$deviceText}-{$colorText}-{$caseType}";
        $payload = [
            'type' => 'design',
            'name' => $taskName,
            'payload' => json_encode($temp)
        ];
        return $this->taskRepository->create($payload);
    }

    private function createOrUpdateDesignMaterial($filename)
    {
        list($caseType, $code, $version) = explode('-', $filename);
        $payload = [
            'filename' => $filename,
            'case_type' => $caseType,
            'code' => $code,
            'version' => $version
        ];

        return $this->designMaterialRepository->createOrUpdate($payload, ['filename']);
    }

    private function saveDesignMaterial($filename, $type)
    {
        $destFolder = env('DesignFileSourceDir') . "/material/{$type}";
        $extension = ($type === 'print')? 'pdf' : 'png';
        $destPath = "{$destFolder}/{$filename}.{$extension}";
        file_put_contents($destPath, file_get_contents($this->payload["material_{$type}"]));
    }
}
