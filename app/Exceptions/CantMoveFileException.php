<?php

namespace App\Exceptions;

use Exception;

class CantMoveFileException extends Exception
{
    protected $message;

    public function __construct($message = 'cant move file exception.')
    {
        parent::__construct($message);
    }

    public function getStatusCode()
    {
        return 500;
    }
}
