<?php

namespace App\Http\Middleware;

use Closure;

class CheckTokenMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $envToken = env('DESIGN_SERVER_API_TOKEN', '');
        $token = $request->header('DESIGN-SERVER-API-TOKEN');

        if ($envToken === $token) {
            return $next($request);
        }

        return response()->json([
            'ok' => false,
            'message' => 'Access Deny'
        ], 401);
    }
}
