<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Services\DeleteRequestLogService;

class DeleteLog extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'delete:log {--days=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '清除 request logs';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $start = microtime(true);
        $days = $this->option('days');
        app(DeleteRequestLogService::class)
            ->setDays($days)
            ->exec();
        $end = microtime(true);
        $total = round($end - $start, 2);
        echo "清除log成功，總花費{$total}秒\n";
    }
}
