<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\Repositories\TaskItemFailJobRepository;
use Illuminate\Validation\ValidationException;
use Exception;
use App\Notifications\SlackNotification;
use Illuminate\Support\Facades\Notification;
use Illuminate\Support\Arr;

class DeletePendingFileJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /** @var array $taskItem */
    public $taskItem;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(array $taskItem)
    {
        $this->taskItem = $taskItem;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $this->deleteFailJob($this->taskItem);
        if ($this->taskItem['havePrint']) {
            $pendingPath = env('DesignFileSourceDir'). "/{$this->taskItem['materialPrintPath']}";
            if (file_exists($pendingPath)) {
                unlink($pendingPath);
            }
        }
        if ($this->taskItem['haveWeb']) {
            $pendingPath = env('DesignFileSourceDir'). "/{$this->taskItem['materialWebPath']}";
            if (file_exists($pendingPath)) {
                unlink($pendingPath);
            }
        }
    }

    public function failed(Exception $e)
    {
        $error_message = $e->getMessage();
        if ($e instanceof ValidationException) {
            $error_message = json_encode($e->validator->errors());
        }
        $form = [
            'job_name' => get_class($this),
            'job_key' => Arr::get($this->taskItem, 'id', ''),
            'payload' => json_encode($this->taskItem),
            'error_message' => $error_message
        ];
        app(TaskItemFailJobRepository::class)
            ->createOrUpdate($form, ['job_name', 'job_key']);
        $this->sendSlackNotification($form);
    }

    private function sendSlackNotification($form)
    {
        $slackNotification = app(SlackNotification::class)
            ->setChannel('#設計出圖系統')
            ->setTitle("Job名稱：{$form['job_name']}")
            ->setMessage("錯誤訊息：{$form['error_message']}");
        Notification::route('slack', env('SLACK_URL'))
            ->notify($slackNotification);
    }

    private function deleteFailJob($taskItem)
    {
        $jobName = get_class($this);
        $jobKey = $taskItem['id'];
        app(TaskItemFailJobRepository::class)
            ->deleteByJobKey($jobName, $jobKey);
    }
}
