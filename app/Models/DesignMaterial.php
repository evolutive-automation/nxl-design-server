<?php

namespace App\Models;

use App\Models\Model;

class DesignMaterial extends Model
{
    public $table = 'design_materials';
}
