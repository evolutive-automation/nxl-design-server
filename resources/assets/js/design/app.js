import Vue from 'vue'
import router from '@design/router'
import vuetify from '@design/plugins/vuetify';
import '@mdi/font/css/materialdesignicons.min.css';

new Vue({
  el: '#app',
  vuetify,
  router
});
