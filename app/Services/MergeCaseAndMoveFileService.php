<?php

namespace App\Services;

use App\Services\Service;
use Intervention\Image\Facades\Image;
use Exception;

class MergeCaseAndMoveFileService extends Service
{
    /** @var array $payload */
    protected $payload;

    public function setPayload($payload)
    {
        $this->payload = $payload;

        return $this;
    }

    public function exec()
    {
        $casePaths = $this->mergeCase();
        $this->moveToPrintingRoom($casePaths);
        return $casePaths;
    }

    private function mergeCase()
    {
        $result = [];
        $designPath = $this->getDesignPath();
        $caseType = $this->payload['case_type'];
        $caseColors = config('enum.caseColor')[$caseType];
        foreach ($caseColors as $caseColor) {
            $casePath = $this->getCasePath($caseColor);
            if ($casePath === '') {
                continue;
            }
            if ($caseType === 'ModNX') {
                $backPath = $this->getCasePath('26');
                $image = Image::make($backPath);
                $image->insert($designPath);
                $image->insert($casePath);
            } else {
                $image = Image::make($casePath);
                $image->insert($designPath);
            }
            $destPath = $this->getDestPath($caseColor);
            $image->save($destPath);
            array_push($result, $destPath);
        }
        return $result;
    }

    private function getDesignPath()
    {
        return env('DesignFileSourceDir')."/{$this->payload['outputWebPath']}";
    }

    private function getCasePath($caseColor)
    {
        $result = '';
        $caseType = $this->payload['case_type'];
        $deviceCode = $this->payload['device_code'];
        $folder = env('CaseFileSourceDir')."/{$caseType}/{$caseColor}";
        $filename = "{$caseType}-{$caseColor}-{$deviceCode}.png";
        $filepath = "{$folder}/{$filename}";
        if (file_exists($filepath)) {
            $result = $filepath;
        }
        return $result;
    }

    private function getDestPath($caseColor)
    {
        $folder = env('DesignFileSourceDir').'/mergeCase/web';
        $prefixFilename = "{$this->payload['case_type']}";
        if ($this->payload['case_type'] === 'ModNX') {
            $filename = "{$prefixFilename}-26-{$caseColor}-{$this->payload['device_code']}-{$this->payload['code']}.png";
        } else {
            $filename = "{$prefixFilename}-{$caseColor}-{$this->payload['device_code']}-{$this->payload['code']}.png";
        }
        return "{$folder}/{$filename}";
    }

    private function moveToPrintingRoom($casePaths)
    {
        $paths = $casePaths;
        array_push(
            $paths,
            env('DesignFileSourceDir'). "/{$this->payload['outputPrintPath']}",
            env('DesignFileSourceDir'). "/{$this->payload['outputWebPath']}"
        );
        foreach ($paths as $path) {
            try {
                if (!file_exists($path)) {
                    continue;
                }
                $filename = basename($path);
                $fileInfo = explode('-', $filename);
                $destFolder = $this->getDestFolder($path);
                if ($destFolder === '') {
                    continue;
                }
                checkFolder($destFolder);
                $folderLength = preg_match('/pdf/', $filename)? count($fileInfo) - 2 : count($fileInfo) - 1;
                $recursiveFolder = '';
                for ($i = 1; $i <= $folderLength; $i++) {
                    $recursiveFolder = $this->checkRecursiveFolder($destFolder, $fileInfo, $i);
                }
                $destPath = "{$recursiveFolder}/{$filename}";
                moveFile($path, $destPath);
            } catch (Exception $e) {
                sendSlackNotification("{$path}檔案搬移失敗", "錯誤訊息：".$e->getMessage());
            }
        }
    }

    private function getDestFolder($path)
    {
        $result = '';
        if (preg_match("/mergeCase\/web/", $path)) {
            $result = env('MergeCaseFileSourceDir');
        } elseif (preg_match("/output\/print/", $path)) {
            $result = env('PrintFileSourceDir');
        } elseif (preg_match("/output\/web/", $path)) {
            $result = env('PreviewFileSourceDir');
        }
        return $result;
    }

    private function checkRecursiveFolder($destFolder, $fileInfo, $index)
    {
        $folder = $destFolder;
        for ($i = 0; $i < $index; $i++) {
            $folder .= "/{$fileInfo[$i]}";
        }
        checkFolder($folder);
        return $folder;
    }
}
