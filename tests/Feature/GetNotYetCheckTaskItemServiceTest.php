<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\WithFaker;
use App\Services\GetNotYetCheckTaskItemService;
use App\Models\TaskItem;
use Tests\DesignTestCase;

class GetNotYetCheckTaskItemServiceTest extends DesignTestCase
{
    use WithFaker, RefreshDatabase, DatabaseMigrations;

    public function test_it_can_get_task_item_by_task()
    {
        $fakeDesignTask = $this->createFakeDesignTask('pending');
        $task = $fakeDesignTask['task'];
        $designMaterial = $fakeDesignTask['designMaterial'];
        $this->createFakeDesignTaskItem($task, $designMaterial);
        TaskItem::where('task_id', $task->id)->update([
            'have_print' => true,
            'have_web' => true,
            'is_allowed' => false
        ]);

        $items = app(GetNotYetCheckTaskItemService::class)
            ->setTaskId($task->id)
            ->exec();

        foreach ($items as $item) {
            $this->assertEquals($task->id, $item['taskId']);
            $this->assertEquals(false, $item['isAllowed']);
            $this->assertEquals(true, $item['havePrint']);
            $this->assertEquals(true, $item['haveWeb']);
            $this->assertNotEquals("", $item['outputPrintPath']);
            $this->assertNotEquals("", $item['outputWebPath']);
            $this->assertNotEquals("", $item['casePath']);
        }
    }
}
