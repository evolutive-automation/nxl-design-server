<?php

namespace Tests\Feature;

use App\Models\Brand;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\WithFaker;
use App\Services\GetBrandService;
use Tests\DesignTestCase;

class GetBrandServiceTest extends DesignTestCase
{
    use WithFaker, RefreshDatabase, DatabaseMigrations;

    public function test_it_can_get_brand_list()
    {
        $this->createMetaData();

        $data = app(GetBrandService::class)
            ->setArgs([])
            ->exec();
        $brands = Brand::get();
        $this->assertCount(count($brands), $data);

        foreach ($data as $row) {
            $keys = collect($row)->keys()->toArray();
            $this->assertEquals(['name', 'code'], $keys);
        }
    }
}
