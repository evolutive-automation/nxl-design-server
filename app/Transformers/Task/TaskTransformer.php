<?php

namespace App\Transformers\Task;

use App\Models\Task;
use App\Transformers\Transformer;
use Carbon\Carbon;
use Illuminate\Support\Arr;

class TaskTransformer extends Transformer
{
    /**
     * @param Task $taskModel
     *
     * @return array
     */
    public function exec($taskModel)
    {
        $startedAt = is_null($taskModel->started_at)? null : Carbon::parse($taskModel->started_at, 'UTC')->setTimezone('Asia/Taipei')->toDateTimeString();
        $endedAt = is_null($taskModel->ended_at)? null : Carbon::parse($taskModel->ended_at, 'UTC')->setTimezone('Asia/Taipei')->toDateTimeString();
        $payload = json_decode($taskModel->payload, true);

        return [
            'id' => $taskModel->id,
            'type' => $taskModel->type,
            'typeText' => $this->getTypeText($taskModel->type),
            'status' => $taskModel->status,
            'statusText' => $this->getStatusText($taskModel->status),
            'progress' => $this->calculateProgress($taskModel),
            'spentTime' => $this->calculateSpentTime($startedAt, $endedAt),
            'name' => $taskModel->name,
            'payload' => $payload,
            'quantity' => (int) $taskModel->quantity,
            'ngQuantity' => (int) $taskModel->ng_quantity,
            'doneQuantity' => (int) $taskModel->done_quantity,
            'createdUser' => Arr::get($payload, 'created_user', ''),
            'version' => Arr::get($payload, 'version', ''),
            'startedAt' => $startedAt,
            'endedAt' => $endedAt,
            'notifiedAt' => is_null($taskModel->notified_at)? null : Carbon::parse($taskModel->notified_at, 'UTC')->setTimezone('Asia/Taipei')->toDateTimeString()
        ];
    }

    private function getTypeText($type)
    {
        $result = $type;

        switch ($type) {
            case 'design':
                $result = '設計變更';
                break;
            case 'device':
                $result = '型號變更';
                break;
        }

        return $result;
    }

    private function getStatusText($status)
    {
        $result = $status;
        $list = config('enum.status');
        if (isset($list[$status])) {
            $result = $list[$status];
        }
        return $result;
    }

    private function calculateProgress($taskModel)
    {
        $quantity = (int) $taskModel->quantity;
        $doneQuantity = (int) $taskModel->done_quantity;
        $doneQuantity = (int) $taskModel->done_quantity;
        $ngQuantity = (int) $taskModel->ng_quantity;
        $result = "0%";
        if ($quantity > 0) {
            $tempQuantity = ($doneQuantity + $ngQuantity);
            if ($tempQuantity > $quantity) {
                $tempQuantity = $quantity;
            }
            $result = round($tempQuantity/$quantity, 2) * 100 . "%";
        }
        return $result;
    }

    private function calculateSpentTime($startedAt, $endedAt)
    {
        $result = '';
        if (!is_null($startedAt) && !is_null($endedAt)) {
            $start = Carbon::parse($startedAt);
            $end = Carbon::parse($endedAt);
            $result = $start->diffInMinutes($end).'分鐘';
        }

        return $result;
    }
}
