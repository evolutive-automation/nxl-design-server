<?php

namespace App\Models;

use App\Models\Model;
use App\Models\Brand;

class Device extends Model
{
    public $table = 'devices';

    public function brand()
    {
        return $this->belongsTo(Brand::class, 'brand_code', 'code');
    }
}
