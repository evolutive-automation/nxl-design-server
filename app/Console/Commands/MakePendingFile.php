<?php

namespace App\Console\Commands;

use App\Models\TaskItem;
use Illuminate\Console\Command;
use App\Jobs\SavePendingFileJob;

class MakePendingFile extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'make:pendingFile';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '重新建立 Pending 檔案';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $start = microtime(true);
        $data = TaskItem::where('is_allowed', 1)
            ->where(function ($q) {
                return $q->where('have_print', 0)
                    ->orWhere('have_web', 0);
            })
            ->whereHas('task', function ($q) {
                return $q->where('status', 'making');
            })
            ->with(['design'])
            ->get();
        foreach ($data as $taskItemModel) {
            $design = $taskItemModel->design;
            $filename = $design->material->filename;
            if (!$taskItemModel->have_print) {
                $destPath = env('DesignFileSourceDir')."/{$taskItemModel->material_print_path}";
                $sourcePath = $this->getMaterialPath($filename, 'print');
                file_put_contents($destPath, file_get_contents($sourcePath));
            }
            if (!$taskItemModel->have_web) {
                $destPath = env('DesignFileSourceDir')."/{$taskItemModel->material_web_path}";
                $sourcePath = $this->getMaterialPath($filename, 'web');
                file_put_contents($destPath, file_get_contents($sourcePath));
            }
        }
        $end = microtime(true);
        $total = round($end - $start, 2);
        echo "重新建立 Pending 檔案成功，總花費{$total}秒\n";
    }

    private function getMaterialPath($filename, $type)
    {
        $materialFolder = env('DesignFileSourceDir') . "/material/{$type}";
        $extension = ($type === 'print')? 'pdf' : 'png';

        return "{$materialFolder}/{$filename}.{$extension}";
    }
}
