<?php

namespace App\Transformers\DesignMaterial;

use App\Models\DesignMaterial;
use App\Transformers\Transformer;

class DesignMaterialTransformer extends Transformer
{
    /**
     * @param DesignMaterial $designMaterialModel
     *
     * @return array
     */
    public function exec($designMaterialModel)
    {
        return [
            'id' => $designMaterialModel->id,
            'filename' => $designMaterialModel->filename,
            'caseType' => $designMaterialModel->case_type,
            'code' => $designMaterialModel->code,
            'version' => $designMaterialModel->version,
            'printPath' => "material/print/{$designMaterialModel->filename}.pdf",
            'webPath' => "material/web/{$designMaterialModel->filename}.png"
        ];
    }
}
