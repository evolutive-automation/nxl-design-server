<?php

namespace Tests\Feature;

use App\Exceptions\DuplicateDeviceException;
use App\Models\Device;
use App\Services\CreateDeviceTaskService;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\DesignTestCase;

class CreateDeviceTaskServiceTest extends DesignTestCase
{
    use WithFaker, RefreshDatabase, DatabaseMigrations;

    public function test_it_can_create_device_type_have_shadow_task()
    {
        $this->createMetaData();
        Device::where('brand_code', '03')->where('code', '074')->delete();
        $payload = [
            'version' => 1,
            'brand_code' => '03',
            'code' => '074',
            'name' => 'Samsung S9',
            'case_type' => 'SolidSuit',
            'created_user' => 'tony',
            'colors' => ['52','53'],
            'mask_print' => $this->mockUploadFile('mask-print-SolidSuit-074.pdf', 'application/pdf'),
            'mask_web' => $this->mockUploadFile('mask-web-SolidSuit-074.png', 'image/png'),
            'shadow_web' => $this->mockUploadFile('shadow-web-SolidSuit-074.png', 'image/png')
        ];
        $colorText = implode(',', $payload['colors']);
        $expectedName = "{$payload['code']}-{$colorText}-{$payload['case_type']}";
        app(CreateDeviceTaskService::class)
            ->setPayload($payload)
            ->exec();
        $temp = $payload;
        unset($temp['mask_print']);
        unset($temp['mask_web']);
        unset($temp['shadow_web']);
        $this->assertDatabaseHas('tasks', [
            'type' => 'device',
            'status' => 'pending',
            'name' => $expectedName,
            'payload' => json_encode($temp),
            'quantity' => 0,
            'done_quantity' => 0
        ]);
        $this->assertDatabaseHas('devices', [
            'brand_code' => $payload['brand_code'],
            'code' => $payload['code'],
            'name' => $payload['name'],
            'have_shadow' => 1
        ]);
        $filename = "{$payload['case_type']}-{$payload['code']}";
        $this->assertTrue(file_exists(env('DesignFileSourceDir')."/mask/print/mask-print-{$filename}.pdf"));
        $this->assertTrue(file_exists(env('DesignFileSourceDir')."/mask/web/mask-web-{$filename}.png"));
        $this->assertTrue(file_exists(env('DesignFileSourceDir')."/shadow/web/shadow-web-{$filename}.png"));
    }

    public function test_it_can_create_device_type_no_shadow_task()
    {
        $this->createMetaData();
        Device::where('brand_code', '03')->where('code', '074')->delete();
        $payload = [
            'version' => 1,
            'brand_code' => '03',
            'code' => '074',
            'name' => 'Samsung S9',
            'case_type' => 'SolidSuit',
            'created_user' => 'tony',
            'colors' => ['52','53'],
            'mask_print' => $this->mockUploadFile('mask-print-SolidSuit-074.pdf', 'application/pdf'),
            'mask_web' => $this->mockUploadFile('mask-web-SolidSuit-074.png', 'image/png'),
            'shadow_web' => null
        ];
        $colorText = implode(',', $payload['colors']);
        $expectedName = "{$payload['code']}-{$colorText}-{$payload['case_type']}";
        app(CreateDeviceTaskService::class)
            ->setPayload($payload)
            ->exec();
        $temp = $payload;
        unset($temp['mask_print']);
        unset($temp['mask_web']);
        unset($temp['shadow_web']);
        $this->assertDatabaseHas('tasks', [
            'type' => 'device',
            'status' => 'pending',
            'name' => $expectedName,
            'payload' => json_encode($temp),
            'quantity' => 0,
            'done_quantity' => 0
        ]);
        $this->assertDatabaseHas('devices', [
            'brand_code' => $payload['brand_code'],
            'code' => $payload['code'],
            'name' => $payload['name'],
            'have_shadow' => 0
        ]);
        $filename = "{$payload['case_type']}-{$payload['code']}";
        $this->assertTrue(file_exists(env('DesignFileSourceDir')."/mask/print/mask-print-{$filename}.pdf"));
        $this->assertTrue(file_exists(env('DesignFileSourceDir')."/mask/web/mask-web-{$filename}.png"));
    }
}
