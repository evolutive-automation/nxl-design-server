<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>設計出圖系統</title>
    </head>
    <body>
        <v-app id="app">
            <v-main
                class="ma-0 pa-0 teal lighten-2"
            >
                <router-view style="max-width: 100%;"/>
            </v-main>
        </v-app>
        <script src="{{ asset('assets/design.js') }}"></script>
    </body>
</html>
