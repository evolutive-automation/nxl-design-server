<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Services\GetCaseTypeService;
use Illuminate\Http\Request;

class CaseTypeController extends Controller
{
    public function search(Request $request)
    {
        $data = app(GetCaseTypeService::class)
            ->setArgs($request->all())
            ->exec();

        return ok(['data' => $data]);
    }
}
