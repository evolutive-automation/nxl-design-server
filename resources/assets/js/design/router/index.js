import Vue from 'vue'
import Router from 'vue-router'
import Home from '@design/views/Home.vue'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: "*",
      redirect: {
        name: 'home-page'
      }
    }, {
      path: "/:version/:ink/:number/home",
      name: "home-page",
      component: Home
    },
  ],
});
