<?php

namespace App\Services;

use App\Jobs\SavePendingFileJob;
use App\Repositories\DesignMaterialRepository;
use App\Repositories\DesignRepository;
use App\Repositories\DeviceRepository;
use App\Repositories\TaskItemRepository;
use App\Repositories\TaskRepository;
use Illuminate\Contracts\Filesystem\FileNotFoundException;
use App\Services\Service;
use App\Transformers\Design\WithMaterialTransformer;
use Carbon\Carbon;
use Exception;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class CreateDeviceTaskItemService extends Service
{
    /** @var array $task */
    protected $task;

    /** @var TaskRepository $taskRepository */
    protected $taskRepository;

    /** @var TaskItemRepository $taskItemRepository */
    protected $taskItemRepository;

    /** @var DesignRepository $designRepository */
    protected $designRepository;

    /** @var DeviceRepository $deviceRepository */
    protected $deviceRepository;

    public function __construct(
        TaskRepository $taskRepository,
        TaskItemRepository $taskItemRepository,
        DesignRepository $designRepository,
        DeviceRepository $deviceRepository
    ) {
        $this->taskRepository = $taskRepository;
        $this->taskItemRepository = $taskItemRepository;
        $this->designRepository = $designRepository;
        $this->deviceRepository = $deviceRepository;
    }

    public function setTask($task)
    {
        $this->task = $task;

        return $this;
    }

    public function exec()
    {
        $payload = $this->task['payload'];
        $this->validateRule($payload);
        $this->updateTaskStatus();
        $device = $this->getDevice($payload['code']);
        $designs = $this->getDesigns($payload);
        if (empty($designs)) {
            throw new ModelNotFoundException("該任務{$this->task['name']}，沒有設計素材可以產檔案");
        }
        $designs = $this->createOrUpdateDeviceDesigns($payload['code'], $designs);
        $taskItems = $this->processTaskItems($designs, $device['haveShadow']);
        $this->createTaskItems($taskItems);
        $this->updateTask($taskItems);
        return $taskItems;
    }

    private function validateRule($payload)
    {
        $this->validate($payload, [
            'brand_code' => 'required|string',
            'code' => 'required|string',
            'name' => 'required|string',
            'case_type' => 'required|string',
            'colors' => 'required|array',
            'version' => 'required|numeric'
        ]);
    }

    private function updateTaskStatus()
    {
        $payload = [
            'status' => 'making'
        ];

        $this->taskRepository->update($this->task['id'], $payload);
    }

    private function getDevice($code)
    {
        return $this->deviceRepository->firstWhere('code', $code);
    }

    private function getDesigns($payload)
    {
        $caseType = $payload['case_type'];
        $colors = $payload['colors'];
        $version = $payload['version'];

        return $this->designRepository->getDesignByMaterial($caseType, $colors, $version);
    }

    private function createOrUpdateDeviceDesigns($deviceCode, $designs)
    {
        $data = [];
        foreach ($designs as $design) {
            $payload = [
                'design_material_id' => $design['designMaterialId'],
                'case_type' => $design['caseType'],
                'color_code' => $design['colorCode'],
                'device_code' => $deviceCode,
                'sort' => $design['sort'],
                'code' => $design['code'],
                'print_type' => $design['printType']
            ];
            $row = $this->designRepository
                ->setRelation(['material'])
                ->setTransformer(WithMaterialTransformer::class)
                ->createOrUpdate($payload, ['case_type','color_code','device_code','code']);
            array_push($data, $row);
        }

        return $data;
    }

    private function processTaskItems($designs, $hasShadow)
    {
        $data = [];
        $now = Carbon::now();
        foreach ($designs as $design) {
            try {
                $relativePrintPath = $this->savePendingFile($design, 'print');
                $relativeWebPath = $this->savePendingFile($design, 'web', $hasShadow);
                array_push($data, [
                    'task_id' => $this->task['id'],
                    'design_id' => $design['id'],
                    'material_print_path' => $relativePrintPath,
                    'material_web_path' => $relativeWebPath,
                    'created_at' => $now
                ]);
            } catch (Exception $e) {
                continue;
            }
        }
        return $data;
    }

    private function savePendingFile($design, $type, $hasShadow = false)
    {
        $extension = ($type === 'print')? 'pdf' : 'png';
        $folder = "pending/{$type}";
        $prefixName = "{$design['caseType']}-{$design['colorCode']}-{$design['deviceCode']}-{$design['code']}-{$design['printType']}";
        $filename = "{$prefixName}.{$extension}";
        if ($type === 'web') {
            $isShadow = $hasShadow? '1' : '0';
            $filename = "{$prefixName}-{$isShadow}.{$extension}";
        }
        $destPath = env('DesignFileSourceDir')."/{$folder}/{$filename}";
        $sourceFolder = env('DesignFileSourceDir') . "/material/{$type}";
        $sourcePath = "{$sourceFolder}/{$design['material']['filename']}.{$extension}";
        if (file_exists($sourcePath)) {
            file_put_contents($destPath, file_get_contents($sourcePath));
        }

        return "{$folder}/{$filename}";
    }

    private function createTaskItems($items)
    {
        return $this->taskItemRepository->createMany($items);
    }

    private function updateTask($items)
    {
        $payload = [
            'quantity' => count($items),
            'started_at' => Carbon::now()
        ];

        $this->taskRepository->update($this->task['id'], $payload);
    }
}
