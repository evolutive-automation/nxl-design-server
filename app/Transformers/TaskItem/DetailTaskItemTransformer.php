<?php

namespace App\Transformers\TaskItem;

use App\Models\TaskItem;
use App\Transformers\Design\DesignTransformer;
use App\Transformers\Task\TaskTransformer;
use App\Transformers\Transformer;

class DetailTaskItemTransformer extends Transformer
{
    /**
     * @param TaskItem $taskItemModel
     *
     * @return array
     */
    public function exec($taskItemModel)
    {
        return [
            'id' => $taskItemModel->id,
            'taskId' => $taskItemModel->task_id,
            'havePrint' => $taskItemModel->have_print? true : false,
            'haveWeb' => $taskItemModel->have_web? true : false,
            'isAllowed' => $taskItemModel->is_allowed? true : false,
            'materialPrintPath' => $taskItemModel->material_print_path,
            'materialWebPath' => $taskItemModel->material_web_path,
            'task' => app(TaskTransformer::class)->exec($taskItemModel->task),
            'design' => app(DesignTransformer::class)->exec($taskItemModel->design)
        ];
    }
}
