<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\WithFaker;
use App\Services\GetGmgColorService;
use Tests\DesignTestCase;

class GetGmgColorServiceTest extends DesignTestCase
{
    use WithFaker, RefreshDatabase, DatabaseMigrations;

    public function test_it_can_get_color_list()
    {
        $colors = config('enum.gmgColor');

        $data = app(GetGmgColorService::class)
            ->exec();

        $this->assertArrayHasKey('phone', $data);
        $this->assertArrayHasKey('airpods', $data);
        $this->assertEquals($colors['phone'], $data['phone']);
        $this->assertEquals($colors['airpods'], $data['airpods']);
    }
}
