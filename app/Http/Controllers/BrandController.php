<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Services\GetBrandService;
use Illuminate\Http\Request;

class BrandController extends Controller
{
    public function search(Request $request)
    {
        $data = app(GetBrandService::class)
            ->setArgs($request->all())
            ->exec();

        return ok(['data' => $data]);
    }
}
