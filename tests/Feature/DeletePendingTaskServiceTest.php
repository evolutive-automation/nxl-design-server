<?php

namespace Tests\Feature;

use App\Exceptions\CantDeleteTaskException;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\WithFaker;
use App\Services\DeletePendingTaskService;
use Tests\DesignTestCase;

class DeletePendingTaskServiceTest extends DesignTestCase
{
    use WithFaker, RefreshDatabase, DatabaseMigrations;

    public function test_it_can_delete_pending_task()
    {
        $fakeDesignTask = $this->createFakeDesignTask('pending');
        $task = $fakeDesignTask['task'];

        app(DeletePendingTaskService::class)
            ->setTaskId($task['id'])
            ->exec();

        $this->assertDatabaseMissing('tasks', [
            'id' => $task['id']
        ]);
    }

    public function test_it_can_throw_cant_delete_task_exception()
    {
        $this->withExceptionHandling();
        $this->expectException(CantDeleteTaskException::class);

        $fakeDesignTask = $this->createFakeDesignTask('making');
        $task = $fakeDesignTask['task'];

        app(DeletePendingTaskService::class)
            ->setTaskId($task['id'])
            ->exec();
    }
}
