<?php

namespace App\Models;

use App\Models\TaskItem;
use App\Models\Model;

class Task extends Model
{
    public $table = 'tasks';

    public function taskItems()
    {
        return $this->hasMany(TaskItem::class, 'task_id', 'id');
    }
}
