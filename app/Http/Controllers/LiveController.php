<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Design;
use App\Models\DesignMaterial;
use App\Models\TaskItem;
use App\Models\Task;
use Illuminate\Http\Request;
use ZipArchive;

class LiveController extends Controller
{
    public function jeffery(Request $request)
    {
        $designCodes = [
            'OD34','OD35','OD36','OX01','OX02','OX03','OX04','OX05',
            'OZ03','OZ05','OZ12','OZ17','OZ30','OZ34','OZ42'
        ];
        $errors = [];
        foreach ($designCodes as $designCode) {
            $materials = DesignMaterial::where('code', $designCode)->get();
            foreach ($materials as $material) {
                try {
                    if (is_null($material)) {
                        continue;
                    }
                    $desings = Design::where('design_material_id', $material->id)->get();
                    $designIds = $desings->pluck('id')->toArray();
                    $taskItems = TaskItem::whereIn('design_id', $designIds)->get();
                    $taskItemIds = $taskItems->pluck('id')->toArray();
                    $taskId = is_null($taskItems->first())? null : $taskItems->first()->task_id;
                    if (count($taskItemIds)) {
                        TaskItem::whereIn('id', $taskItemIds)->delete();
                    }
                    if (!is_null($taskId)) {
                        Task::where('id', $taskId)->delete();
                    }
                    if (count($designIds)) {
                        Design::whereIn('id', $designIds)->delete();
                    }
                    $material->delete();
                } catch (\Exception $e) {
                    array_push($errors, $designCode);
                }
            }
        }
        return $errors;
    }

    public function createDesignMaterial()
    {
        $filenames = [
            'Clear-A001-1'
        ];
        foreach ($filenames as $filename) {
            $isExists = DesignMaterial::where('filename', $filename)
                ->exists();
            if (!$isExists) {
                list($caseType, $code, $version) = explode('-', $filename);
                DesignMaterial::create([
                    'filename' => $filename,
                    'case_type' => $caseType,
                    'code' => $code,
                    'version' => $version
                ]);
            }
        }
        return ok();
    }
}
