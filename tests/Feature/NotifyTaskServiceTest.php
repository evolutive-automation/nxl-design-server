<?php

namespace Tests\Feature;

use App\Notifications\SlackNotification;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\WithFaker;
use App\Services\NotifyTaskService;
use Tests\DesignTestCase;
use Carbon\Carbon;
use Illuminate\Notifications\AnonymousNotifiable;
use Illuminate\Support\Facades\Notification;

class NotifyTaskServiceTest extends DesignTestCase
{
    use WithFaker, RefreshDatabase, DatabaseMigrations;

    public function test_it_can_update_notified_at_by_made_status()
    {
        $fakeDesignTask = $this->createFakeDesignTask('made');
        $task = $fakeDesignTask['task'];
        $task->started_at = Carbon::now();
        $task->ended_at = Carbon::now();
        $task->save();

        Notification::fake();
        app(NotifyTaskService::class)
            ->exec();

        Notification::assertSentTo(new AnonymousNotifiable, SlackNotification::class);
        $this->assertDatabaseHas('tasks', [
            'id' => $task->id,
            'notified_at' => Carbon::now()->toDateTimeString()
        ]);
    }

    public function test_it_can_update_notified_at_by_making_status()
    {
        $fakeDesignTask = $this->createFakeDesignTask('making');
        $task = $fakeDesignTask['task'];
        $task->started_at = Carbon::now()->subHour();
        $task->done_quantity = 1;
        $task->save();

        Notification::fake();
        app(NotifyTaskService::class)
            ->exec();

        Notification::assertSentTo(new AnonymousNotifiable, SlackNotification::class);
        $this->assertDatabaseHas('tasks', [
            'id' => $task->id,
            'notified_at' => Carbon::now()->toDateTimeString()
        ]);
    }
}
