<?php

namespace App\Services;

use App\Repositories\TaskRepository;
use App\Services\Service;

class GetNotYetCheckTaskService extends Service
{
    /** @var TaskRepository $taskRepository */
    protected $taskRepository;

    public function __construct(
        TaskRepository $taskRepository
    ) {
        $this->taskRepository = $taskRepository;
    }
    public function exec()
    {
        return $this->taskRepository->getNotCheckedTasks();
    }
}
