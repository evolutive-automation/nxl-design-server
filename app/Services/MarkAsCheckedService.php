<?php

namespace App\Services;

use App\Events\NotifyCheckedDesignEvent;
use App\Repositories\TaskRepository;
use App\Services\Service;
use App\Transformers\Task\DetailTaskTransformer;
use Carbon\Carbon;

class MarkAsCheckedService extends Service
{
    /** @var int $taskId */
    protected $taskId;

    /** @var TaskRepository $taskRepository */
    protected $taskRepository;

    public function __construct(
        TaskRepository $taskRepository
    ) {
        $this->taskRepository = $taskRepository;
    }

    public function setTaskId($taskId)
    {
        $this->taskId = $taskId;

        return $this;
    }

    public function exec()
    {
        $task = $this->getTask();
        $this->updateTask();
        $this->fireCheckedDesignEvent($task['type'], $task['items']);
    }

    public function getTask()
    {
        return $this->taskRepository
            ->setRelation(['taskItems.design.device'])
            ->setTransformer(DetailTaskTransformer::class)
            ->findOrFail($this->taskId);
    }

    public function updateTask()
    {
        $payload = [
            'status' => 'checked',
            'checked_at' => Carbon::now()
        ];

        $this->taskRepository->update($this->taskId, $payload);
    }

    private function fireCheckedDesignEvent($taskType, $items)
    {
        foreach ($items as $item) {
            if (!$item['isAllowed']) {
                continue;
            }
            $design = $item['design'];
            $device = $design['device'];
            $payload = [
                'id' => $item['id'],
                'task' => $taskType,
                'case_type' => $design['caseType'],
                'color_code' => $design['colorCode'],
                'device_code' => $design['deviceCode'],
                'sort' => $design['sort'],
                'code' => $design['code'],
                'print_type' => $design['printType'],
                'device' => [
                    'brand_code' => $device['brandCode'],
                    'name' => $device['name'],
                    'code' => $device['code']
                ],
                'outputPrintPath' => $item['outputPrintPath'],
                'outputWebPath' => $item['outputWebPath']
            ];
            event(new NotifyCheckedDesignEvent($payload));
        }
    }
}
