<?php

namespace App\Services;

use App\Repositories\TaskRepository;
use App\Services\Service;

class GetPendingTaskService extends Service
{
    /** @var int $limit */
    protected $limit;

    /** @var TaskRepository $taskRepository */
    protected $taskRepository;

    public function __construct(
        TaskRepository $taskRepository
    ) {
        $this->taskRepository = $taskRepository;
    }

    public function setLimit($limit)
    {
        $this->limit = $limit;

        return $this;
    }

    public function exec()
    {
        $data = [];
        $tasks = $this->getMakingTasks();
        if (count($tasks) < (int) $this->limit) {
            $needQuantity = $this->limit - count($tasks);
            $data = $this->getPendingTasks($needQuantity);
        }
        return $data;
    }

    private function getMakingTasks()
    {
        $args = [
            'status' => 'making'
        ];

        return $this->taskRepository->search($args);
    }

    private function getPendingTasks($needQuantity)
    {
        return $this->taskRepository->getLimitWithPendingStatus($needQuantity);
    }
}
