<?php

namespace App\Transformers\Brand;

use App\Models\Brand;
use App\Transformers\Transformer;

class BrandTransformer extends Transformer
{
    /**
     * @param Brand $brandModel
     *
     * @return array
     */
    public function exec($brandModel)
    {
        return [
            'name' => $brandModel->name,
            'code' => $brandModel->code
        ];
    }
}
