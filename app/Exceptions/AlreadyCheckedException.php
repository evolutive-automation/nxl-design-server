<?php

namespace App\Exceptions;

use Exception;

class AlreadyCheckedException extends Exception
{
    protected $message;

    public function __construct($message = 'already checked task exception.')
    {
        parent::__construct($message);
    }

    public function getStatusCode()
    {
        return 400;
    }
}
