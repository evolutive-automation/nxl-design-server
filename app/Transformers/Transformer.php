<?php

namespace App\Transformers;

use App\Models\Model;

abstract class Transformer
{
    /**
     * @param Model $model
     */
    abstract public function exec($model);
}
